#pragma once

struct Bounds;
struct Vector3;

//Currently only implements functions for collisions with Bounds objects
struct BoundsTestable
{
	virtual bool Contains(const Vector3& point) const = 0;
	virtual bool Contains(const Bounds& bounds) const = 0;
	virtual bool Intersects(const Bounds& bounds) const = 0;
	virtual bool ContainedBy(const Bounds& bounds) const = 0;
};