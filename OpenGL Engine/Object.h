#pragma once

#include <vector>
#include <array>
#include <memory>

#include "Quaternion.h"
#include "Matrix.h"
#include "Model.h"
#include "Bounds.h"

struct Texture;
struct Mesh;

class Object
{
public:
	std::string Name = "";

	Model* model = nullptr;
	Vector3 ModelOffset = Vector3::Zero();

	Vector3 Position = Vector3::Zero();
	Quaternion Rotation = Quaternion::Identity;	//Relative to 0,0,-1
	Vector3 Scale = Vector3::One();
	Vector3 Velocity = Vector3::Zero();
	Quaternion AngularVelocity = Quaternion::Identity;

	Bounds AABB;	//World-space bounding box. Needs to be updated on every move/rotate or model/mesh edit
	
	Object(const std::string& name) : Name(name) {}
	Object(const std::string& name, Model* model) : Name(name), model(model) {}

	void RecentreOnModel();
	Matrix4 MakeTransformMatrix() const;
	Matrix4 MakeModelTransformMatrix() const;
	Bounds& RecalcAABB();

	static Bounds GetBounds(const std::vector<Object*>& Objects);
};