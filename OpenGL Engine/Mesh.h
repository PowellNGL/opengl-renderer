#pragma once

#include <vector>
#include <string>

#include "Vector.h"

struct Mesh
{
private:
	static unsigned int nextID;
	unsigned int id;

public:
	std::string Name = "";

	std::vector<Vector3> Verts;
	std::vector<Vector3> Normals;
	std::vector<Vector2> UVs;
	std::vector<Vector3> Tangents;
	std::vector<Vector3> Binormals;

	std::vector<unsigned int> Triangles;

	unsigned int ID() const { return id; }

	Mesh() : id(nextID++) {}
	Mesh(const std::string& name) : id(nextID++), Name(name) {}

	Mesh(const Mesh& other)
	{
		*this = other;
	}
	Mesh(Mesh&& other) noexcept
	{
		*this = std::move(other);
	}
	Mesh& operator= (const Mesh& other) 
	{ 
		if (&other != this)
		{
			id = nextID++;
			Name = other.Name;
			Verts = other.Verts;
			Normals = other.Normals;
			UVs = other.UVs;
			Tangents = other.Tangents;
			Binormals = other.Binormals;
			Triangles = other.Triangles;
		}
		return *this;
	}
	Mesh& operator= (Mesh&& other) noexcept
	{ 
		if (&other != this)
		{
			id = other.id;
			Name = std::move(other.Name);
			Verts = std::move(other.Verts);
			Normals = std::move(other.Normals);
			UVs = std::move(other.UVs);
			Tangents = std::move(other.Tangents);
			Binormals = std::move(other.Binormals);
			Triangles = std::move(other.Triangles);
		}
		return *this;
	}

	void AppendVertex(const Vector3& position, const Vector3& normal, const Vector2& UV, const Vector3& Tangent, const Vector3& Binormal);
	void AppendVertex(const Vector3& position, const Vector3& normal, const Vector2& UV) { AppendVertex(position, normal, UV, Vector3::Zero(), Vector3::Zero()); }
	void AppendVertex(const Vector3& position, const Vector3& normal) { AppendVertex(position, normal, Vector2::Zero(), Vector3::Zero(), Vector3::Zero()); }
	void AppendVertex(const Vector3& position) { AppendVertex(position, Vector3::Zero(), Vector2::Zero(), Vector3::Zero(), Vector3::Zero()); }
	void AppendTriangle(const uint32_t v0, const uint32_t v1, const uint32_t v2);
	//Check this mesh's arrays are all the right length
	bool IsValid() const;
	void CombineWith(const Mesh& otherMesh);
	void CalcNormals();
	void CalcTangents();
	std::pair<Vector3, Vector3> CalcTangent(const size_t triangleID) const;
	void Reverse();
};