#pragma once

#include "Vector.h"

struct Quaternion
{
	friend struct Matrix4;

private:
	float x, y, z, w;
	constexpr Quaternion(const float x, const float y, const float z, const float w) : x(x), y(y), z(z), w(w) {}
	constexpr explicit Quaternion(const Vector4& vector) : x(vector.x), y(vector.y), z(vector.y), w(vector.w) {}
public:
	const static Quaternion Identity;

	constexpr Quaternion() : x(Identity.x), y(Identity.y), z(Identity.z), w(Identity.w) {};
	Quaternion(const Vector3& axis, const float rotation);
	explicit Quaternion(const Vector3& eulerAngles);
	Quaternion(const Vector3& origin, const Vector3& dest);

	Quaternion Normalised() const;
	void Normalise();
	Quaternion Conjugation() const;
	Vector3 Axis() const;
	float Angle() const;
	Vector3 ToEulerAngles() const;
	Quaternion Reversed() const;

	Quaternion operator+ (const Quaternion& qB) const;
	void operator+= (const Quaternion& qB);
	Quaternion operator- (const Quaternion& qB) const;
	void operator-= (const Quaternion& qB);
	Quaternion operator* (const Quaternion& qB) const;
	void operator*= (const Quaternion& qB);
	Vector3 operator* (const Vector3& v) const;
};

Vector3 operator* (const Vector3& v, const Quaternion& q);