#pragma once

#include <unordered_map>
#include <vector>

#include "Wrappers.h"
#include "Mesh.h"

struct MeshBufferInfo
{
public:
	const unsigned int MeshID;
	VertexBuffers& buffers;
	unsigned int VertsOffset;
	unsigned int VertsLength;
	unsigned int IndicesOffset;
	unsigned int IndicesLength;

	MeshBufferInfo(const Mesh& mesh, VertexBuffers& buffers, const unsigned int vertsOffset, const unsigned int vertsLength, const unsigned int indicesOffset, const unsigned int indicesLength)
		: MeshID(mesh.ID()), buffers(buffers), VertsOffset(vertsOffset), VertsLength(vertsLength), IndicesOffset(indicesOffset), IndicesLength(indicesLength) {}
};


//This class makes vertex buffers and element buffers, and buffers mesh data into them
//Once a mesh has been buffered, a MeshBufferInfo is added to BufferedMeshes, using the vertex buffer's openGL ID as the key
//The MeshBufferInfo contains a reference to the mesh, the vertex buffer it is buffered into (and by extension the element buffer, since it is also created by the vertex buffer wrapper),
//and the offsets and length of the mesh's region in those vertex and element buffers
//If the mesh is updated, its data is simply overwritten in the relevant buffers with glBufferSubData
//To remove a mesh from the buffers, its entry in BufferedMeshes is simply deleted. If the mesh was the only one in its buffers,
//the buffers are also deleted, otherwise there will simply be a region of wasted space in the buffers
class MeshBufferManager
{
public:
	VertexArrayObject VAO;	//Put this before emptyVertexBuffer so it is constructed earlier and can be passed into its constructor
private:
	bool MeshNeedsLargerBufferRegion(const Mesh& mesh) const;

public:
	std::unordered_map<int, MeshBufferInfo> BufferedMeshes;	//The key is the Mesh ID
	std::vector<std::unique_ptr<VertexBuffers>> bufferWrappers;

	void BufferMeshesIntoSingleBuffer(const std::vector<Mesh*>& Meshes);
	void BufferMeshes(const std::vector<Mesh*>& Meshes);
	void BufferMesh(const Mesh& mesh);
	void UnbufferMesh(const Mesh& mesh);
	void UpdateMesh(const Mesh& mesh);
};