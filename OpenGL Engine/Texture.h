#pragma once

#include <vector>
#include <unordered_map>
#include <memory>
#include <algorithm>
#include <cassert>
#include <cstring>

#include "Colour.h"
#include "Vector.h"
#include "Shared.h"

struct Texture
{
	std::vector<float> _pixels{};
	size_t _width{};
	size_t _height{};
	size_t _channels{};

	inline Texture() = default;
	inline Texture(size_t width, size_t height, size_t channels, Colour initialColour) 
		: _width(width), _height(height), _channels(channels)
	{
		_pixels.resize(width * height * channels);
		for (size_t i = 0; i < _pixels.size(); i += channels) {
			std::memcpy(&_pixels[i], &initialColour, channels * sizeof(float));
		}
	}
	inline Texture(size_t width, size_t height, size_t channels)
		: _width(width), _height(height), _channels(channels)
	{
		_pixels.resize(width * height * channels);
	}
	inline Texture(size_t width, size_t height, size_t channels, const uint8_t* source_bytes)
		: _width(width), _height(height), _channels(channels)
	{
		_pixels.resize(width * height * channels);

		static constexpr auto byteToFloat = []() {
			std::array<float, 256> out{};
			for (size_t i = 0; i < out.size(); i++) {
				out[i] = static_cast<float>(i) / 255;
			}
			return out;
		}();

		size_t offset = 0;
		std::transform(source_bytes, source_bytes + _pixels.size(), _pixels.begin(), [](auto v) {
			return byteToFloat[static_cast<size_t>(v)];
		});
	}

	inline float* Pixel(size_t x, size_t y) { return &_pixels[(y * _width + x) * _channels]; }
	inline const float* Pixel(size_t x, size_t y) const { return &_pixels[(y * _width + x) * _channels]; }

	inline void Resize(size_t width, size_t height)
	{
		_pixels.resize(width * height * _channels);
		_width = width;
		_height = height;
	}

	//Copy the source texture onto this texture at the specified offset
	inline void Blit(const Texture& source, const ptrdiff_t xOffset, const ptrdiff_t yOffset)
	{
		assert(_channels == source._channels);

		size_t regionLeftBound = static_cast<size_t>(std::max<ptrdiff_t>(0, xOffset));
		size_t regionTopBound = static_cast<size_t>(std::max<ptrdiff_t>(0, yOffset));
		size_t regionRightBound = static_cast<size_t>(std::min<ptrdiff_t>(_width, xOffset + source._width));
		size_t regionBottomBound = static_cast<size_t>(std::min<ptrdiff_t>(_height, yOffset + source._height));

		for (size_t y = regionTopBound; y < regionBottomBound; y++)
		{
			float* targetRowStart = _pixels.data() + (regionLeftBound + y * _width) * _channels;
			const float* sourceRowStart = source._pixels.data() + ((-xOffset + regionLeftBound) + (-yOffset + y) * source._width) * source._channels;
			std::copy(sourceRowStart, sourceRowStart + (regionRightBound - regionLeftBound) * source._channels, targetRowStart);
		}
	}

	inline void FlipY()
	{
		for (size_t topRow = 0; topRow < _height / 2; topRow++)
		{
			size_t bottomRow = _height - 1 - topRow;
			auto topIt = _pixels.begin() + topRow * _width * _channels;
			auto bottomIt = _pixels.begin() + bottomRow * _width * _channels;

			std::swap_ranges(topIt, topIt + _width * _channels, bottomIt);
		}
	}

};