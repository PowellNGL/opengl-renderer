#pragma once

#include "Constants.h"

template<typename T, typename U>
constexpr auto DebugValue(const T& debug_value, const U& normal_value) {
#ifdef _DEBUG
	return debug_value;
#else
	return normal_value;
#endif
}