#pragma once

#include <array>

#include "Constants.h"
#include "Matrix.h"
#include "Vector.h"
#include "BoundsTestable.h"

struct Bounds : public BoundsTestable
{
public:
	Vector3 v1;
	Vector3 v2;

	constexpr Bounds() : v1(Vector3::Zero()), v2(Vector3::Zero()) {}
	constexpr Bounds(const Vector3& v1, const Vector3& v2) : v1(v1), v2(v2) {}
	constexpr Bounds(const float x1, const float y1, const float z1, const float x2, const float y2, const float z2) : v1(Vector3(x1, y1, z1)), v2(Vector3(x2, y2, z2)) {}

	Vector3 Centre() const;
	float Width() const;
	float Height() const;
	float Depth() const;
	Vector3 Size() const;
	std::array<Vector3, 8> AsVertList() const;

	//Used for iterating over vertices without getting a whole array of them upfront
	Vector3 GetSingleVert(const unsigned int i) const;

	bool Intersects(const Bounds& bounds) const override;
	bool Contains(const Vector3& point) const override;
	bool Contains(const Bounds& other) const override;
	bool ContainedBy(const Bounds& other) const override { return other.Contains(*this); }

	Bounds Intersection(const Bounds& other) const;
	Bounds Union(const Bounds& other) const;
	Vector3 ConstrainPoint(const Vector3& other) const;	//Limit a point to be within these bounds
	Axis LongestAxis() const;
	float Midpoint(const Axis axis) const;
	float MaxExtent(const Axis& axis) const { return v2[axis]; }
	float MinExtent(const Axis& axis) const { return v1[axis]; }
};

Bounds operator+ (const Bounds& bounds, const Vector3& vector);
Bounds operator- (const Bounds& bounds, const Vector3& vector);
void operator+= (Bounds& bounds, const Vector3& vector);
void operator-= (Bounds& bounds, const Vector3& vector);
Bounds operator* (const Bounds& bounds, const float value);
Bounds operator/ (const Bounds& bounds, const float value);
void operator*= (Bounds& bounds, const float value);
void operator/= (Bounds& bounds, const float value);
bool operator== (const Bounds& boundsA, const Bounds& boundsB);
bool operator!= (const Bounds& boundsA, const Bounds& boundsB);