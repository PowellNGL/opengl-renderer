#include "Wrappers.h"
#include "Mesh.h"
#include <SDL.h>
#include <filesystem>
#include <fstream>
#include "Texture.h"

std::unordered_map<std::string, std::string> Program::shaderFileSources;

VertexBuffers::VertexBuffers(VertexArrayObject& vao, size_t numVerts, size_t numIndices) 
	: NumVerts(numVerts), NumIndices(numIndices), VAO(vao)
{
	glBindVertexArray(vao.GLIndex());
	glGenBuffers(6, BufferIndices.data());

	glBindBuffer(GL_ARRAY_BUFFER, BufferIndices[(size_t)Type::Position]);
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(float) * 3, NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, BufferIndices[(size_t)Type::Normal]);
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(float) * 3, NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, BufferIndices[(size_t)Type::UV]);
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(float) * 2, NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, BufferIndices[(size_t)Type::Tangent]);
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(float) * 3, NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, BufferIndices[(size_t)Type::Binormal]);
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(float) * 3, NULL, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, BufferIndices[(size_t)Type::Index]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(uint32_t), NULL, GL_STATIC_DRAW);
}

VertexBuffers::~VertexBuffers()
{
	glDeleteBuffers(BufferIndices.size(), BufferIndices.data());
}

TextureBuffer::TextureBuffer(const Vector2i& resolution, const GLint format, const std::vector<const Texture*>& startingTexture, const TextureBufferOptions& options)
{
	Colour borderColour(0, 0, 0, 0);
	
	GLint inputFormat = -1;
	switch (format)
	{
	case GL_R8: case GL_R8_SNORM: case GL_R16: case GL_R16_SNORM: case GL_R16F: case GL_R32F: case GL_R8I: case GL_R8UI: case GL_R16I: case GL_R16UI: case GL_R32I: case GL_R32UI:
		inputFormat = GL_RED;
		break;

	case GL_RG8: case GL_RG8_SNORM: case GL_RG16: case GL_RG16_SNORM: case GL_RG16F: case GL_RG32F: case GL_RG8I: case GL_RG8UI: case GL_RG16I: case GL_RG16UI: case GL_RG32I: case GL_RG32UI:
		inputFormat = GL_RG;
		break;

	case GL_R3_G3_B2: case GL_RGB4: case GL_RGB5: case GL_RGB8: case GL_RGB8_SNORM: case GL_RGB10: case GL_RGB12: case GL_RGB16_SNORM: case GL_RGBA2: case GL_RGBA4:
	case GL_SRGB8: case GL_RGB16F: case GL_RGB32F: case GL_R11F_G11F_B10F: case GL_RGB9_E5: case GL_RGB8I: case GL_RGB8UI: case GL_RGB16I: case GL_RGB16UI: case GL_RGB32I: case GL_RGB32UI:
		inputFormat = GL_RGB;
		break;

	case GL_RGB5_A1: case GL_RGBA8: case GL_RGBA8_SNORM: case GL_RGB10_A2: case GL_RGB10_A2UI: case GL_RGBA12: case GL_RGBA16: case GL_SRGB8_ALPHA8: case GL_RGBA16F:
	case GL_RGBA32F: case GL_RGBA8I: case GL_RGBA8UI: case GL_RGBA16I: case GL_RGBA16UI: case GL_RGBA32I: case GL_RGBA32UI:
		inputFormat = GL_RGBA;
		break;

	case GL_DEPTH_COMPONENT16: case GL_DEPTH_COMPONENT24: case GL_DEPTH_COMPONENT32: case GL_DEPTH_COMPONENT32F:
		inputFormat = GL_DEPTH_COMPONENT;
		break;
	}

	GLint target = -1;
	switch (options.Type)
	{
	case TextureBufferType::Flat:
		target = GL_TEXTURE_2D;
		break;
	case TextureBufferType::Cubemap:
		target = GL_TEXTURE_CUBE_MAP;
		break;
	case TextureBufferType::Array:
		target = GL_TEXTURE_2D_ARRAY;
		break;
	}

	GLint minFilter = -1, magFilter = -1;
	if (options.UseLinearFilter)
	{
		magFilter = GL_LINEAR;
		if (options.UseMipMap)
		{
			minFilter = GL_LINEAR_MIPMAP_LINEAR;
		} else {
			minFilter = GL_LINEAR;
		}
	} else {
		magFilter = GL_NEAREST;
		if (options.UseMipMap)
		{
			minFilter = GL_NEAREST_MIPMAP_NEAREST;
		} else {
			minFilter = GL_NEAREST;
		}
	}

	GLint clamping;
	switch (options.EdgeMode)
	{
	case TextureEdgeMode::ClampToEdge:
		clamping = GL_CLAMP_TO_EDGE;
		break;
	case TextureEdgeMode::ClampToBorder:
		clamping = GL_CLAMP_TO_BORDER;
		break;
	default:
	case TextureEdgeMode::Repeat:
		clamping = GL_REPEAT;
		break;
	}

	if (inputFormat == GL_DEPTH_COMPONENT)
	{
		borderColour = Colour(1, 1, 1, 1);
	}

	glGenTextures(1, &glIndex);
	glBindTexture(target, glIndex);

	if (options.Type == TextureBufferType::Flat)
	{ 
		glTexImage2D(target, 0, format, std::max(1, (int)resolution.x), std::max(1, (int)resolution.y), 0, inputFormat, GL_FLOAT, startingTexture[0] == nullptr ? 0 : startingTexture[0]->Pixel(0, 0));
	}
	else if (options.Type == TextureBufferType::Cubemap)
	{
		for (size_t i = 0; i < 6; i++)
		{
			const float* data;
			if (startingTexture.size() < i + 1 || startingTexture[i] == nullptr)
			{
				data = nullptr;
			}
			else 
			{
				data = startingTexture[i]->Pixel(0, 0);
			}
				
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, resolution.x, resolution.y, 0, inputFormat, GL_FLOAT, data);
		}
	}
	else if (options.Type == TextureBufferType::Array)
	{
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, format, resolution.x, resolution.y, options.NumLayers);
	}

	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter);
	glTexParameteri(target, GL_TEXTURE_WRAP_R, clamping);
	glTexParameteri(target, GL_TEXTURE_WRAP_S, clamping);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, clamping);
	glTexParameterfv(target, GL_TEXTURE_BORDER_COLOR, (GLfloat*)&borderColour);
	if (options.UseShadowTest)
	{
		glTexParameteri(target, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(target, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
	}
	if (options.UseMipMap)
	{
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	if (options.UseAnisotropicFiltering)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 16);
	}

	glTarget = target;
}

std::string& Program::ReadShaderFile(const std::filesystem::path& filepath)
{
	using namespace std::filesystem;

	path canonicalPath = canonical(filepath);
	std::string pathString = canonicalPath.generic_string();

	if (shaderFileSources.contains(pathString)) {
		return shaderFileSources[pathString];
	}

	std::ifstream file(pathString);
	if (!file.is_open())
	{
		SDL_ShowSimpleMessageBox(0, "Error", (std::string("Error loading file: ") + pathString).c_str(), NULL);
	}

	std::string contents{ std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>() };
	return shaderFileSources[canonicalPath.generic_string()] = contents;
}

void Program::ClearShaderFiles()
{
	shaderFileSources.clear();
}

VertexArrayObject::VertexArrayObject()
{
	glGenVertexArrays(1, &glIndex);
	glBindVertexArray(glIndex);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(2, 2, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(3, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribFormat(4, 3, GL_FLOAT, GL_FALSE, 0);

	glVertexAttribBinding(0, 0);
	glVertexAttribBinding(1, 1);
	glVertexAttribBinding(2, 2);
	glVertexAttribBinding(3, 3);
	glVertexAttribBinding(4, 4);
}

void BindTextures(const int bindingOffset, const std::initializer_list<const TextureBuffer*>& textures)
{
	auto it = textures.begin();
	for (int i = bindingOffset; i < bindingOffset + textures.size(); i++)
	{
		if (*it != nullptr)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture((**it).GLTarget(), (**it).GLIndex());
		}
		it++;
	}
}

void BindTexturesByID(const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures, const int bindingOffset, const std::initializer_list<TextureID>& textureIDs)
{
	auto it = textureIDs.begin();
	for (int i = bindingOffset; i < bindingOffset + textureIDs.size(); i++)
	{
		if (bufferedTextures.find(*it) != bufferedTextures.end())
		{
			TextureBuffer* textureBuffer = bufferedTextures.at(*it).get();
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(textureBuffer->GLTarget(), textureBuffer->GLIndex());
		}
		it++;
	}
}