#include "Matrix.h"

const Matrix4 Matrix4::Identity = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

Matrix4 Matrix4::operator*(const float scalar) const
{
	Matrix4 out;

	for (int i = 0; i < 16; i++)
	{
		out.elements[i] = elements[i] * scalar;
	}

	return out;
}

Matrix4 Matrix4::operator+(const Matrix4& matrixB) const
{
	Matrix4 out;

	for (int i = 0; i < 16; i++)
	{
		out.elements[i] = elements[i] + matrixB.elements[i];
	}

	return out;
}

Matrix4 Matrix4::operator-(const Matrix4& matrixB) const
{
	Matrix4 out;

	for (int i = 0; i < 16; i++)
	{
		out.elements[i] = elements[i] - matrixB.elements[i];
	}

	return out;
}

//Copied shamelessly from https://stackoverflow.com/a/1148405/626796
Matrix4 Matrix4::Inverse() const
{
	std::array<float, 16> inv, invOut;
	float det;
	auto& m = elements;

	inv[0] = m[5] * m[10] * m[15] -
		m[5] * m[11] * m[14] -
		m[9] * m[6] * m[15] +
		m[9] * m[7] * m[14] +
		m[13] * m[6] * m[11] -
		m[13] * m[7] * m[10];

	inv[4] = -m[4] * m[10] * m[15] +
		m[4] * m[11] * m[14] +
		m[8] * m[6] * m[15] -
		m[8] * m[7] * m[14] -
		m[12] * m[6] * m[11] +
		m[12] * m[7] * m[10];

	inv[8] = m[4] * m[9] * m[15] -
		m[4] * m[11] * m[13] -
		m[8] * m[5] * m[15] +
		m[8] * m[7] * m[13] +
		m[12] * m[5] * m[11] -
		m[12] * m[7] * m[9];

	inv[12] = -m[4] * m[9] * m[14] +
		m[4] * m[10] * m[13] +
		m[8] * m[5] * m[14] -
		m[8] * m[6] * m[13] -
		m[12] * m[5] * m[10] +
		m[12] * m[6] * m[9];

	inv[1] = -m[1] * m[10] * m[15] +
		m[1] * m[11] * m[14] +
		m[9] * m[2] * m[15] -
		m[9] * m[3] * m[14] -
		m[13] * m[2] * m[11] +
		m[13] * m[3] * m[10];

	inv[5] = m[0] * m[10] * m[15] -
		m[0] * m[11] * m[14] -
		m[8] * m[2] * m[15] +
		m[8] * m[3] * m[14] +
		m[12] * m[2] * m[11] -
		m[12] * m[3] * m[10];

	inv[9] = -m[0] * m[9] * m[15] +
		m[0] * m[11] * m[13] +
		m[8] * m[1] * m[15] -
		m[8] * m[3] * m[13] -
		m[12] * m[1] * m[11] +
		m[12] * m[3] * m[9];

	inv[13] = m[0] * m[9] * m[14] -
		m[0] * m[10] * m[13] -
		m[8] * m[1] * m[14] +
		m[8] * m[2] * m[13] +
		m[12] * m[1] * m[10] -
		m[12] * m[2] * m[9];

	inv[2] = m[1] * m[6] * m[15] -
		m[1] * m[7] * m[14] -
		m[5] * m[2] * m[15] +
		m[5] * m[3] * m[14] +
		m[13] * m[2] * m[7] -
		m[13] * m[3] * m[6];

	inv[6] = -m[0] * m[6] * m[15] +
		m[0] * m[7] * m[14] +
		m[4] * m[2] * m[15] -
		m[4] * m[3] * m[14] -
		m[12] * m[2] * m[7] +
		m[12] * m[3] * m[6];

	inv[10] = m[0] * m[5] * m[15] -
		m[0] * m[7] * m[13] -
		m[4] * m[1] * m[15] +
		m[4] * m[3] * m[13] +
		m[12] * m[1] * m[7] -
		m[12] * m[3] * m[5];

	inv[14] = -m[0] * m[5] * m[14] +
		m[0] * m[6] * m[13] +
		m[4] * m[1] * m[14] -
		m[4] * m[2] * m[13] -
		m[12] * m[1] * m[6] +
		m[12] * m[2] * m[5];

	inv[3] = -m[1] * m[6] * m[11] +
		m[1] * m[7] * m[10] +
		m[5] * m[2] * m[11] -
		m[5] * m[3] * m[10] -
		m[9] * m[2] * m[7] +
		m[9] * m[3] * m[6];

	inv[7] = m[0] * m[6] * m[11] -
		m[0] * m[7] * m[10] -
		m[4] * m[2] * m[11] +
		m[4] * m[3] * m[10] +
		m[8] * m[2] * m[7] -
		m[8] * m[3] * m[6];

	inv[11] = -m[0] * m[5] * m[11] +
		m[0] * m[7] * m[9] +
		m[4] * m[1] * m[11] -
		m[4] * m[3] * m[9] -
		m[8] * m[1] * m[7] +
		m[8] * m[3] * m[5];

	inv[15] = m[0] * m[5] * m[10] -
		m[0] * m[6] * m[9] -
		m[4] * m[1] * m[10] +
		m[4] * m[2] * m[9] +
		m[8] * m[1] * m[6] -
		m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];	
	det = 1.0f / det;

	for (int i = 0; i < 16; i++)
		invOut[i] = inv[i] * det;
	return Matrix4(invOut);
}

Matrix4 Matrix4::FromEulerAngles(const Vector3& rotation)
{
	auto XRotation = Matrix4::Identity;
	XRotation.Element(1,1) = cosf(rotation.x);
	XRotation.Element(2,1) = -sinf(rotation.x);
	XRotation.Element(1,2) = sinf(rotation.x);
	XRotation.Element(2,2) = cosf(rotation.x);

	auto YRotation = Matrix4::Identity;
	YRotation.Element(0,0) = cosf(rotation.y);
	YRotation.Element(2,0) = sinf(rotation.y);
	YRotation.Element(0,2) = -sinf(rotation.y);
	YRotation.Element(2,2) = cosf(rotation.y);

	auto ZRotation = Matrix4::Identity;
	ZRotation.Element(0,0) = cosf(rotation.z);
	ZRotation.Element(1,0) = -sinf(rotation.z);
	ZRotation.Element(0,1) = sinf(rotation.z);
	ZRotation.Element(1,1) = cosf(rotation.z);

	return XRotation * YRotation * ZRotation;
}

//https://stackoverflow.com/a/1556470/626796
Matrix4 Matrix4::FromQuaternion(const Quaternion& q)
{
	Quaternion qn = q.Normalised();

	return{
		1.0f-2.0f*qn.y*qn.y-2.0f*qn.z*qn.z,	2.0f*qn.x*qn.y-2.0f*qn.z*qn.w,		2.0f*qn.x*qn.z+2.0f*qn.y*qn.w,		0.0f,
		2.0f*qn.x*qn.y+2.0f*qn.z*qn.w,		1.0f-2.0f*qn.x*qn.x-2.0f*qn.z*qn.z,	2.0f*qn.y*qn.z-2.0f*qn.x*qn.w,		0.0f,
		2.0f*qn.x*qn.z-2.0f*qn.y*qn.w,		2.0f*qn.y*qn.z+2.0f*qn.x*qn.w,		1.0f-2.0f*qn.x*qn.x-2.0f*qn.y*qn.y,	0.0f,
		0.0f,								0.0f,								0.0f,								1.0f
	};
}

Matrix4 Matrix4::MakeTranslation(const Vector3& translation)
{
	auto out = Matrix4::Identity;
	out.Element(3,0) = translation.x;
	out.Element(3,1) = translation.y;
	out.Element(3,2) = translation.z;
	return out;
}

Matrix4 Matrix4::MakeScale(const Vector3& scale)
{
	auto out = Matrix4::Identity;
	out.Element(0,0) = scale.x;
	out.Element(1,1) = scale.y;
	out.Element(2,2) = scale.z;
	return out;
}

Matrix4 Matrix4::MakeOrthographicProjection(const float nearClip, const float farClip, const Vector2& viewportScale)
{
	Matrix4 out = Matrix4::Identity;
	out.Element(0,0) = (2 / viewportScale.x);
	out.Element(1,1) = (2 / viewportScale.y);
	out.Element(2,2) = 1 / (nearClip - farClip);
	out.Element(3,2) = -nearClip / (nearClip - farClip);
	out.Element(3,3) = 1;

	return out;
}

Matrix4 Matrix4::MakePerspectiveProjection(const float nearClip, const float farClip, const float fovX, const float fovY)
{
	Matrix4 out = Matrix4::Identity;
	out.Element(0,0) = 1 / tanf(fovX / 2);
	out.Element(1,1) = 1 / tanf(fovY / 2);
	out.Element(2,2) = -farClip / (farClip - nearClip);
	out.Element(3,2) = -(farClip * nearClip) / (farClip - nearClip);
	out.Element(2,3) = -1;
	out.Element(3,3) = 0;

	return out;
}