#include <SDL.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <sstream>
#include <filesystem>
#include <unordered_map>
#include <random>
#include <cmath>
#include <list>
#include <optional>
#include <numbers>

#include "AssetLoader.h"
#include "Renderer.h"
#include "DoomReader.h"
#include "Simplex.h"
#include "Terrain.h"
#include "MeshBufferManager.h"
#include "Scene.h"
#include "Objects.h"
#include "Texture.h"
#include "Vector.h"
#include "Shared.h"

struct Main
{
	Main(const Vector2i resolution, SDL_Window* window);
	void SetUpScene();
	void SetUpCamera();
	void ClearScene();
	void SetUpRenderer();
	void HandleEvents();
	void Update(Scene& scene, const float delta, const int ticks);
	void MainLoop();
	void AddCube();
	void RemoveCube();
	
	SDL_Window* window;
	std::optional<Renderer> renderer;
	Camera camera;
	Vector3 cameraEulerRotation = Vector3(0, pi, 0);
	std::vector<PointLight> pointLights;
	std::vector<DirectionalLight> directionalLights;
	std::list<int> frameTimes;
	std::unique_ptr<Terrain> terrain;
	std::unique_ptr<Scene> scene;
	std::vector<Object*> cubes;

	bool quit = false;
	bool leftMouseDown = false;
	bool objectMovement = false;
	float targetFPS = 165;
	Vector2i resolution;
	float resolutionScale = 1.0f;	//If this is higher than 2 it won't filter properly
	int wireframeMode = 0;
	int outputGBuffer = 0;
	bool drawBoundingBoxes = false;
	bool colourChangedChunks = false;
	float averageFrameTime = 0;
	const int frameTimeAveragePeriod = 5000;
	int boundingBoxDepth = 0;
	bool stopped = false;
	bool stepOnce = false;
	std::mt19937 random;
};

#define TRY_SDL(function) if (function < 0) { std::cout << "SDL error (" << #function << "): " << SDL_GetError(); abort(); }

int main(int argc, char* argv[])
{
	SDL_Window* window = NULL;
	SDL_GLContext context = NULL;
	SDL_DisplayMode displayMode;

	TRY_SDL(SDL_Init(SDL_INIT_VIDEO))
	TRY_SDL(SDL_GetDesktopDisplayMode(0, &displayMode))
	const Vector2i resolution(displayMode.w, displayMode.h);

	window = SDL_CreateWindow("3D Renderer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, resolution.x, resolution.y, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (window == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Window creation error", SDL_GetError(), NULL);
		return 1;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	context = SDL_GL_CreateContext(window);
	SDL_GL_SetSwapInterval(0);

	Main main(resolution, window);
	main.MainLoop();

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}

Main::Main(const Vector2i resolution, SDL_Window* window)
	: resolution(resolution), window(window), camera("Camera"), renderer(std::in_place)
{
	SetUpCamera();
	SetUpScene();
	SetUpRenderer();
}

void Main::SetUpScene()
{
	const auto modelsDir = std::filesystem::current_path() / ".." / "glTF-Sample-Models" / "2.0" / "";
	scene = std::make_unique<Scene>();

	const std::filesystem::path sponzaFile(modelsDir / "Sponza" / "glTF" / "Sponza.gltf");
	Model& sponza = LoadFile(sponzaFile, *scene, renderer->render_data);

	const std::filesystem::path avocadoFile(modelsDir / "Avocado" / "glTF" / "Avocado.gltf");
	Model& avocado = LoadFile(avocadoFile, *scene, renderer->render_data);

	const std::filesystem::path sphereTestFile(modelsDir / "MetalRoughSpheres" / "glTF" / "MetalRoughSpheres.gltf");
	Model& spheres = LoadFile(sphereTestFile, *scene, renderer->render_data);

	scene->CreateObject(avocado.Name, &avocado);
	Object* spheresObject = scene->CreateObject(spheres.Name, &spheres);
	spheresObject->Position += 1.0f;
	spheresObject->Scale *= 0.8;

	Object* sponzaObject = scene->CreateObject(sponza.Name, &sponza);
	sponzaObject->Scale *= 0.05f;
	sponzaObject->Position = Vector3::Zero();

	terrain = std::unique_ptr<Terrain>(new Terrain(scene.get(), 1, renderer->render_data.Materials));
	camera.FarClip = terrain->maxChunkDistance * terrain->chunkSize;

	//Point lights

	{
		const auto lightFile = modelsDir / "Suzanne" / "glTF" / "Suzanne.gltf";
		Model& lightModel = LoadFile(lightFile, *scene, renderer->render_data);		
		Mesh& lightMesh = *lightModel.Meshes.front().mesh;

		constexpr float intensity = 2.0f;
		pointLights.emplace_back(*scene, renderer->render_data.Materials, "Light1", Colour(1, 1, 0.3f, 1), intensity, lightMesh);
		pointLights.back().object->Position = { -3, 2, -2 };
		pointLights.emplace_back(*scene, renderer->render_data.Materials, "Light2", Colour(0, 1, 1, 1), intensity, lightMesh);
		pointLights.back().object->Position = { -6, 1, -4 };
		pointLights.emplace_back(*scene, renderer->render_data.Materials, "Light3", Colour(1, 0.5f, 0.2f, 1), intensity, lightMesh);
		pointLights.back().object->Position = { 3, 3, 5 };
		pointLights.emplace_back(*scene, renderer->render_data.Materials, "Light4", Colour(0.8f, 0.8f, 1, 1), intensity, lightMesh);
		pointLights.back().object->Position = { 3, 1, -3 };
		directionalLights.emplace_back(*scene, renderer->render_data.Materials, "Sunlight", Colour(1, 1, 1, 1), 3.0f, Vector3(-1, -1, 1), lightMesh);

		//Random light rotations
		std::srand(static_cast<unsigned int>(std::time(NULL)));
		for (auto& light : pointLights)
		{
			Vector3 randomAxis = Vector3(((std::rand() % 200) - 100) / 50.0f, ((std::rand() % 200) - 100) / 50.0f, ((std::rand() % 200) - 100) / 50.0f);
			light.object->AngularVelocity = Quaternion(randomAxis, static_cast<float>(std::numbers::pi / 300.0));

			light.object->Scale *= 0.3f;
		}
	}

	float groundHeight = terrain->GetSimplexHeight(Vector2(0, 0));
	for (auto& object : scene->Objects)
	{
		object.model->RecalcAABB();
		object.Position += Vector3::Up() * (groundHeight + 5.0f);
		object.RecalcAABB();
		for (MeshInstance& instance : object.model->Meshes) {
			assert(instance.mesh->IsValid());
		}
	}

	//Doom levels
	//DoomReader::ReadWAD("Models\\DOOM.WAD", "E1M1", *scene, renderer->render_data);

	camera.Position.y = terrain->GetSimplexHeight(Vector2(camera.Position.x, camera.Position.z)) + 2;
}

void Main::SetUpCamera()
{
	camera.FovY = 80.0f * pi / 180.0f;
	camera.FovX = 2.0f * atanf((static_cast<float>(resolution.x) / static_cast<float>(resolution.y)) * tanf(camera.FovY / 2.0f));
	camera.NearClip = 0.1f;
	camera.FarClip = 100.0f;
	camera.Position = Vector3(0, -6, -4);
	camera.Rotation = Quaternion(Vector3::Up(), cameraEulerRotation.y);
}

void Main::ClearScene()
{
	scene.reset();
	pointLights.clear();
	directionalLights.clear();
	terrain.reset();
}

void Main::SetUpRenderer()
{
	renderer->OutputResolution = resolution;
	renderer->InternalResolution = resolution * resolutionScale;
	renderer->PointLightShadowResolution = { 500, 500 };
	renderer->DirectionalLightShadowResolution = { 2000, 2000 };
	renderer->InitOpenGL();

	const Texture* blueSky = renderer->render_data.Textures.New(1, 1, 3, Colour{ 0.8f, 0.9f, 1, 1 });
	std::vector<const Texture*> skyboxTexture(6, blueSky);
	renderer->BufferSkybox(skyboxTexture);

	std::vector<Mesh*> meshList(scene->CommonMeshes.size());
	std::transform(scene->CommonMeshes.begin(), scene->CommonMeshes.end(), meshList.begin(), [](Mesh& mesh) {return &mesh; });
	renderer->meshBufferManager->BufferMeshesIntoSingleBuffer(meshList);
	for (const Object* object : terrain->GetChunkObjects())
	{
		renderer->meshBufferManager->BufferMesh(*object->model->Meshes.front().mesh);
	}

	renderer->BufferTextures();
}

void Main::MainLoop()
{
	int lastFrameStart = SDL_GetTicks();
	int frameStart = lastFrameStart;

	using namespace std::chrono;
	auto start = system_clock::now();

	int frameCounter = 0;
	constexpr int maxFrames = -1;
	while(!quit && frameCounter != maxFrames)
	{
		frameCounter++;

		frameStart = SDL_GetTicks();

		//Update stuff
		HandleEvents();
		if (!stopped || stepOnce) {
			Update(*scene, 1000.0f / (frameStart - lastFrameStart), frameStart);

			//Render
			renderer->Render(*scene, pointLights, directionalLights, camera, wireframeMode, outputGBuffer, drawBoundingBoxes, boundingBoxDepth);
			SDL_GL_SwapWindow(window);

			stepOnce = false;
		}

		//Wait until it's time to render the next frame
		int frameEnd = SDL_GetTicks();
		int ticksToWait = std::max<int>(0, (1000 / static_cast<int>(targetFPS)) - (frameEnd - frameStart) - 1);
		SDL_Delay(ticksToWait);
		lastFrameStart = frameStart;
	}

	auto end = system_clock::now();
	auto duration = end - start;
	auto ms = duration_cast<milliseconds>(duration).count();
	std::cout << frameCounter << " frames completed in " << ms << "ms. " << (ms / frameCounter) << "ms per frame\n";

	ClearScene();
}

void Main::AddCube() {
	auto distribution = std::uniform_real_distribution<float>(-1.0f, 1.0f);
	auto object = scene->CreateObject("Generated Object", &*(std::next(scene->CommonModels.begin())));
	auto groundHeight = terrain->GetSimplexHeight({ 0, 0 });

	size_t size = DebugValue(30, 100);
	object->Position = Vector3(distribution(random), distribution(random), distribution(random)) * size + Vector3(0, groundHeight + (size / 2), 0);
	object->Rotation = Quaternion(Vector3(distribution(random), distribution(random), distribution(random)));
	//object->Velocity = Vector3(distribution(random), distribution(random), distribution(random));
	object->AngularVelocity = Quaternion(Vector3(distribution(random), distribution(random), distribution(random)) * 0.2f);
	object->RecentreOnModel();
	cubes.push_back(object);
}

void Main::RemoveCube() {
	if (!cubes.empty()) {
		auto distribution = std::uniform_int_distribution<>(0, cubes.size() - 1);
		size_t index = distribution(random);
		scene->Objects.Delete(cubes.at(index));
		cubes.at(index) = cubes.back();
		cubes.resize(cubes.size() - 1);
	}
}

void Main::HandleEvents()
{
	SDL_Event e;

	while (SDL_PollEvent(&e) != 0)
	{
		switch (e.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_MOUSEMOTION:
			if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
			{
				cameraEulerRotation += Vector3(-e.motion.yrel / 200.0f, -e.motion.xrel / 200.0f, 0);
				cameraEulerRotation.x = std::fminf(pi / 2, std::fmaxf(-pi / 2, cameraEulerRotation.x));
				camera.Rotation = Quaternion(Vector3::Up(), cameraEulerRotation.y) * Quaternion(Vector3::Left(), cameraEulerRotation.x);
			}
			else if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
			{
				if (!pointLights.empty())
				{
					pointLights.back().object->Position += camera.Rotation * Vector3(e.motion.xrel / 100.0f, -e.motion.yrel / 100.0f, 0);
				}
			}
			break;
		case SDL_MOUSEWHEEL:
			break;
		case SDL_KEYDOWN:
			switch (e.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				quit = true;
				break;
			case SDLK_f:
				objectMovement = !objectMovement;
				break;
			case SDLK_r:
				if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_LSHIFT])
				{
					ClearScene();
					SetUpScene();
				}

				scene->ObjectsInvalidated = true;
				renderer.emplace();
				SetUpRenderer();
				break;
			case SDLK_e:
				wireframeMode = (wireframeMode + 1) % 3;
				break;
			case SDLK_q:
				outputGBuffer = (outputGBuffer + 1) % 6;
				break;
			case SDLK_b:
				drawBoundingBoxes = !drawBoundingBoxes;
				break;
			case SDLK_t:
				colourChangedChunks = !colourChangedChunks;
				break;
			case SDLK_KP_PLUS:
				boundingBoxDepth++;
				break;
			case SDLK_KP_MINUS:
				boundingBoxDepth--;
				break;
			case SDLK_TAB:
				for (int i = 0; i < DebugValue(100, 10000); i++) {
					AddCube();
				}
				break;
			case SDLK_BACKSPACE:
				RemoveCube();
				break;
			case SDLK_p:
				stopped = !stopped;
				break;
			case SDLK_SEMICOLON:
				if (stopped) {
					stepOnce = true;
				}
				break;
			}
		}
	}

	const Uint8* keystate = SDL_GetKeyboardState(NULL);
	Vector3 cameraMovementDirection;
	float cameraSpeed = 0.3f;

	if (keystate[SDL_SCANCODE_W])
	{
		cameraMovementDirection.z--;
	}
	if (keystate[SDL_SCANCODE_S])
	{
		cameraMovementDirection.z++;
	}
	if (keystate[SDL_SCANCODE_A])
	{
		cameraMovementDirection.x--;
	}
	if (keystate[SDL_SCANCODE_D])
	{
		cameraMovementDirection.x++;
	}
	if (keystate[SDL_SCANCODE_SPACE])
	{
		cameraMovementDirection.y++;
	}
	if (keystate[SDL_SCANCODE_LCTRL])
	{
		cameraMovementDirection.y--;
	}
	if (keystate[SDL_SCANCODE_LSHIFT])
	{
		cameraSpeed = 10;
	}
	if (keystate[SDL_SCANCODE_LALT])
	{
		cameraSpeed = 1000;
	}

	if (cameraMovementDirection.Magnitude() != 0)
	{
		Vector3 normalisedCameraMovement = Vector3(cameraMovementDirection.x, 0, cameraMovementDirection.z).Normalised() * Quaternion(Vector3::Up(), cameraEulerRotation.y);
		cameraMovementDirection = normalisedCameraMovement + Vector3(0, cameraMovementDirection.y, 0);
		camera.Position += cameraMovementDirection * cameraSpeed;
		camera.Position.y = std::max(std::min(camera.Position.y, 2500.0f), -100.0f);
		if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
		{
			if (!pointLights.empty())
			{
				pointLights.back().object->Position += cameraMovementDirection * cameraSpeed;
			}
		}
	}
}

void Main::Update(Scene& scene, const float delta, const int ticks)
{
	if (!cubes.empty())
	{
		//size_t num_to_remove = (cubes.size() + 1) / 2;
		//for (int i = 0; i < num_to_remove; i++) {
		//	RemoveCube();
		//}

		//auto distribution = std::uniform_int_distribution<>(0, num_to_remove * 2);
		//auto num_to_add = distribution(random);
		//for (int i = 0; i < num_to_add; i++) {
		//	AddCube();
		//}
	}

	for (Object& object : scene.Objects)
	{
		if (objectMovement)
		{
			object.Position += object.Velocity;
			object.Rotation *= object.AngularVelocity;
			object.Rotation.Normalise();
		}
		object.RecalcAABB();
	}

	std::vector<Object*> changedChunksObjects = terrain->UpdateChunks(Vector2(camera.Position.x, camera.Position.z), 10000, colourChangedChunks);
	if (!changedChunksObjects.empty()) scene.ObjectsInvalidated = true;
	for (const Object* ref : changedChunksObjects)
	{
		Mesh& mesh = *ref->model->Meshes.front().mesh;
		renderer->meshBufferManager->UpdateMesh(mesh);
	}

	if (scene.ObjectsInvalidated)
	{
		scene.RebuildBVH();
		renderer->ObjectTransforms.resize(scene.Objects.Capacity());
	} else {
		scene.ObjectBVH.UpdateNodeBounds([](const Object* object) { return object->AABB; });
	}

	if (drawBoundingBoxes)
	{
		for (const auto& object : scene.Objects)
		{
			Renderer::DebugDraw draw;
			draw.colour = Colour(1, 0.8f, 0.8f, 1);
			draw.transform = object.MakeTransformMatrix();
			draw.primitive = object.model->AABB + object.ModelOffset;
			renderer->debugDraws.push_back(draw);

			draw.colour = Colour(1, 1, 1, 1);
			draw.transform = Matrix4::Identity;
			draw.primitive = object.AABB;
			renderer->debugDraws.push_back(draw);
		}
		for (const PointLight& light : pointLights)
		{
			Renderer::DebugDraw draw;
			draw.transform = Matrix4::Identity;
			draw.colour = Colour(1, 1, 0.3f, 1);
			draw.primitive = light.GetLightAreaAABB();
			renderer->debugDraws.push_back(draw);
		}
	}
}