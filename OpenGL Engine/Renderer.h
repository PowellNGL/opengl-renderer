#pragma once

#include <GL/glew.h>
#include <string>
#include <vector>
#include <SDL.h>
#include <memory>
#include <unordered_map>
#include <array>
#include <optional>
#include <filesystem>
#include <variant>

#include "Constants.h"
#include "Vector.h"
#include "Uniforms.h"
#include "RenderStages.h"
#include "RenderData.h"
#include "Line.h"

struct Mesh;
class Matrix;
struct Bounds;
class Object;
class PointLight;
class DirectionalLight;
class Camera;
class MeshBufferManager;
struct TextureBuffer;
struct Framebuffer;
struct Texture;

class Renderer
{
private:
	void InitShaders();
	void BuildBuffers();

public:
	RenderData render_data;

	std::unique_ptr<RenderStages> renderStages;
	std::unique_ptr<Uniforms> uniformBlocks;
	std::unique_ptr<MeshBufferManager> meshBufferManager;

	std::unique_ptr<Framebuffer> GBufferFBO;
	std::unique_ptr<TextureBuffer> GBuffers[3];
	std::unique_ptr<TextureBuffer> CameraDepthBuffer;
	
	std::unique_ptr<TextureBuffer> InternalColourBuffer;

	std::unique_ptr<Framebuffer> DebugFBO;

	std::unique_ptr<Framebuffer> LightingPassFBO;

	std::unique_ptr<Framebuffer> PointLightDepthFBO;
	std::unique_ptr<TextureBuffer> PointLightDepthBuffer;

	std::array<float, NUM_CASCADES + 1> CascadeBoundaries = { 0, 0.001f, 0.003f, 0.008f, 0.03f, 0.1f, 1};
	std::unique_ptr<Framebuffer> DirectionalLightDepthFBO;
	std::unique_ptr<TextureBuffer> DirectionalLightShadowCascades;

	std::unique_ptr<Framebuffer> BloomConvolutionFBO;
	std::unique_ptr<TextureBuffer> BloomDownsizeTextureBuffer;

	std::unique_ptr<Framebuffer> GaussianBlurFBO;
	std::unique_ptr<TextureBuffer> BloomBlurTextureBufferX;
	std::unique_ptr<TextureBuffer> BloomBlurTextureBufferY;

	std::vector<Matrix4> ObjectTransforms;

	std::unordered_map<int, std::unique_ptr<TextureBuffer>> BufferedTextures;
	std::unique_ptr<TextureBuffer> SkyboxBuffer;

	struct DebugDraw
	{
		Colour colour;
		Matrix4 transform;
		std::variant<Bounds, Frustum, Line> primitive;
	};

	std::vector<DebugDraw> debugDraws;

	Vector2 PointLightShadowResolution = { 1, 1 };
	Vector2 DirectionalLightShadowResolution = { 1, 1 };
	Vector2 InternalResolution = { 1, 1 };
	Vector2 OutputResolution = { 1, 1 };

	Colour ambientLight = Colour(0.2f, 0.2f, 0.2f, 1);

	Renderer()
	{
		// Make sure texture/material 0 is just a default
		render_data.Materials.New();
		render_data.Textures.New(0, 0, 3);
	}
	void InitOpenGL();
	void BufferTextures();
	void BufferSkybox(const std::vector<const Texture*>& textures);
	void Render(const Scene& scene, const std::vector<PointLight>& pointLights, const std::vector<DirectionalLight>& directionalLights, const Camera& camera, const int wireframeMode, const int bufferDrawMode, const bool drawBoundingBoxes, const int boundingBoxDepth);
};