#pragma once

#include "Vector.h"

struct Line
{
	Vector3 start;
	Vector3 end;
};