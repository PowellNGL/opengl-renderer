#pragma once

#include <list>
#include <memory>
#include <unordered_map>
#include <utility>

#include "Constants.h"
#include "Object.h"
#include "BVH.h"
#include "TypedArena.h"

class Model;
struct Mesh;

class Scene
{
public:
	bool ObjectsInvalidated{ true };

	TypedArena<Object> Objects;
	BVH<Object*> ObjectBVH;

	// TODO: Add a BVH iterator
	std::vector<Object*> GetAllIntersectingObjects(const BoundsTestable& collider) const {
		return ObjectBVH.GetAllIntersectingValues(collider);
	}

	//This isn't a definitive list, just the assets that need to be stored somewhere other than their owning class
	//Some objects (terrain and lights) manage their own models/meshes
	std::list<Model> CommonModels;
	std::list<Mesh> CommonMeshes;

	template<typename... ArgsT>
	Object* CreateObject(ArgsT&&... args)
	{
		ObjectsInvalidated = true;
		return Objects.New(std::forward<ArgsT>(args)...);
	}
	void RemoveObject(const ObjectID index)
	{
		ObjectsInvalidated = true;
		Objects.Delete(index);
	}
	void RemoveObject(Object* object)
	{
		ObjectsInvalidated = true;
		Objects.Delete(object);
	}

	void RebuildBVH()
	{
		std::vector<BVH<Object*>::NodeSetupInfo> setupInfo;
		for (Object& object : Objects) {
			setupInfo.emplace_back(&object, object.AABB);
		}
		ObjectBVH.Rebuild(setupInfo);
		ObjectsInvalidated = false;
	}

	Scene() = default;
	Scene(const Scene& other) = delete;
	Scene(Scene&& other) = delete;
	Scene& operator=(const Scene& other) = delete;
	Scene& operator=(Scene&& other) = delete;
};