#pragma once

#include <GL/glew.h>

#include "Wrappers.h"
#include "Vector.h"
#include "Matrix.h"
#include "Objects.h"
#include "Constants.h"
#include "Frustum.h"

class Uniforms
{
public:
#pragma pack(push, 1)
	struct Clip
	{
		static Clip Make(float near, float far)
		{
			Clip out;
			out.near = near;
			out.far = far;
			out.invFar = 1 / far;
			out.invLength = 1 / (far - near);
			return out;
		}

		float near;
		float far;
		float invFar;
		float invLength;
	};

	struct RenderUniforms
	{
		Vector2 ViewportResolution;
		int TimeMillis = 0;
	};

	struct CameraUniforms
	{
		Matrix4 ViewMatrix;
		Matrix4 InverseViewMatrix;
		Matrix4 RotationMatrix;
		Matrix4 InverseRotationMatrix;
		Matrix4 ProjectionMatrix;
		Matrix4 InverseProjectionMatrix;
		Clip clip;
		Vector2 FOV;
		Vector2 HalfSizeNearPlane;

		CameraUniforms(const Camera& camera) {
			ProjectionMatrix = camera.MakeProjectionMatrix();
			InverseProjectionMatrix = ProjectionMatrix.Inverse();
			ViewMatrix = camera.MakeViewMatrix();
			InverseViewMatrix = ViewMatrix.Inverse();
			RotationMatrix = Matrix4::FromQuaternion(camera.Rotation.Reversed());
			InverseRotationMatrix = Matrix4::FromQuaternion(camera.Rotation);
			FOV = { camera.FovX, camera.FovY };
			HalfSizeNearPlane = {std::tanf(FOV.x / 2), std::tanf(FOV.y / 2)};
			clip = Clip::Make(camera.NearClip, camera.FarClip);
		}
	};
	
	struct MaterialUniforms
	{
		Vector4 Albedo;
		Vector4 Emissivity;
		float Roughness;
		float Metallic;
		unsigned int IsSkybox = false;
		unsigned int HasAlbedoMap;
		unsigned int HasRoughnessMap;
		unsigned int HasMetallicMap;
		unsigned int HasNormalMap;
		unsigned int HasEmissivityMap;

		MaterialUniforms(const Material& material) {
			HasAlbedoMap = material.HasAlbedoMap();
			HasEmissivityMap = material.HasEmissivityMap();
			HasRoughnessMap = material.HasRoughnessMap();
			HasMetallicMap = material.HasMetallicMap();
			HasNormalMap = material.HasNormalMap();

			Albedo = { material.Albedo.r, material.Albedo.g, material.Albedo.b, material.Albedo.a };
			Roughness = material.Roughness;
			Metallic = material.Metallic;
			Emissivity = { material.Emissivity.r, material.Emissivity.g, material.Emissivity.b, 1.0f };
			IsSkybox = material.IsSkybox;
		}
	};

	struct PointLightUniforms
	{
		Clip clip;
		Vector4 Position;
		Vector4 LightColour;
		float LightIntensity;

		PointLightUniforms(const PointLight& light)
		{
			Position = Vector4(light.object->Position, 1.0f);
			LightColour = light.colour;
			LightIntensity = light.intensity;
			clip = Clip::Make(0.001f, light.LightRadius());
		}
	};

	struct DirectionalLightUniforms
	{
		struct CascadeInfo
		{
		public:
			Matrix4 Transform;
			Matrix4 Projection;
			Clip clip;
			Vector4 Position;
			Vector2 DepthMapScale;
			float Extent;
			float Bias;
		private:
			//Size needs to be a multiple of the largest member of the array to align properly.
			//Since mat4s count as arrays of vec4s for alignment purposes, the struct needs to align to a multiple of a vec4 (16 bytes)
			//char padding[8];
		};

		CascadeInfo cascadeInfo[NUM_CASCADES];
		Vector4 Direction;
		Vector4 LightColour;
		Vector2 InverseShadowMapResolution;
		float LightIntensity;

		DirectionalLightUniforms(const DirectionalLight& light, const Vector2i& shadowResolution, const std::array<float, NUM_CASCADES + 1>& cascadeBoundaries, const Camera& camera)
		{
			Direction = Vector4(-Vector3::Forward() * light.object->Rotation, 0);
			LightColour = light.colour;
			LightIntensity = light.intensity;
			InverseShadowMapResolution = 1 / shadowResolution;
			for (size_t i = 0; i < NUM_CASCADES; i++)
			{
				Camera tempCamera(camera);
				tempCamera.NearClip = camera.NearClip + (camera.FarClip - camera.NearClip) * cascadeBoundaries[i];
				tempCamera.FarClip = camera.NearClip + (camera.FarClip - camera.NearClip) * cascadeBoundaries[i + 1];
				Bounds lightAlignedBounds = tempCamera.MakeFrustum().GetBounds(Matrix4::FromQuaternion(light.object->Rotation.Reversed()));
				Vector3 boundsCentre = (lightAlignedBounds.Centre() * light.object->Rotation);
				Vector3 depthMapScale = Vector3(lightAlignedBounds.Width(), lightAlignedBounds.Height(),
					(boundsCentre - tempCamera.Position).Magnitude() + (camera.FarClip - camera.NearClip) * 2); //Z depth has to be large enough to contain all objects within maximum camera view distance or they won't cast a shadow from behind the camera

				DirectionalLightUniforms::CascadeInfo& info = cascadeInfo[i];
				info.Position = Vector4(boundsCentre, 1) - (Direction * (depthMapScale.z / 2));
				info.Transform = Matrix4::FromQuaternion(light.object->Rotation.Reversed()) * Matrix4::MakeTranslation(-info.Position);
				info.Projection = Matrix4::MakeOrthographicProjection(0, depthMapScale.z, Vector2(depthMapScale));
				info.clip = Clip::Make(0, depthMapScale.z);
				info.DepthMapScale = Vector2(depthMapScale.x / 2, depthMapScale.y / 2);
				info.Extent = cascadeBoundaries[i + 1];
				Vector2 pixelScale(depthMapScale.x / shadowResolution.x, depthMapScale.y / shadowResolution.y);
				info.Bias = std::max(pixelScale.x, pixelScale.y) * SHADOW_BIAS_MULTIPLIER;
			}
		}
	};
#pragma pack(pop)

	RenderUniforms renderUniforms;
	
	UniformBufferObject<RenderUniforms>				RenderUniformBuffer				{0, 1, GL_DYNAMIC_DRAW};
	UniformBufferObject<CameraUniforms>				CameraUniformBuffer				{1, 1, GL_DYNAMIC_DRAW};
	UniformBufferObject<MaterialUniforms>			MaterialUniformBuffer			{2, 1, GL_DYNAMIC_DRAW};
	UniformBufferObject<PointLightUniforms>			PointLightUniformBuffer			{3, 1, GL_DYNAMIC_DRAW};
	UniformBufferObject<DirectionalLightUniforms>	DirectionalLightUniformBuffer   {4, 1, GL_DYNAMIC_DRAW};

	void BufferRenderUniforms() const {
		RenderUniformBuffer.UploadAll(&renderUniforms);
	}

public:
	void SetRenderResolution(const Vector2& resolution)
	{
		renderUniforms.ViewportResolution = resolution;
		BufferRenderUniforms();
	}

	void SetCurrentTime(const unsigned int timeMillis)
	{
		renderUniforms.TimeMillis = timeMillis;
		BufferRenderUniforms();
	}
};