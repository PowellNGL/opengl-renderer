#pragma once

#include <vector>
#include <unordered_set>
#include "Vector.h"

namespace Triangulator
{
	struct PolyVert
	{
		int index;
		Vector2 position;
		std::unordered_set<PolyVert*> next;
		std::unordered_set<PolyVert*> prev;
	};

	std::vector<std::vector<int>> Triangulate(std::vector<PolyVert*>& verts);
	void ConnectOrRemoveLeaves(std::vector<PolyVert*>& verts);
	bool VertsAreConnected(const PolyVert& startVert, const PolyVert& endVert);
	void DisconnectVerts(PolyVert& startVert, PolyVert& endVert);
	void ConnectVerts(PolyVert& startVert, PolyVert& endVert);
	void RemoveIsolatedVerts(std::vector<PolyVert*>& verts);
	bool LineIntersectsPoly(const PolyVert& start, const PolyVert& end, const std::vector<PolyVert*>& verts);
	bool CheckLineIntersection(const Vector2& A, const Vector2& B, const Vector2& C, const Vector2& D);
	float RotationToPoint(const Vector2& origin, const Vector2& pointA, const Vector2& pointB);
	PolyVert& MostClockwiseOutgoingEdge(PolyVert& A, PolyVert& B);
	PolyVert& LeastClockwiseIncomingEdge(const PolyVert& B, const PolyVert& C);
	bool PointIsInsideCorner(const Vector2& cornerA, const Vector2& cornerB, const Vector2& cornerC, const Vector2& point);
	bool AnyPointInsideTriangle(const PolyVert& A, const PolyVert& B, const PolyVert& C, const std::vector<PolyVert*>& verts);
	void SwapEdgeStates(std::vector<PolyVert*>& edgesToSwap, std::vector<PolyVert*>& allVerts);
	void RemoveColinearVerts(std::vector<PolyVert*>& verts);
}