#pragma once

#include <memory>
#include <vector>

#include "Wrappers.h"
#include "Constants.h"
#include "Objects.h"
#include "Line.h"

struct Material;
class Uniforms;
struct Bounds;
class MeshBufferManager;
struct Mesh;
class Scene;
class Renderer;

//This class contains a reference to a single compiled instance of each shader used by the renderer.
//The renderer contains a reference to a single instance of this class, and can therefore access and call each shader through it
class RenderStages
{
protected:
	//I have divided up the rendering pipeline into broadly defined 'Stages', represented by a load of classes that inherit from the RenderStage class below. 
	//They each instantiate and call their own Program instance(s) according to their own needs
	//They also take in a reference to the uniform blocks available to each shader, so any stage can update these uniforms as necessary
	//Each RenderStage currently contains a Run() method taking in arguments it needs in order to perform its graphical duties
	//The idea is that I can extend each stage as necessary with Framebuffers/Texturebuffers/whatever that are exclusive to it and are buffered/destroyed along with the stage itself.
	//This should take some of the micromanagement out of the Renderer class.
	class RenderStage
	{
	protected:
		Renderer& renderer;
		Uniforms& uniformBlocks;
		const Mesh* last_drawn_mesh{ nullptr };
		RenderStage(Renderer& renderer, Uniforms& uniformBlocks) : renderer(renderer), uniformBlocks(uniformBlocks) {}

		void DrawMesh(const Mesh& mesh, const MeshBufferManager& manager);
	};

	class CopyTextureStage : RenderStage
	{
	private:
		Program program = Program("FullScreenQuad.vert", "", "CopyTexture.frag", "");
	public:
		CopyTextureStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const TextureBuffer& source, const Framebuffer* destination, const Vector2i& destinationResolution);
	};
	
	class MakeGBuffersStage : RenderStage
	{
	private:
		Program programNoAlpha = Program("Transforms.vert", "", "BuildGBuffers.frag", "");
		Program programAlpha = Program("Transforms.vert", "", "BuildGBuffers.frag", "", { { "ALPHA_TEXTURES_ENABLED", "" } });
		Material* wireframe_material;
	public:
		MakeGBuffersStage(Renderer& renderer, Uniforms& uniformBlocks);
		void Run(const Framebuffer& framebuffer,
			const Scene& scene, 
			const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures,
			MeshBufferManager& meshBufferer, 
			const Camera& camera,
			const Vector2i& resolution,
			const Colour& ambientLight,
			const int wireframeMode);
	};

	class LightingStage : RenderStage
	{
	private:
		Program directionalLightProgram = Program("FullScreenQuad.vert", "", "Lighting.frag", "", { { "DIRECTIONAL_LIGHT", "", }, {"LIGHT_CUTOFF_MULTIPLIER", std::to_string(PointLight::LIGHT_CUTOFF_MULTIPLIER) } });
		Program pointLightProgram = Program("FullScreenQuad.vert", "", "Lighting.frag", "", { { "POINT_LIGHT", "" }, { "LIGHT_CUTOFF_MULTIPLIER", std::to_string(PointLight::LIGHT_CUTOFF_MULTIPLIER) } });
	public:
		LightingStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const Vector2i& resolution,
			const Framebuffer& framebuffer,
			const TextureBuffer& albedoGBuffer,
			const TextureBuffer& smoothAndMetalGBuffer, 
			const TextureBuffer& normalGBuffer,
			const TextureBuffer& depthGBuffer,
			const TextureBuffer& pointLightShadowBuffer,
			const TextureBuffer& directionalLightShadowBuffer,
			const bool isPointLight);
	};

	class PointLightShadowMapStage : RenderStage
	{
	private:
		Program programNoAlpha = Program("PointLightVert.vert", "", "PointLightFrag.frag", "");
		Program programAlpha = Program("PointLightVert.vert", "", "PointLightFrag.frag", "", { { "ALPHA_TEXTURES_ENABLED", "" } });

	public:
		PointLightShadowMapStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const PointLight& light,
			const Vector2i& shadowResolution,
			const Framebuffer& framebuffer,
			const TextureBuffer& shadowMap,
			const Scene& scene,
			const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures,
			MeshBufferManager& meshBufferManager,
			const Camera& camera);
	};

	class DirectionalLightShadowMapStage : RenderStage
	{
	private:
		Program programNoAlpha = Program("DirectionalLightVert.vert", "", "DirectionalLightFrag.frag", "");
		Program programAlpha = Program("DirectionalLightVert.vert", "", "DirectionalLightFrag.frag", "", { { "ALPHA_TEXTURES_ENABLED", "" } });

	public:
		DirectionalLightShadowMapStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const DirectionalLight & light, 
			const Vector2i & shadowResolution,
			const std::array<float, NUM_CASCADES + 1>& cascadeBoundaries,
			const Framebuffer & framebuffer,
			const Scene& scene, 
			const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures,
			MeshBufferManager& meshBufferManager,
			const Camera& camera,
			const TextureBuffer& shadowMap);
	};

	class SkyboxStage : RenderStage
	{
	private:
		Program program = Program("FullScreenQuad.vert", "", "Skybox.frag", "");
	public:
		SkyboxStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const Framebuffer& GBufferFBO,
			const TextureBuffer& SkyboxCubemap,
			const Vector2i& resolution);
	};

	class BloomConvolutionStage : RenderStage
	{
	private:
		Program program = Program("FullScreenQuad.vert", "", "BloomConvolution.frag", "");
	public:
		BloomConvolutionStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const TextureBuffer& sourceTextureBuffer, 
			const TextureBuffer& convolutionsTextureBuffer,
			const Vector2i& sourceResolution, 
			const Framebuffer& framebuffer);
	};

	class GaussianBlurStage : RenderStage
	{
	private:
		Program program = Program("FullScreenQuad.vert", "", "GaussianBlur.frag", "");

	public:
		GaussianBlurStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const TextureBuffer& sourceTextureBuffer, 
			const int sourceTextureLevel,
			const Vector2i& sourceResolution,
			const TextureBuffer& targetTextureBufferX, 
			const TextureBuffer& targetTextureBufferY, 
			const Framebuffer& framebuffer);
	};

	class PostprocessingStage : RenderStage
	{
	private:
		Program program = Program("FullScreenQuad.vert", "", "PostProcessing.frag", "");
	public:
		PostprocessingStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const Vector2i& outputResolution, 
			const TextureBuffer& colourBuffer, 
			const TextureBuffer& depthBuffer, 
			const TextureBuffer& bloomBuffer);
	};

	class DrawLineStage : RenderStage
	{
	private:
		Program program = Program("DebugLineVert.vert", "", "DebugLineFrag.frag", "");
	public:
		DrawLineStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const Line& line, const Matrix4& transform, const Camera& camera,
			const Framebuffer& framebuffer, const Colour& colour);
	};

	class DrawBoundsStage : RenderStage
	{
	private:
		Program program = Program("DebugDrawBoundingBoxVert.vert", "DebugDrawBoundingBoxGeom.geom", "DebugDrawBoundingBoxFrag.frag", "");
	public:
		DrawBoundsStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {}
		void Run(const Bounds& bounds, const Matrix4& transform, const Camera& camera,
			const Framebuffer& framebuffer, const Colour& colour);
	};

public:
	CopyTextureStage CopyTexture;
	MakeGBuffersStage MakeGBuffers;
	LightingStage Lighting;
	PointLightShadowMapStage PointLightShadowMap;
	DirectionalLightShadowMapStage DirectionalLightShadowMap;
	SkyboxStage Skybox;
	DrawLineStage DrawLine;
	BloomConvolutionStage BloomConvolution;
	GaussianBlurStage GaussianBlur;
	PostprocessingStage Postprocessing;
	DrawBoundsStage DrawBounds;

	RenderStages(Renderer& renderer, Uniforms& uniformBlocks) :
		CopyTexture(renderer, uniformBlocks), 
		MakeGBuffers(renderer, uniformBlocks), 
		Lighting(renderer, uniformBlocks), 
		PointLightShadowMap(renderer, uniformBlocks),
		DirectionalLightShadowMap(renderer, uniformBlocks),
		Skybox(renderer, uniformBlocks),
		DrawLine(renderer, uniformBlocks),
		BloomConvolution(renderer, uniformBlocks),
		GaussianBlur(renderer, uniformBlocks),
		Postprocessing(renderer, uniformBlocks),
		DrawBounds(renderer, uniformBlocks) {}
};