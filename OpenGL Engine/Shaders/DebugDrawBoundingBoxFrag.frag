//! #include "ShaderHeader.glsl"

out vec4 fragColour;

layout (location = 3) uniform vec4 lineColour;

void main()
{
	fragColour = lineColour;
}