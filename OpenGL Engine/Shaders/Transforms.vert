//! #include "ShaderHeader.glsl"

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 binormal;

out vec3 eyeSpaceNormal;
out vec3 eyeSpacePosition;
out vec2 interpolatedTexCoord;
out vec3 eyeSpaceTangent;
out vec3 eyeSpaceBinormal;

layout (location = 0) uniform mat4 MV;
layout (location = 1) uniform mat4 MVP;

void main()
{
	interpolatedTexCoord = texCoord;
	gl_Position = MVP * vec4(pos, 1.0);
	eyeSpacePosition = (MV * vec4(pos, 1.0)).xyz;
	eyeSpaceNormal = normalize(MV * vec4(normal, 0.0)).xyz;
 	eyeSpaceTangent = (MV * vec4(tangent, 0.0)).xyz;
 	eyeSpaceBinormal = (MV * vec4(binormal, 0.0)).xyz;
}