//! #include "ShaderHeader.glsl"

#ifdef ALPHA_TEXTURES_ENABLED
layout (binding = 0) uniform sampler2D albedoSampler;

in vec2 finalTexCoord;
#endif

in float finalDepth;

void main()
{
	#ifdef ALPHA_TEXTURES_ENABLED
	if (texture(albedoSampler, finalTexCoord).w < 0.5)
		discard;
	#endif
	
	gl_FragDepth = finalDepth + length(vec2(dFdx(finalDepth), dFdy(finalDepth))) + 0.001f;
}