//! #include "ShaderHeader.glsl"

layout (points) in;
layout (line_strip, max_vertices = 24) out;

layout (location = 0) uniform vec3 minBounds;
layout (location = 1) uniform vec3 maxBounds;
layout (location = 2) uniform mat4 MVP;

void main()
{
	//Generate box vertices from bounds
	const vec4 v[8] = 
	{
		vec4(minBounds.x, minBounds.y, minBounds.z, 1),
		vec4(minBounds.x, minBounds.y, maxBounds.z, 1),
		vec4(maxBounds.x, minBounds.y, maxBounds.z, 1),
		vec4(maxBounds.x, minBounds.y, minBounds.z, 1),
		vec4(maxBounds.x, maxBounds.y, minBounds.z, 1),
		vec4(minBounds.x, maxBounds.y, minBounds.z, 1),
		vec4(minBounds.x, maxBounds.y, maxBounds.z, 1),
		vec4(maxBounds.x, maxBounds.y, maxBounds.z, 1)
	};

	////Generate triangles from verts
	//const vec4 t[36] = 
	//{
	//	v[0], v[2], v[1],
	//	v[0], v[3], v[2],

	//	v[0], v[1], v[6],
	//	v[0], v[6], v[5],

	//	v[1], v[2], v[7],
	//	v[1], v[7], v[6],

	//	v[2], v[3], v[4],
	//	v[2], v[4], v[7],

	//	v[3], v[0], v[5],
	//	v[3], v[5], v[4],

	//	v[5], v[6], v[7],
	//	v[5], v[7], v[4]
	//};

	//Generate lines from verts
	const vec4 l[24] =
	{
		v[0], v[1],
		v[1], v[2],
		v[2], v[3],
		v[3], v[0],

		v[0], v[5],
		v[1], v[6],
		v[2], v[7],
		v[3], v[4],

		v[5], v[6],
		v[6], v[7],
		v[7], v[4],
		v[4], v[5]
	};

	for (int i = 0; i < 24;	i += 2)
	{
		vec4 v0 = MVP * l[i + 0];
		vec4 v1 = MVP * l[i + 1];

		gl_Position = v0;
		EmitVertex();
		gl_Position = v1;
		EmitVertex();
		EndPrimitive();
	}
}