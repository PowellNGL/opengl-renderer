//! #include "ShaderHeader.glsl"

layout (location = 0) in vec3 pos;

layout (location = 0) uniform vec3 start;
layout (location = 1) uniform vec3 end;
layout (location = 2) uniform mat4 MVP;

void main()
{
	if (gl_VertexID == 0) 
	{
		gl_Position = MVP * vec4(start, 1);
	} 
	else
	{
		gl_Position = MVP * vec4(end, 1);
	}
}