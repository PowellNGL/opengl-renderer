//! #include "ShaderHeader.glsl"

layout (binding = 0) uniform sampler2D albedoSampler;
layout (binding = 1) uniform sampler2D roughnessSampler;
layout (binding = 2) uniform sampler2D metallicSampler;
layout (binding = 3) uniform sampler2D normalSampler;
layout (binding = 4) uniform sampler2D emissivitySampler;

in vec3 eyeSpacePosition;
in vec3 eyeSpaceNormal;
in vec2 interpolatedTexCoord;
in vec3 eyeSpaceTangent;
in vec3 eyeSpaceBinormal;

layout (location = 0) out vec3 albedo;
layout (location = 1) out vec2 roughnessAndMetallic;
layout (location = 2) out vec3 normal;
layout (location = 3) out vec3 initialColour;

layout (location = 2) uniform vec3 ambientLight;

vec3 applyNormalMap();

void main ()
{
	vec4 albedoSample;
    if (material.hasAlbedoMap)
	{
        albedoSample = texture(albedoSampler, interpolatedTexCoord);
    } else {
		albedoSample = material.albedo;
	}
	albedo = albedoSample.xyz;

	#ifdef ALPHA_TEXTURES_ENABLED
	if (albedoSample.w < 0.5)
	{
		discard;
	}
	#endif

	if (material.hasRoughnessMap)
	{
		roughnessAndMetallic.x = texture(roughnessSampler, interpolatedTexCoord).x;
	} else {
		roughnessAndMetallic.x = material.roughness;
	}

	if (material.hasMetallicMap)
	{
		roughnessAndMetallic.y = texture(metallicSampler, interpolatedTexCoord).x;
	} else {
		roughnessAndMetallic.y = material.metallic;
	}

	if (material.hasNormalMap)
	{
		normal = applyNormalMap();
	} else {
		normal = eyeSpaceNormal;
	}

    initialColour = albedo * ambientLight.xyz;

	if (material.hasEmissivityMap)
	{
		initialColour += texture(emissivitySampler, interpolatedTexCoord).xyz;
	} else {
		initialColour += material.emissivity.xyz;
	}
}

vec3 applyNormalMap()
{
	mat3 TangentToEyeSpace = mat3(vec3(eyeSpaceTangent.x, eyeSpaceBinormal.x, eyeSpaceNormal.x),
								  vec3(eyeSpaceTangent.y, eyeSpaceBinormal.y, eyeSpaceNormal.y),
								  vec3(eyeSpaceTangent.z, eyeSpaceBinormal.z, eyeSpaceNormal.z));
	vec3 textureNormal = texture(normalSampler, interpolatedTexCoord).xyz * 2 - 1;
    return normalize(textureNormal * TangentToEyeSpace);
}