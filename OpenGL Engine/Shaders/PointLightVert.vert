//! #include "ShaderHeader.glsl"

layout (location = 0) in vec3 pos;

#ifdef ALPHA_TEXTURES_ENABLED
layout (location = 2) in vec2 texCoord;

out vec2 finalTexCoord;
#endif

out float finalDepth;

layout (location = 0) uniform mat4 MV;
layout (location = 1) uniform mat4 MVP;

void main()
{
	#ifdef ALPHA_TEXTURES_ENABLED
	finalTexCoord = texCoord;
	#endif
	
	vec3 eyeSpacePosition = (MV * vec4(pos, 1)).xyz;
	gl_Position = MVP * vec4(pos, 1);
	finalDepth = length(eyeSpacePosition) * pointLight.clip.invFar;
}