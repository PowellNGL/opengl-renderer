//! #include "ShaderHeader.glsl"

layout (binding = 0) uniform sampler2D albedoBuffer;
layout (binding = 1) uniform sampler2D roughnessAndMetallicBuffer;
layout (binding = 2) uniform sampler2D normalBuffer;
layout (binding = 3) uniform sampler2D depthBuffer;
#ifdef POINT_LIGHT
layout (binding = 4) uniform samplerCubeShadow pointLightShadowBuffer;
#endif
#ifdef DIRECTIONAL_LIGHT
layout (binding = 5) uniform sampler2DArrayShadow directionalLightShadowCascades;
#endif

in vec3 viewVector;
out vec3 fragmentColour;

float LightRemainingAfterShadows();
void GetLightParameters();
void GetMaterialParameters();
vec3 TotalReflectedLight();
float NormalDistributionFunction(float NdotH, float roughness);
float GeometricShadowing(float NdotV, float NdotL, float roughness);
vec3 FresnelFunction(vec3 F0, float VdotH);
void debugDrawShadowCascadeMaps();
void debugLightShadowCascades();

//Everything is in eye space unless otherwise specified

//Values to be reconstructed from buffers
ivec2 screenPos;
vec3 albedo;
float roughness;
float metallic;
vec3 reflectance;
vec3 normal;
vec3 fragmentPosition;
float depth;

//Light parameters, read or calculated from uniforms
vec3 lightPosition;
vec3 lightVector;
vec3 lightColour;
float lightIntensity;
float lightDistance;
float attenuation;
Clip lightClip;

#ifdef DIRECTIONAL_LIGHT
int currentCascade = 0;
vec3 lightSpacePosition;
#endif

void main()
{
	//These are reconstructed before the rest of the G Buffer values so that the fragment can be discarded before doing any more calculations if it's too far from the light source
	screenPos = ivec2(gl_FragCoord.xy);

	float depthBufferDepth = texelFetch(depthBuffer, screenPos, 0).x;	
	if (depthBufferDepth == 1.0f) {
		discard;
	}

	float realDepth = (camera.projection[3][2] / (camera.projection[2][2] + depthBufferDepth)) + camera.clip.near * camera.clip.invLength;
	depth = realDepth * camera.clip.invFar;
	fragmentPosition = viewVector * realDepth;

	//Get parameters but also do early discard for lights outside the cutoff range
	GetLightParameters();

	//Unpack the G-Buffer
	GetMaterialParameters();
	
	//Do actual lighting calculations
	fragmentColour = TotalReflectedLight();	
	fragmentColour = max(fragmentColour, 0);
	fragmentColour *= LightRemainingAfterShadows();
}

vec3 TotalReflectedLight()
{	
	vec3 lightIrradiance = lightColour * lightIntensity * attenuation;
	
	vec3 viewVectorNormalised = normalize(-viewVector);
	vec3 halfwayVector = normalize(viewVectorNormalised - lightVector);
	float NdotV = abs(dot(normal, viewVectorNormalised));
	float NdotH = max(dot(normal, halfwayVector), 0.0f);
	float NdotL = max(dot(normal, -lightVector), 0.0f);
	float LdotH = max(dot(-lightVector, halfwayVector), 0.0f);
	
	//Combine the components of the Cook-Torrance model
	float D = NormalDistributionFunction(NdotH, roughness);
	vec3 F = FresnelFunction(reflectance, LdotH);
	float G = GeometricShadowing(NdotV, NdotL, roughness);
	vec3 specular = (D * F * G) / max((4 * NdotL * NdotV), 0.0001f);

	//TODO: try out the disney diffuse BRDF
	vec3 diffuse = (albedo / PI) * (1.0 - metallic);
	
	return (specular + diffuse) * lightIrradiance * NdotL;
}

float NormalDistributionFunction(float NdotH, float roughness)
{
	//GGX
	float a2 = max(0.005, roughness * roughness);	//Attempt to reduce unrealistically bright spots by limiting the smoothness
	float f = (NdotH * NdotH * (a2 - 1.0f)) + 1.0f;
	return a2 / (PI * f * f);
}

float G1(float dotValue, float roughness)
{
	//GGX
	float a2 = roughness * roughness;
	return (2 * dotValue) / (dotValue + sqrt(a2 + (1 - a2) * (dotValue * dotValue)));
}

float GeometricShadowing(float NdotV, float NdotL, float roughness)
{
	//Smith with GGX
	return G1(NdotL, roughness) * G1(NdotV, roughness);
}

vec3 FresnelFunction(vec3 F0, float VdotH)
{
	//Schlick
	return F0 + (vec3(1) - F0) * pow(1 - VdotH, 5);
}

void GetLightParameters()
{
	#ifdef POINT_LIGHT
		lightPosition = (camera.view * pointLight.position).xyz;
		lightDistance = distance(lightPosition, fragmentPosition);
		lightIntensity = pointLight.intensity;

		float lightCutoff = lightIntensity * LIGHT_CUTOFF_MULTIPLIER;
		if (lightDistance > lightCutoff)
			discard;

		//The light from a point light fades out until it is zero at (lightCutoffMultiplier * intensity).
		//If lightCutoffMultiplier is too small, the light will be cut off too quickly and look bad,
		//too large and fragments do lighting calculations when they are far enough from the source that the illumination is negligible
		//Setting it to a very large number is effectively the same as using the ordinary inverse square law
		attenuation = clamp(LIGHT_CUTOFF_MULTIPLIER / (lightDistance * lightDistance) - LIGHT_CUTOFF_MULTIPLIER / (lightCutoff * lightCutoff), 0, 1);
		lightVector = normalize(fragmentPosition - lightPosition);
		lightColour = pointLight.colour.xyz;
		lightClip = pointLight.clip;
	#endif
	
	#ifdef DIRECTIONAL_LIGHT
		for (int i = 0; i < NUM_CASCADES; i++)
		{
			if (depth <= directionalLight.cascade[i].extent)
			{
				currentCascade = i;
				break;
			}
		}
		lightVector = normalize((camera.view * directionalLight.direction).xyz);
		lightPosition = (camera.view * directionalLight.cascade[currentCascade].position).xyz;
		lightDistance = dot(normalize(fragmentPosition - lightPosition), lightVector) * distance(lightPosition, fragmentPosition);	//Distance to plane with directional light position on it
		lightSpacePosition = (directionalLight.cascade[currentCascade].transform * camera.inverseView * vec4(fragmentPosition, 1)).xyz;
		lightColour = directionalLight.colour.xyz;
		lightIntensity = directionalLight.intensity;
		lightClip = directionalLight.cascade[currentCascade].clip;
		attenuation = 1;
	#endif

}

void GetMaterialParameters()
{
	albedo = texelFetch(albedoBuffer, screenPos, 0).xyz;
	vec2 rm = texelFetch(roughnessAndMetallicBuffer, screenPos, 0).xy;
	roughness = rm.x;
	metallic = rm.y;
	reflectance = mix(vec3(0.04f), albedo, metallic);
	normal = texelFetch(normalBuffer, screenPos, 0).xyz;
}

#ifdef DIRECTIONAL_LIGHT
float sampleDirectionalShadowMap(const vec2 UV, const int cascade, const float testDistance)
{	
	return texture(directionalLightShadowCascades, vec4(UV, cascade,  min(testDistance, 0.99999f))).x;
}
#endif

float LightRemainingAfterShadows()
{	
	#ifdef POINT_LIGHT
		vec3 ray = (camera.inverseView * vec4(normalize(lightPosition - fragmentPosition), 0)).xyz;
		ray.x *= -1;
		float testDistance = (distance(lightPosition, fragmentPosition) * lightClip.invFar).x;
		return texture(pointLightShadowBuffer, vec4(ray, min(testDistance, 0.99999f))).x;
	#endif

	#ifdef DIRECTIONAL_LIGHT
		const float weights[7][7] =
		{
			{ 0.005084, 0.009377, 0.013539, 0.015302, 0.013539, 0.009377, 0.005084 },
			{ 0.009377, 0.017296, 0.024972, 0.028224, 0.024972, 0.017296, 0.009377 },
			{ 0.013539, 0.024972, 0.036054, 0.040749, 0.036054, 0.024972, 0.013539 },
			{ 0.015302, 0.028224, 0.040749, 0.046056, 0.040749, 0.028224, 0.015302 },
			{ 0.013539, 0.024972, 0.036054, 0.040749, 0.036054, 0.024972, 0.013539 },
			{ 0.009377, 0.017296, 0.024972, 0.028224, 0.024972, 0.017296, 0.009377 },
			{ 0.005084, 0.009377, 0.013539, 0.015302, 0.013539, 0.009377, 0.005084 }
		};
		vec2 depthMapScale = directionalLight.cascade[currentCascade].depthMapScale;
		vec2 pixelUVSize = directionalLight.inverseShadowMapResolution;
		vec2 shadowMapUV = (lightSpacePosition.xy / depthMapScale) / 2 + 0.5;
		float testDistance = (lightDistance - lightClip.near) * lightClip.invLength;
		float total = 0;
		for (int x = 0; x < 7; x++)
		{
			for (int y = 0; y < 7; y ++)
			{
				total += weights[x][y] * sampleDirectionalShadowMap(shadowMapUV + vec2(x - 3, y - 3) * pixelUVSize, currentCascade, testDistance);
			}
		}
		return total;
	#endif
}

#ifdef DIRECTIONAL_LIGHT
void debugDrawShadowCascadeMaps()
{
	int size = 400;
	if (screenPos.y < size && screenPos.x < size * NUM_CASCADES)
	{
		fragmentColour = vec3(texture(directionalLightShadowCascades, vec4((screenPos.x % size) / float(size), (screenPos.y % size) / float(size), screenPos.x / size, 0.9999f)).xxx);
	}
}

void debugLightShadowCascades()
{
	const vec4 cascadeColours[NUM_CASCADES] = { vec4(1, 0, 0, 1), vec4(0, 1, 0, 1), vec4(0, 0, 1, 1), vec4(1, 1, 0, 1), vec4(1, 0, 1, 1), vec4(0, 1, 1, 1) };
	fragmentColour *= cascadeColours[currentCascade].xyz / 2;
	fragmentColour += cascadeColours[currentCascade].xyz / 50;
}
#endif