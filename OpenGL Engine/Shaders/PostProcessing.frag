//! #include "ShaderHeader.glsl"

layout (binding = 0) uniform sampler2D screenBuffer;
layout (binding = 1) uniform sampler2D depthBuffer;
layout (binding = 2) uniform sampler2D bloomBuffer;

in vec2 screenUV;
in vec3 viewVector;

out vec3 fragColour;

layout (location = 0) uniform vec3 fogColour;

const float fogHeightBottom = 0;
const float fogHeightTop = 1000;

void main()
{
	vec3 inputColour = texture(screenBuffer, screenUV).xyz;
	float depthInWorldUnits = (camera.projection[3][2] / (camera.projection[2][2] + texture(depthBuffer, screenUV).x)) + camera.clip.near * camera.clip.invLength;
	vec3 fragPosEyeSpace = viewVector * depthInWorldUnits;
	float depth0to1 = depthInWorldUnits * camera.clip.invFar;
	float distance0to1 = min(length(fragPosEyeSpace) * camera.clip.invFar, 1);
	vec4 fragPosWorldSpace = camera.inverseView * vec4(fragPosEyeSpace, 1);

	//Fog
	float fogFactor = depth0to1 == 1 ? 0 : distance0to1;
	float fogHeightFactor = clamp((fragPosWorldSpace.y - fogHeightBottom) / fogHeightTop, 0, 1);
	fogFactor -= fogHeightFactor * (1 - fogFactor);
	fragColour = mix(inputColour, fogColour, clamp(fogFactor, 0, 1));

	//Add bloom texture on top
	fragColour += texture(bloomBuffer, screenUV).xyz;
}