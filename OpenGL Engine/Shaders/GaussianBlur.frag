//! #include "ShaderHeader.glsl"

layout (binding = 0) uniform sampler2D sourceSampler;

layout (location = 0) uniform vec2 sourceResolutionScale;
layout (location = 1) uniform bool xPass;

in vec2 screenUV;

out vec4 colour;

//const float kernel[7] = { 0.00598f, 0.060626f, 0.241843f, 0.383102f, 0.241843f, 0.060626f, 0.005
//const float kernel[11] = { 0.000003, 0.000229, 0.005977, 0.060598, 0.24173, 0.382925, 0.24173, 0.060598, 0.005977, 0.000229, 0.000003 };
const float kernel[31] = { 0.000091, 0.000225, 0.00052, 0.001131, 0.002313, 0.004443, 0.00802, 0.013605, 0.021687, 0.032488, 0.045734, 0.060499, 0.075207, 0.087854, 0.096442, 0.099487, 0.096442, 0.087854, 0.075207, 0.060499, 0.045734, 0.032488, 0.021687, 0.013605, 0.00802, 0.004443, 0.002313, 0.001131, 0.00052, 0.000225, 0.000091};

void main ()
{
	if (xPass)
	{
		for (int x = 0; x < 31; x++)
		{
			colour += texture(sourceSampler, screenUV + vec2(x - 15, 0) * sourceResolutionScale) * kernel[x];
		}
	} else {
		for (int y = 0; y < 31; y++)
		{
			colour += texture(sourceSampler, screenUV + vec2(0, y - 15) * sourceResolutionScale) * kernel[y];
		}
	}
}