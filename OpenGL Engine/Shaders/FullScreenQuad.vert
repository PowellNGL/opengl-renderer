//! #include "ShaderHeader.glsl"

out vec3 viewVector;
out vec2 screenUV;
out vec3 worldSpaceViewVector;	//These should be optimised away if the shader compiler realises they're not referenced in subsequent stages

const vec2 corners[3] = { vec2(-1, -1), vec2( 3, -1), vec2( -1, 3)};
const vec2 UVs[3] = { vec2(0, 0), vec2(2, 0), vec2(0, 2)};

void main()
{
	viewVector = vec3(camera.halfSizeNearPlane * corners[gl_VertexID].xy, -1);
	worldSpaceViewVector = (camera.inverseRotation * vec4(viewVector, 0)).xyz;
	screenUV = UVs[gl_VertexID];
	gl_Position = vec4(corners[gl_VertexID], 0, 1.0);
}