//! #include "ShaderHeader.glsl"

layout (binding = 5) uniform samplerCube skyboxSampler;

in vec3 viewVector;
in vec3 worldSpaceViewVector;

layout (location = 3) out vec4 initialColour;

void main ()
{
	initialColour = texture(skyboxSampler, worldSpaceViewVector);
}