//! #include "ShaderHeader.glsl"

layout (binding = 0) uniform sampler2D colourSampler;

layout (location = 0) uniform int sourceLevel;
layout (location = 1) uniform bool doThresholding;

out vec3 outputColour;

in vec2 screenUV;

void main ()
{
	outputColour = textureLod(colourSampler, screenUV, sourceLevel).xyz;
	if (doThresholding)
	{
		float luminance = dot(outputColour.rgb, vec3(0.2126f, 0.7152f, 0.0722f));
		outputColour = min(outputColour, vec3(1.3)) * step(1, luminance);
	}
}