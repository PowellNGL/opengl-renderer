//! #include "ShaderHeader.glsl"

layout (binding = 0) uniform sampler2D inputTexture;

out vec4 fragColour;

in vec2 screenUV;

void main()
{
	fragColour = vec4(texture(inputTexture, screenUV).xyz, 1);
} 