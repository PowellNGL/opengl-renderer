//This file is inserted at the top of all the other shaders to create a standard set of uniform blocks and constants for them to use.
//The 'include "ShaderHeader.glsl"' comment at the top of all the other shader files is there to tell the GLSL language integration VS extension that this file is required for syntax/error highlighting

#version 440

const float PI = 3.14159265358979323846f;
const int NUM_CASCADES = 6;

struct Clip
{
	float near;
	float far;
	float invFar;
	float invLength;
};

layout (binding = 0, std140, row_major) uniform RenderUniforms
{
	vec2 viewport;
	int timeMillis;
} render;

layout (binding = 1, std140, row_major) uniform CameraUniforms
{
	mat4 view;
	mat4 inverseView;
	mat4 rotation;
	mat4 inverseRotation;
	mat4 projection;
	mat4 inverseProjection;
	Clip clip;
	vec2 fov;
	vec2 halfSizeNearPlane;
} camera;

layout (binding = 2, std140, row_major) uniform MaterialUniforms
{
	vec4 albedo;
	vec4 emissivity;
	float roughness;
	float metallic;
	bool isSkybox;
	bool hasAlbedoMap;
	bool hasRoughnessMap;
	bool hasMetallicMap;
	bool hasNormalMap;
	bool hasEmissivityMap;
} material;

layout (binding = 3, std140, row_major) uniform PointLightUniforms
{
	Clip clip;
	vec4 position;
	vec4 colour;
	float intensity;
} pointLight;

struct DirectionalLightCascadeInfo
{
	mat4 transform;
	mat4 projection;
	Clip clip;
	vec4 position;
	vec2 depthMapScale;
	float extent;
	float bias;
};

layout (binding = 4, std140, row_major) uniform DirectionalLightUniforms
{
	DirectionalLightCascadeInfo cascade[NUM_CASCADES];
	vec4 direction;
	vec4 colour;
	vec2 inverseShadowMapResolution;
	float intensity;
} directionalLight;