//! #include "ShaderHeader.glsl"

layout (location = 0) out vec4 fragColour;

layout (location = 3) uniform vec4 colour;

void main ()
{
	fragColour = colour;
}