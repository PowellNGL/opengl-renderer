//! #include "ShaderHeader.glsl"

layout (location = 0) in vec3 pos;

#ifdef ALPHA_TEXTURES_ENABLED
layout (location = 2) in vec2 texCoord;

out vec2 finalTexCoord;
#endif

out float finalDepth;

layout (location = 0) uniform mat4 MV;
layout (location = 1) uniform mat4 MVP;
layout (location = 2) uniform int cascade;

void main()
{
	#ifdef ALPHA_TEXTURES_ENABLED
    finalTexCoord = texCoord;
	#endif

	vec4 eyeSpacePosition = MV * vec4(pos, 1);
	gl_Position = MVP * vec4(pos, 1);
	
	Clip clip = directionalLight.cascade[cascade].clip;
	finalDepth = (-eyeSpacePosition.z + directionalLight.cascade[cascade].bias + clip.near) * clip.invLength;
}