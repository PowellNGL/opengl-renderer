#include "Bounds.h"
#include "Matrix.h"

Vector3 Bounds::Centre() const
{
	return Vector3(
		v1.x + (v2.x - v1.x) / 2,
		v1.y + (v2.y - v1.y) / 2,
		v1.z + (v2.z - v1.z) / 2
	);
}

float Bounds::Width() const
{
	return v2.x - v1.x;
}

float Bounds::Height() const
{
	return v2.y - v1.y;
}

float Bounds::Depth() const
{
	return v2.z - v1.z;
}

Vector3 Bounds::Size() const
{
	return Vector3(Width(), Height(), Depth());
}

std::array<Vector3, 8> Bounds::AsVertList() const
{
	return std::array<Vector3, 8> {
		v1,
			Vector3(v1.x, v1.y, v2.z),
			Vector3(v2.x, v1.y, v2.z),
			Vector3(v2.x, v1.y, v1.z),
			Vector3(v2.x, v2.y, v1.z),
			Vector3(v1.x, v2.y, v1.z),
			Vector3(v1.x, v2.y, v2.z),
			v2 };
}

Vector3 Bounds::GetSingleVert(const unsigned int i) const
{
	switch (i)
	{
	default:
		return v1;
		break;
	case 1:
		return Vector3(v1.x, v1.y, v2.z);
		break;
	case 2:
		return Vector3(v2.x, v1.y, v2.z);
		break;
	case 3:
		return Vector3(v2.x, v1.y, v1.z);
		break;
	case 4:
		return Vector3(v2.x, v2.y, v1.z);
		break;
	case 5:
		return Vector3(v1.x, v2.y, v1.z);
		break;
	case 6:
		return Vector3(v1.x, v2.y, v2.z);
		break;
	case 7:
		return v2;
		break;
	}
}

bool Bounds::Intersects(const Bounds& other) const
{
	return !(other.v2.x <= v1.x ||
			 other.v1.x >= v2.x ||
			 other.v2.y <= v1.y ||
			 other.v1.y >= v2.y ||
			 other.v2.z <= v1.z ||
			 other.v1.z >= v2.z);
}

bool Bounds::Contains(const Vector3& point) const
{
	return (point.x >= v1.x &&
			point.x <= v2.x &&
			point.y >= v1.y &&
			point.y <= v2.y &&
			point.z >= v1.z &&
			point.z <= v2.z);
}

bool Bounds::Contains(const Bounds& other) const
{
	return (other.v1.x >= v1.x &&
			other.v1.y >= v1.y &&
			other.v1.z >= v1.z &&
			other.v2.x <= v2.x &&
			other.v2.y <= v2.y &&
			other.v2.z <= v2.z);
}

Bounds Bounds::Union(const Bounds& other) const
{
	return Bounds(Min(v1, other.v1), Max(v2, other.v2));
}

Vector3 Bounds::ConstrainPoint(const Vector3& point) const
{
	return Max(v1, Min(v2, point));
}

Axis Bounds::LongestAxis() const
{
	Vector3 diagonal = v2 - v1;
	if (diagonal.x > diagonal.y && diagonal.x > diagonal.z)
		return Axis::X;
	else if (diagonal.y > diagonal.z)
		return Axis::Y;
	else
		return Axis::Z;
}

float Bounds::Midpoint(const Axis axis) const
{
	switch (axis)
	{
	default:
	case Axis::X:
		return (v2.x + v1.x) * 0.5f;
	case Axis::Y:
		return (v2.y + v1.y) * 0.5f;
	case Axis::Z:
		return (v2.z + v1.z) * 0.5f;
	}
}

Bounds Bounds::Intersection(const Bounds& other) const
{
	return Bounds(Max(v1, other.v1), Min(v2, other.v2));
}

Bounds operator+(const Bounds& bounds, const Vector3& vector)
{
	return Bounds(bounds.v1 + vector, bounds.v2 + vector);
}

Bounds operator-(const Bounds& bounds, const Vector3& vector)
{
	return Bounds(bounds.v1 - vector, bounds.v2 - vector);
}

void operator+=(Bounds& bounds, const Vector3& vector)
{
	bounds.v1 += vector;
	bounds.v2 += vector;
}

void operator-=(Bounds& bounds, const Vector3& vector)
{
	bounds.v1 -= vector;
	bounds.v2 -= vector;
}

Bounds operator*(const Bounds& bounds, const float value)
{
	return Bounds(bounds.v1 * value, bounds.v2 * value);
}

Bounds operator/(const Bounds& bounds, const float value)
{
	return Bounds(bounds.v1 / value, bounds.v2 / value);
}

void operator*=(Bounds& bounds, const float value)
{
	bounds.v1 *= value;
	bounds.v2 *= value;
}

void operator/=(Bounds& bounds, const float value)
{
	bounds.v1 /= value;
	bounds.v2 /= value;
}

bool operator==(const Bounds& boundsA, const Bounds& boundsB)
{
	return boundsA.v1 == boundsB.v1 && boundsA.v2 == boundsB.v2;
}

bool operator!=(const Bounds& boundsA, const Bounds& boundsB)
{
	return boundsA.v1 != boundsB.v1 || boundsA.v2 != boundsB.v2;
}