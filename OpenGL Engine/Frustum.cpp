#include "Frustum.h"

std::array<Vector3, 8> Frustum::GetCorners() const
{
	Matrix4 InverseViewProj = ViewProjMatrix.Inverse();
	std::array<Vector3, 8> corners = ClipSpaceBounds.AsVertList();
	for (auto& corner : corners)
	{
		Vector4 unprojectedPoint = InverseViewProj * Vector4(corner, 1);
		corner = Vector3(unprojectedPoint) / unprojectedPoint.w;
	}

	return corners;
}

Bounds Frustum::GetBounds() const
{
	auto corners = GetCorners();
	Bounds bounds;
	bounds.v1 = bounds.v2 = corners[0];

	for (int i = 1; i < corners.size(); i++)
	{
		bounds.v1.x = std::min(bounds.v1.x, corners[i].x);
		bounds.v1.y = std::min(bounds.v1.y, corners[i].y);
		bounds.v1.z = std::min(bounds.v1.z, corners[i].z);
		bounds.v2.x = std::max(bounds.v2.x, corners[i].x);
		bounds.v2.y = std::max(bounds.v2.y, corners[i].y);
		bounds.v2.z = std::max(bounds.v2.z, corners[i].z);
	}

	return bounds;
}
Bounds Frustum::GetBounds(const Matrix4& transformation) const
{
	auto corners = GetCorners();	
	Bounds bounds;
	bounds.v1 = bounds.v2 = transformation * Vector4(corners[0], 1);

	for (int i = 1; i < corners.size(); i++)
	{
		Vector4 corner = transformation * Vector4(corners[i], 1);

		bounds.v1.x = std::min(bounds.v1.x, corner.x);
		bounds.v1.y = std::min(bounds.v1.y, corner.y);
		bounds.v1.z = std::min(bounds.v1.z, corner.z);
		bounds.v2.x = std::max(bounds.v2.x, corner.x);
		bounds.v2.y = std::max(bounds.v2.y, corner.y);
		bounds.v2.z = std::max(bounds.v2.z, corner.z);
	}

	return bounds;
}

//Can return true for some edge cases that don't actually intersect
bool Frustum::Intersects(const Bounds& bounds) const
{
	unsigned int planeTestResults[6] = { 0, 0, 0, 0, 0, 0 };	//-x, +x, -y, +y, -z, +z

	for (int i = 0; i < 8; i++)
	{
		//Look at each corner of the bounding box in turn
		Vector3 vert = bounds.GetSingleVert(i);

		Vector4 clipSpacePoint = ViewProjMatrix * Vector4(vert, 1);

		//Test the point against each frustum plane in clip space
		//If the point is 'outside' the plane, add 1 to the relevant entry in planeTestResults
		planeTestResults[0] += clipSpacePoint.x < -clipSpacePoint.w;
		planeTestResults[1] += clipSpacePoint.x > clipSpacePoint.w;
		planeTestResults[2] += clipSpacePoint.y < -clipSpacePoint.w;
		planeTestResults[3] += clipSpacePoint.y > clipSpacePoint.w;
		planeTestResults[4] += clipSpacePoint.z < 0;
		planeTestResults[5] += clipSpacePoint.z > clipSpacePoint.w;

		//This function works by checking all 6 planes of the frustum in clip space.
		//If all the verts of the bounding box we're checking are outside one of the planes
		//then we know the bounding box is outside clip space and we can return false.
		//Therefore, this part tries to reject the bounding box early by checking if there are any planes
		//for which all verts so far are outside it. If not, the box must be intersecting the frustum and we can return		
		if (!(planeTestResults[0] == i + 1 ||
			planeTestResults[1] == i + 1 ||
			planeTestResults[2] == i + 1 ||
			planeTestResults[3] == i + 1 ||
			planeTestResults[4] == i + 1 ||
			planeTestResults[5] == i + 1))
		{
			return true;
		}
	}

	return false;
}

bool Frustum::Contains(const Bounds& bounds) const
{
	for (int i = 0; i < 8; i++)
	{
		//For each corner of the bounding box
		Vector3 vert = bounds.GetSingleVert(i);

		//Transform that corner to clip space
		Vector4 clipSpacePoint = ViewProjMatrix * Vector4(vert, 1);

		//If any corner is outside the bounds of clip space, it is outside the frustum in world space
		//and therefore the bounds are not completely contained
		if (clipSpacePoint.x < -clipSpacePoint.w
			|| clipSpacePoint.x > clipSpacePoint.w
			|| clipSpacePoint.y < -clipSpacePoint.w
			|| clipSpacePoint.y > clipSpacePoint.w
			|| clipSpacePoint.z < 0
			|| clipSpacePoint.z > clipSpacePoint.w)
		{
			return false;
		}
	}

	return true;
}

bool Frustum::ContainedBy(const Bounds& bounds) const
{
	for (const Vector3& corner : GetCorners())
	{
		if (!bounds.Contains(corner))
		{
			return false;
		}
	}
	return true;
}

bool Frustum::Contains(const Vector3& point) const
{
	Vector4 clipSpacePoint = ViewProjMatrix * Vector4(point, 1);

	return (clipSpacePoint.x < -clipSpacePoint.w
		|| clipSpacePoint.x > clipSpacePoint.w
		|| clipSpacePoint.y < -clipSpacePoint.w
		|| clipSpacePoint.y > clipSpacePoint.w
		|| clipSpacePoint.z < 0
		|| clipSpacePoint.z > clipSpacePoint.w);
}