#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
#include <cstddef>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/pbrmaterial.h>
#include <assimp/material.h>
#include <SDL_image.h>

#include "RenderData.h"
#include "Scene.h"
#include "Mesh.h"

inline Texture* LoadTextureFromFile(const std::filesystem::path& filepath, RenderData& render_data)
{
	SDL_Surface* surface = IMG_Load(filepath.string().c_str());

	const SDL_PixelFormat* fmt = surface->format;
	int channels = fmt->Amask == 0 ? 3 : 4;
	auto format = fmt->Amask == 0 ? SDL_PIXELFORMAT_RGB24 : SDL_PIXELFORMAT_RGBA32;

	SDL_Surface* newSurface = SDL_ConvertSurfaceFormat(surface, format, 0);
	Texture* ref = render_data.Textures.New(surface->w, surface->h, channels, (const uint8_t*)newSurface->pixels);
	ref->FlipY();
	SDL_FreeSurface(newSurface);
	SDL_FreeSurface(surface);
	return ref;
}

inline Model& LoadFile(const std::filesystem::path& filepath, Scene& scene, RenderData& render_data)
{	
	Assimp::Importer importer;
	const aiScene* assimp_scene = importer.ReadFile(filepath.string(),
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_OptimizeMeshes |
		//aiProcess_GenNormals |
		aiProcess_RemoveRedundantMaterials |
		aiProcessPreset_TargetRealtime_MaxQuality
	);

	std::vector<MaterialID> materialIDMap;
	materialIDMap.resize(assimp_scene->mNumMaterials);

	std::unordered_map<std::string, Texture*> texture_map;
	for (size_t i_material = 0; i_material < assimp_scene->mNumMaterials; i_material++)
	{
		auto material = render_data.Materials.New();

		const aiMaterial* assimp_material = assimp_scene->mMaterials[i_material];

		materialIDMap[i_material] = render_data.Materials.ToIndex(material);

		for (size_t j = 1; j <= 18; j++)
		{
			aiString str;
			size_t k = 0;
			while (assimp_material->Get(_AI_MATKEY_TEXTURE_BASE, j, k++, str) == AI_SUCCESS)
			{
				std::cout << std::to_string(j) << " " << str.C_Str() << '\n';
			}
		}
		
		auto GetTexture = [&](aiTextureType type, unsigned int index) -> Texture*
		{
			aiString str;
			if (assimp_material->GetTexture(type, index, &str) == AI_SUCCESS)
			{
				auto path = filepath.parent_path() / str.C_Str();
				auto path_key = std::filesystem::canonical(path).string();

				auto it = texture_map.find(path_key);
				if (it == texture_map.end()) {
					Texture* ref = LoadTextureFromFile(path, render_data);
					it = texture_map.emplace(path_key, ref).first;
				}
				return it->second;
			}
			return nullptr;
		};

		Texture* albedo = GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_TEXTURE);
		if (albedo) {
			material->AlbedoMapID = render_data.Textures.ToIndex(albedo);
		}
		Texture* normal = GetTexture(aiTextureType_NORMALS, 0);
		if (normal) {
			material->NormalMapID = render_data.Textures.ToIndex(normal);
		}

		Texture* metal_rough = GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLICROUGHNESS_TEXTURE);
		if (metal_rough)
		{
			Texture* metallic = render_data.Textures.New(metal_rough->_width, metal_rough->_height, 1);
			Texture* roughness = render_data.Textures.New(metal_rough->_width, metal_rough->_height, 1);
			material->RoughnessMapID = render_data.Textures.ToIndex(roughness);
			material->MetallicMapID = render_data.Textures.ToIndex(metallic);

			for (size_t offset = 0; offset < metal_rough->_width * metal_rough->_height; offset++)
			{
				roughness->_pixels[offset] = metal_rough->_pixels[offset * metal_rough->_channels + 1];
				metallic->_pixels[offset] = metal_rough->_pixels[offset * metal_rough->_channels + 2];
			}
		}

		aiString name;
		if (assimp_material->Get(AI_MATKEY_NAME, name) == AI_SUCCESS) {
			material->Name = name.C_Str();
		}
	}

	Model& model = scene.CommonModels.emplace_back(filepath.filename().replace_extension().string());
	for (size_t i_mesh = 0; i_mesh < assimp_scene->mNumMeshes; i_mesh++)
	{
		Mesh& mesh = scene.CommonMeshes.emplace_back();
		const aiMesh* assimp_mesh = assimp_scene->mMeshes[i_mesh];
		for (size_t i_vert = 0; i_vert < assimp_mesh->mNumVertices; i_vert++) {
			const auto position = assimp_mesh->mVertices[i_vert];
			const auto normal = assimp_mesh->mNormals[i_vert];
			const aiVector3D uv = assimp_mesh->mTextureCoords[0] ? assimp_mesh->mTextureCoords[0][i_vert] : aiVector3D();
			const aiVector3D tangent = assimp_mesh->mTangents ? assimp_mesh->mTangents[i_vert] : aiVector3D();
			const aiVector3D binormal = assimp_mesh->mBitangents ? assimp_mesh->mBitangents[i_vert] : aiVector3D();

			mesh.AppendVertex(
				Vector3(position.x, position.y, position.z),
				Vector3(normal.x, normal.y, normal.z),
				Vector2(uv.x, uv.y),
				Vector3(tangent.x, tangent.y, tangent.z),
				Vector3(binormal.x, binormal.y, binormal.z)
			);
		}

		for (size_t i_tri = 0; i_tri < assimp_mesh->mNumFaces; i_tri++)
		{
			const auto& assimp_face = assimp_mesh->mFaces[i_tri];
			assert(assimp_face.mNumIndices == 3);
			mesh.AppendTriangle(assimp_face.mIndices[0], assimp_face.mIndices[1], assimp_face.mIndices[2]);
		}

		model.Meshes.emplace_back(&model, &mesh);
		model.Meshes.back().material = &render_data.Materials[materialIDMap[assimp_mesh->mMaterialIndex]];
	}
	model.RecalcAABB();
	return model;
}