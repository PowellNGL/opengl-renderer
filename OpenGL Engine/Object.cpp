#include "Object.h"
#include "Mesh.h"
#include "Scene.h"

//Move the object's origin to the centre of all its verts, without changing its apparent position
void Object::RecentreOnModel()
{
	Vector3 modelCentre = Vector3::Zero();

	float numVerts = 0;
	for (MeshInstance& meshInstance : model->Meshes)
	{
		Mesh& mesh = *meshInstance.mesh;

		for (Vector3& vert : mesh.Verts)
		{
			modelCentre += vert;
			numVerts++;
		}
	}

	if (numVerts != 0) {
		modelCentre /= numVerts;
		ModelOffset = -modelCentre;
		Position += modelCentre;
	}
}

Matrix4 Object::MakeTransformMatrix() const
{
	return Matrix4::MakeTranslation(Position) * Matrix4::FromQuaternion(Rotation) * Matrix4::MakeScale(Scale);
}

Matrix4 Object::MakeModelTransformMatrix() const
{
	return Matrix4::MakeTranslation(Position) * Matrix4::FromQuaternion(Rotation) * Matrix4::MakeScale(Scale) * Matrix4::MakeTranslation(ModelOffset);
}

//Use the vertices of the local bounds to get an imprecise axis-aligned bounding box for the object
Bounds& Object::RecalcAABB()
{
	Bounds adjustedModelBounds = model->AABB + ModelOffset;
	Matrix4 transform = MakeTransformMatrix();
	AABB.v1 = AABB.v2 = transform * Vector4(adjustedModelBounds.v1, 1);

	for (const Vector3& vert : adjustedModelBounds.AsVertList())
	{
		Vector3 transformedVert = Vector3(transform * Vector4(vert, 1));
		AABB.v1 = Min(AABB.v1, transformedVert);
		AABB.v2 = Max(AABB.v2, transformedVert);
	}

	return AABB;
}

Bounds Object::GetBounds(const std::vector<Object*>& Objects)
{
	if (Objects.empty())
		return Bounds();

	Bounds output = Objects.front()->AABB;

	for (auto object : Objects)
	{
		output = output.Union(object->AABB);
	}

	return output;
}