#pragma once

#include <cmath>
#include <array>

#include "Constants.h"
#include "Colour.h"

constexpr float pi = 3.14159265358979323846f;

struct Vector2;
struct Vector3;
struct Vector4;
struct Matrix4;
struct Vector2i;

struct Vector2
{
public:
	static constexpr inline Vector2 Zero() { return { 0, 0 }; }

	float x, y;

	constexpr Vector2() : x(0), y(0) {}
	constexpr Vector2(const float x, const float y) : x(x), y(y) {}
	Vector2(const Vector3& vec3);
	Vector2(const Vector4& vec4);
	Vector2(const Vector2i& iVec2);

	float Magnitude() const;
	Vector2 Normalised() const;
};

Vector2 operator+ (const Vector2& vecA, const Vector2& vecB);
void operator+= (Vector2& vecA, const Vector2& vecB);
Vector2 operator- (const Vector2& vecA, const Vector2& vecB);
void operator-= (Vector2& vecA, const Vector2& vecB);
Vector2 operator- (const Vector2& vec);
Vector2 operator* (const Vector2& vec, const float multiplier);
Vector2 operator* (const float multiplier, const Vector2& vec);
void operator*= (Vector2& vec, const float multiplier);
Vector2 operator/ (const Vector2& vec, const float divisor);
void operator/= (Vector2& vec, const float divisor);
Vector2 operator/(const float f, const Vector2& vec);
void operator /=(const float f, Vector2& vec);
bool operator== (const Vector2& vecA, const Vector2& vecB);
bool operator!= (const Vector2& vecA, const Vector2& vecB);

float Cross(const Vector2& vecA, const Vector2& vecB);
float Dot(const Vector2& vecA, const Vector2& vecB);
Vector2 Max(const Vector2& vecA, const Vector2& vecB);
Vector2 Min(const Vector2& vecA, const Vector2& vecB);

struct Vector3
{
public:
	static constexpr inline Vector3 Left()		{ return { 1, 0, 0 }; }
	static constexpr inline Vector3 Up()		{ return { 0, 1, 0 }; }
	static constexpr inline Vector3 Forward()	{ return { 0, 0, 1 }; }
	static constexpr inline Vector3 Zero() { return { 0, 0, 0 }; }
	static constexpr inline Vector3 One()		{ return { 1, 1, 1 }; }

	float x, y, z;

	constexpr Vector3() : x(0), y(0), z(0) {}
	constexpr Vector3(const float x, const float y, const float z) : x(x), y(y), z(z) {}
	constexpr Vector3(const Vector2& vec2, const float z) : x(vec2.x), y(vec2.y), z(z) {}
	Vector3(const Vector4& vec4);

	float Magnitude() const;
	float MagnitudeSquared() const;
	void Normalise();
	Vector3 Normalised() const;
	Vector3 ProjectOntoPlane(const Vector3& normal) const;
	Vector3 ProjectOntoVector(const Vector3& vector) const;
	Vector3 GetOrthogonal() const;
	static Vector3 CalcNormal(const Vector3& vecA, const Vector3& vecB, const Vector3& vecC);

	float operator[](const Axis& axis) const;
};

Vector3 ClipLineX(const Vector3& lineStart, const Vector3& lineEnd, float xBound);
Vector3 ClipLineY(const Vector3& lineStart, const Vector3& lineEnd, float yBound);
Vector3 operator+ (const Vector3& vecA, const Vector3& vecB);
void operator+= (Vector3& vecA, const Vector3& vecB);
Vector3 operator- (const Vector3& vecA, const Vector3& vecB);
void operator-= (Vector3& vecA, const Vector3& vecB);
Vector3 operator- (const Vector3& vec);
Vector3 operator* (const Vector3& vec, float multiplier);
Vector3 operator* (float multiplier, const Vector3& vec);
void operator*= (Vector3& vec, float multiplier);
Vector3 operator/ (const Vector3& vec, float divisor);
void operator/= (Vector3& vec, float divisor);
Vector3 operator+ (const Vector3& vec, float value);
void operator+= (Vector3& vec, float value);
Vector3 operator- (const Vector3& vec, float divisor);
void operator-= (Vector3& vec, float value);
bool operator== (const Vector3& vecA, const Vector3& vecB);
bool operator!= (const Vector3& vecA, const Vector3& vecB);

Vector3 Cross(const Vector3& vecA, const Vector3& vecB);
float Dot(const Vector3& vecA, const Vector3& vecB);
Vector3 CartesianToBarycentric(const Vector3& p, const Vector3& a, const Vector3& b, const Vector3& c);
Vector3 BarycentricToCartesian(const Vector3& p, const Vector3& a, const Vector3& b, const Vector3& c);
Vector3 Max(const Vector3& vecA, const Vector3& vecB);
Vector3 Min(const Vector3& vecA, const Vector3& vecB);

struct Vector4
{
public:
	float x, y, z, w;

	constexpr Vector4() = default;
	constexpr Vector4(const float x, const float y, const float z, const float w) : x(x), y(y), z(z), w(w) {}
	constexpr Vector4(const Vector3& vector3, const float w) : x(vector3.x), y(vector3.y), z(vector3.z), w(w) {}
	constexpr Vector4(const Colour& colour) : x(colour.r), y(colour.g), z(colour.b), w(colour.a) {}

	float Magnitude() const;
	Vector4 Normalised() const;
};

Vector4 operator+ (const Vector4& vecA, const Vector4& vecB);
void operator+= (Vector4& vecA, const Vector4& vecB);
Vector4 operator- (const Vector4& vecA, const Vector4& vecB);
void operator-= (Vector4& vecA, const Vector4& vecB);
Vector4 operator- (const Vector4& vec);
Vector4 operator* (const Vector4& vec, const float multiplier);
Vector4 operator* (const float multiplier, const Vector4& vec);
void operator*= (Vector4& vec, const float multiplier);
Vector4 operator/ (const Vector4& vec, const float divisor);
void operator/= (Vector4& vec, const float divisor);
bool operator== (const Vector4& vecA, const Vector4& vecB);
bool operator!= (const Vector4& vecA, const Vector4& vecB);

float Dot(const Vector4& vecA, const Vector4& vecB);
Vector4 Max(const Vector4& vecA, const Vector4& vecB);
Vector4 Min(const Vector4& vecA, const Vector4& vecB);

struct Vector2i
{
public:
	int x;
	int y;

	constexpr Vector2i() : x(0), y(0) {};
	constexpr Vector2i(const int x, const int y) : x(x), y(y) {};
	constexpr Vector2i(const Vector2& vec) : x(static_cast<int>(vec.x)), y(static_cast<int>(vec.y)) {};
};

bool operator== (const Vector2i& vecA, const Vector2i& vecB);
bool operator!= (const Vector2i& vecA, const Vector2i& vecB);
Vector2i operator+ (const Vector2i& vecA, const Vector2i& vecB);
void operator+= (Vector2i& vecA, const Vector2i& vecB);
Vector2i operator- (const Vector2i& vecA, const Vector2i& vecB);
void operator-= (Vector2i& vecA, const Vector2i& vecB);
Vector2i operator- (const Vector2i& vec);
Vector2i operator* (const Vector2i& vec, const float multiplier);
Vector2i operator* (const float multiplier, const Vector2i& vec);
void operator*= (Vector2i& vec, const float multiplier);
Vector2i operator/ (const Vector2i& vec, const float divisor);
void operator/= (Vector2i& vec, const float divisor);