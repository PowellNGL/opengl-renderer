#pragma once

#include <vector>
#include <type_traits>
#include <algorithm>
#include <cassert>
#include <set>
#include <array>
#include <memory>
#include <compare>

template<typename T>
struct TypedArena
{
private:
	static constexpr size_t BufferSize = 1024;
	static constexpr size_t BufferSizeBytes = BufferSize * sizeof(T);
	using Buffer = std::array<std::byte, BufferSizeBytes>;
	using BufferPtr = std::unique_ptr<Buffer>;
	
	struct Gap
	{
		size_t start;
		size_t size;
	};

	std::vector<BufferPtr> buffers;
	std::vector<Gap> gaps;

	std::byte* indexToBytePointer(size_t index) const
	{
		auto& buffer = buffers[index / BufferSize];
		return buffer->data() + ((index % BufferSize) * sizeof(T));
	}

public:
	TypedArena() = default;
	TypedArena(const TypedArena&) = delete;
	TypedArena(TypedArena&&) = delete;
	TypedArena& operator=(const TypedArena&) = delete;
	TypedArena& operator=(TypedArena&&) = delete;

	std::byte* Allocate()
	{
		if (gaps.empty())
		{
			size_t newIndex = buffers.size() * BufferSize;
			auto& newBuffer = buffers.emplace_back(std::make_unique<Buffer>());
			gaps.emplace_back(Gap{ newIndex + 1, BufferSize - 1 });
			return newBuffer->data();
		}

		auto& gap = gaps.front();
		size_t index = gap.start;
		if (gap.size > 1) {
			gap.start++;
			gap.size--;
		} else {
			gaps.erase(gaps.begin());
		}
		return indexToBytePointer(index);
	}

	template<typename... ArgsT>
	T* New(ArgsT&& ...args)
	{
		std::byte* ptr = Allocate();
		return new(ptr) T(std::forward<ArgsT>(args)...);
	}

	void Delete(size_t index)
	{
		operator[](index).~T();

		auto indexCompare = [](const Gap& gap, const size_t& index) {
			return gap.start < index;
		};
		auto gapIt = std::lower_bound(gaps.begin(), gaps.end(), index, indexCompare);
		assert(gapIt->start != index);

		Gap newGap{ index, 1 };
		gaps.emplace(gapIt, newGap);
	}

	void Delete(T* ptr)
	{
		ptr->~T();
		size_t index = ToIndex(ptr);

		auto indexCompare = [](const Gap& gap, const size_t& index) {
			return gap.start < index;
		};
		auto gapIt = std::lower_bound(gaps.begin(), gaps.end(), index, indexCompare);
		assert(gapIt->start != index);

		Gap newGap{ index, 1 };
		gaps.emplace(gapIt, newGap);
	}

	T& operator[](size_t index)
	{
		return *reinterpret_cast<T*>(indexToBytePointer(index));
	}
	const T& operator[](size_t index) const
	{
		return *reinterpret_cast<const T*>(indexToBytePointer(index));
	}

	size_t ToIndex(const T* ptr) const
	{
		const std::byte* bytePtr = reinterpret_cast<const std::byte*>(ptr);
		for (size_t bufferIndex = 0; bufferIndex < buffers.size(); bufferIndex++)
		{
			const auto& buffer = buffers[bufferIndex];
			if (bytePtr >= buffer->data() && bytePtr < buffer->data() + BufferSizeBytes)
			{
				return bufferIndex * BufferSize + ((bytePtr - buffer->data()) / sizeof(T));
			}
		}
		return -1;
	}

	size_t Capacity() const
	{
		return buffers.size() * BufferSize;
	}

	template<bool ConstT>
	struct Iterator
	{
		friend struct TypedArena;

		using iterator_concept = std::forward_iterator_tag;
		using iterator_category = std::forward_iterator_tag;
		using value_type = std::conditional_t<ConstT, const T, T>;
		using pointer = value_type*;
		using reference = value_type&;

		using arena_t = const TypedArena;
		using buffer_iterator = typename std::vector<BufferPtr>::const_iterator;
		using gap_iterator = typename std::vector<Gap>::const_iterator;

		struct Sentinel {};

	private:
		arena_t& _arena;
		size_t _current;
		gap_iterator _nextGapIt;

		Iterator(arena_t& arena) : 
			_arena(arena),
			_nextGapIt(_arena.gaps.begin()),
			_current(0)
		{			
			maybeSkipGap();
		}

		void maybeSkipGap()
		{
			size_t end = _arena.buffers.size() * BufferSize;
			while (_current < end && _current == _nextGapIt->start)
			{
				_current += _nextGapIt->size;
				_nextGapIt++;
			}
		}

		void advance()
		{
			_current++;
			maybeSkipGap();
		}

	public:
		Iterator& operator++()
		{
			advance();
			return *this;
		}
		Iterator operator++(int)
		{
			advance();
			Iterator out(*this);
			return out;
		}
		bool operator==(const Sentinel& s)
		{
			size_t end = _arena.buffers.size() * BufferSize;
			return _current == end;
		}
		reference operator*()
		{
			return const_cast<reference>(_arena[_current]);
		}
		pointer operator->()
		{
			return const_cast<pointer>(&_arena[_current]);
		}
	};

	using iterator = Iterator<false>;
	using const_iterator = Iterator<true>;

	friend struct Iterator<false>;
	friend struct Iterator<true>;

	iterator begin() { return iterator(*this); }
	typename iterator::Sentinel end() { return {}; }
	const_iterator begin() const { return const_iterator(*this); }
	typename const_iterator::Sentinel end() const { return {}; }
};