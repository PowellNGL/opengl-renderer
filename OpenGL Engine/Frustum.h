#pragma once

#include "BoundsTestable.h"
#include "Matrix.h"
#include "Bounds.h"

struct Frustum : public BoundsTestable
{
public:
	Matrix4 ViewProjMatrix;

	constexpr Frustum(const Matrix4& viewProjMatrix) : ViewProjMatrix(viewProjMatrix) {}
	
	std::array<Vector3, 8> GetCorners() const;
	Bounds GetBounds() const;
	Bounds GetBounds(const Matrix4& transformation) const;

	static constexpr Bounds ClipSpaceBounds = Bounds(-1, -1, 0, 1, 1, 1);

	bool Intersects(const Bounds& bounds) const override;
	bool Contains(const Bounds& bounds) const override;
	bool ContainedBy(const Bounds& bounds) const override;
	bool Contains(const Vector3& point) const override;

	bool Intersects(const Frustum& other) const
	{
		Frustum otherInLocalSpace = Frustum(ViewProjMatrix * other.ViewProjMatrix.Inverse());
		return otherInLocalSpace.Intersects(ClipSpaceBounds);
	}
};