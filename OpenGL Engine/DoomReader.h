#pragma once

#include <vector>
#include <array>
#include <unordered_set>
#include <unordered_map>
#include <string>

#include "Vector.h"
#include "RenderData.h"

class Object;
class Scene;
struct Texture;
struct Material;
struct Colour;

namespace DoomReader
{
	struct Rect
	{
		float x1;
		float y1;
		float x2;
		float y2;
	};

	struct DirectoryEntry
	{
		int filepos;
		int size;
		std::string name;
	};

	struct Thing
	{
		int x;
		int y;
		int angle;
		int type;
		int flags;
	};

	struct Linedef
	{
		int startVertex;
		int endVertex;
		int flags;
		int specialType;
		int sectorTag;
		int rightSidedef;
		int leftSidedef;
	};

	struct Sidedef
	{
		Vector2 offset;
		std::string upperTexture;
		std::string lowerTexture;
		std::string middleTexture;
		int sectorNumber;
	};
	
	struct Seg
	{
		int startVertex;
		int endVertex;
		int angle;
		int linedef;
		int direction;
		int offset;
	};

	struct Sector
	{
		int floorHeight;
		int ceilingHeight;
		std::string floorTexture;
		std::string ceilingTexture;
		int lightLevel;
		int type;
		int tagNumber;
	};

	struct Node
	{
		float x;
		float y;
		float dX;
		float dY;
		Rect rightBoundingBox;
		Rect leftBoundingBox;
		int rightChild;
		int leftChild;
	};

	struct Subsector
	{
		int segCount;
		int firstSegNumber;
	};

	struct Map
	{
		std::vector<Thing> Things;
		std::vector<Linedef> Linedefs;
		std::vector<Sidedef> Sidedefs;
		std::vector<Vector2> Vertexes;
		std::vector<Seg> Segs;
		std::vector<Sector> Sectors;
		std::vector<Node> Nodes;
		std::vector<Subsector> Subsectors;
	};

	struct MapPatch
	{
		int OriginX;
		int OriginY;
		int PictureID;
	};

	struct MapTexture
	{
		std::string Name;
		int Width;
		int Height;
		std::vector<MapPatch> Patches;
	};

	struct Picture
	{
		int leftOffset;
		int topOffset;
		Texture* texture;
	};

	void ReadWAD(const std::string& filename, const std::string& mapToLoad, Scene& scene, RenderData& render_data);

	int32_t ReadInt32(const std::vector<char>& buffer, const int offset);
	int16_t ReadInt16(const std::vector<char>& buffer, const int offset);
	uint8_t ReadUInt8(const std::vector<char>& buffer, const int offset);
	DirectoryEntry ReadDirectoryEntry(const std::vector<char>& buffer, const int offset);
	std::string ReadString(const std::vector<char>& buffer, const int offset, const int length);

	inline bool IsMapLump(const DirectoryEntry & entry);
}