#pragma once

#include <vector>
#include "Material.h"
#include "Bounds.h"

class Model;
struct Mesh;

struct MeshInstance
{
	Model* parent;
	Mesh* mesh;
	Material* material = nullptr;
	bool isVisible = true;
	bool isShadowCaster = true;

	MeshInstance(Model* parent, Mesh* mesh) : parent(parent), mesh(mesh) {}
	MeshInstance(Model* parent, Mesh* mesh, Material* material) : parent(parent), mesh(mesh), material(material) {}
};

class Model
{
private:
	static unsigned int nextID;
	unsigned int id;

public:
	std::string Name;	
	std::vector<MeshInstance> Meshes;
	Bounds AABB;

	constexpr unsigned int ID() const { return id; }

	Model() : id(nextID++) {}
	Model(const std::string& name) : id(nextID++), Name(name) {}
	Model(const Model& other)
	{
		*this = other;
	};
	Model(Model&& other) noexcept
	{
		*this = std::move(other);
	};
	Model& operator= (const Model& other) 
	{
		id = nextID++;
		Name = other.Name;
		Meshes = other.Meshes;
		AABB = other.AABB;
		return *this;
	}
	Model& operator= (Model&& other) noexcept
	{
		id = other.id;
		other.id = -1;
		Name = other.Name;
		Meshes = std::move(other.Meshes);
		AABB = std::move(other.AABB);
		return *this;
	}

	void RecalcAABB();
	bool HasMeshes() const;
	bool HasVerts() const;
};