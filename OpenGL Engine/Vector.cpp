#include "Vector.h"
#include "Matrix.h"

Vector3 ClipLineX(const Vector3& lineStart, const Vector3& lineEnd, const float xBound)
{
	float ratio = (xBound - lineStart.x) / (lineEnd.x - lineStart.x);
	Vector3 finalVector = lineStart + (lineEnd - lineStart) * ratio;
	finalVector.x = xBound;
	return finalVector;
}

Vector3 ClipLineY(const Vector3& lineStart, const Vector3& lineEnd, const float yBound)
{
	float ratio = (yBound - lineStart.y) / (lineEnd.y - lineStart.y);
	Vector3 finalVector = lineStart + (lineEnd - lineStart) * ratio;
	finalVector.y = yBound;
	return finalVector;
}

Vector3 operator+ (const Vector3& vecA, const Vector3& vecB)
{
	return{ vecA.x + vecB.x, vecA.y + vecB.y, vecA.z + vecB.z };
}

void operator+= (Vector3& vecA, const Vector3& vecB)
{
	vecA = vecA + vecB;
}

Vector3 operator- (const Vector3& vecA, const Vector3& vecB)
{
	return{ vecA.x - vecB.x, vecA.y - vecB.y, vecA.z - vecB.z };
}

void operator-= (Vector3& vecA, const Vector3& vecB)
{
	vecA.x -= vecB.x;
	vecA.y -= vecB.y;
	vecA.z -= vecB.z;
}

Vector3 operator- (const Vector3& vec)
{
	return{ -vec.x, -vec.y, -vec.z };
}

Vector3 operator* (const Vector3& vec, const float multiplier)
{
	return{ vec.x * multiplier, vec.y * multiplier, vec.z * multiplier };
}

Vector3 operator*(const float multiplier, const Vector3& vec)
{
	return vec * multiplier;
}

void operator*= (Vector3& vec, const float multiplier)
{
	vec = vec * multiplier;
}

Vector3 operator/ (const Vector3& vec, const float divisor)
{
	return{ vec.x / divisor, vec.y / divisor, vec.z / divisor };
}

void operator/= (Vector3& vec, const float divisor)
{
	vec = vec / divisor;
}

Vector3 operator+(const Vector3& vec, float value)
{
	return Vector3(vec.x + value, vec.y + value, vec.z + value);
}

Vector3 operator-(const Vector3& vec, float value)
{
	return Vector3(vec.x - value, vec.y - value, vec.z - value);
}

void operator-=(Vector3& vec, const float value)
{
	vec = vec - value;
}

void operator+=(Vector3& vec, const float value)
{
	vec = vec + value;
}

bool operator==(const Vector3& vecA, const Vector3& vecB)
{
	return vecA.x == vecB.x && vecA.y == vecB.y && vecA.z == vecB.z;
}

bool operator!=(const Vector3& vecA, const Vector3& vecB)
{
	return vecA.x != vecB.x || vecA.y != vecB.y || vecA.z != vecB.z;
}

Vector3 Cross(const Vector3& vecA, const Vector3& vecB)
{
	return{
		vecA.y * vecB.z - vecA.z * vecB.y,
		vecA.z * vecB.x - vecA.x * vecB.z,
		vecA.x * vecB.y - vecA.y * vecB.x
	};
}

float Dot(const Vector3& vecA, const Vector3& vecB)
{
	return	vecA.x * vecB.x +
			vecA.y * vecB.y +
			vecA.z * vecB.z;
}

Vector3 CartesianToBarycentric(const Vector3& p, const Vector3& a, const Vector3& b, const Vector3& c)
{
	Vector3 output;

	//https://en.wikipedia.org/wiki/Barycentric_coordinate_system#Conversion_between_barycentric_and_Cartesian_coordinates

	float det = (b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y);
	output.x = ((b.y - c.y) * (p.x - c.x) + (c.x - b.x) * (p.y - c.y)) / det;
	output.y = ((c.y - a.y) * (p.x - c.x) + (a.x - c.x) * (p.y - c.y)) / det;
	output.z = 1 - output.x - output.y;

	return output;
}

Vector2 operator+(const Vector2& vecA, const Vector2& vecB)
{
	return{ vecA.x + vecB.x, vecA.y + vecB.y };
}

void operator+=(Vector2& vecA, const Vector2& vecB)
{
	vecA.x += vecB.x;
	vecA.y += vecB.y;
}

Vector2 operator-(const Vector2& vecA, const Vector2& vecB)
{
	return{ vecA.x - vecB.x, vecA.y - vecB.y };
}

void operator-=(Vector2& vecA, const Vector2& vecB)
{
	vecA.x -= vecB.x;
	vecA.y -= vecB.y;
}

Vector2 operator-(const Vector2& vec)
{
	return{ -vec.x, -vec.y };
}

Vector2 operator*(const Vector2& vec, const float multiplier)
{
	return{ vec.x * multiplier, vec.y * multiplier };
}

Vector2 operator*(const float multiplier, const Vector2& vec)
{
	return{ vec.x * multiplier, vec.y * multiplier };
}

void operator*=(Vector2& vec, const float multiplier)
{
	vec.x *= multiplier;
	vec.y *= multiplier;
}

Vector2 operator/(const Vector2& vec, const float divisor)
{
	return{ vec.x / divisor, vec.y / divisor };
}

void operator/=(Vector2& vec, const float divisor)
{
	vec.x /= divisor;
	vec.y /= divisor;
}

Vector2 operator/(const float f, const Vector2& vec)
{
	return { f / vec.x, f / vec.y };
}

void operator /=(const float f, Vector2& vec)
{
	vec = { f / vec.x, f / vec.y };
}

bool operator==(const Vector2& vecA, const Vector2& vecB)
{
	return vecA.x == vecB.x && vecA.y == vecB.y;
}

bool operator!=(const Vector2& vecA, const Vector2& vecB)
{
	return vecA.x != vecB.x || vecA.y != vecB.y;
}

//Essentially just a 3D cross product with the vertical component set to zero
float Cross(const Vector2& vecA, const Vector2& vecB)
{
	return vecA.x * vecB.y - vecA.y * vecB.x;
}

float Dot(const Vector2& vecA, const Vector2& vecB)
{
	return vecA.x * vecB.x + vecA.y * vecB.y;
}

Vector2 Max(const Vector2& vecA, const Vector2& vecB)
{
	return Vector2(
		vecA.x > vecB.x ? vecA.x : vecB.x,
		vecA.y > vecB.y ? vecA.y : vecB.y);
}

Vector2 Min(const Vector2& vecA, const Vector2& vecB)
{
	return Vector2(
		vecA.x < vecB.x ? vecA.x : vecB.x,
		vecA.y < vecB.y ? vecA.y : vecB.y);
}

Vector2::Vector2(const Vector3& vec3)
{
	x = vec3.x;
	y = vec3.y;
}

Vector2::Vector2(const Vector4& vec4)
{
	x = vec4.x;
	y = vec4.y;
}

Vector2::Vector2(const Vector2i& iVec2)
{
	x = static_cast<float>(iVec2.x);
	y = static_cast<float>(iVec2.y);
}

float Vector2::Magnitude() const
{
	return sqrt(x * x + y * y);
}

Vector2 Vector2::Normalised() const
{
	float magnitude = Magnitude();
	if (magnitude == 0)
	{
		return *this;
	} else {
		return Vector2(x / magnitude, y / magnitude);
	}
}

Vector3::Vector3(const Vector4& vec4)
{
	x = vec4.x;
	y = vec4.y;
	z = vec4.z;
}

float Vector3::Magnitude() const
{
	return sqrt(x * x + y * y + z * z);
}

float Vector3::MagnitudeSquared() const
{
	return x * x + y * y + z * z;
}

void Vector3::Normalise()
{
	float magnitude = Magnitude();
	if (magnitude == 0)
	{
		return;
	} else {
		*this /= magnitude;
	}
}

Vector3 Vector3::Normalised() const
{
	float magnitude = Magnitude();
	if (magnitude == 0)
	{
		return Vector3(x, y, z);
	}
	else
	{
		return *this / magnitude;
	}
}

Vector3 Vector3::ProjectOntoPlane(const Vector3& normal) const
{
	return *this - (Dot(*this, normal.Normalised()) * normal.Normalised());
}

Vector3 Vector3::ProjectOntoVector(const Vector3& vector) const
{
	return vector.Normalised() * Dot(*this, vector.Normalised());
}

Vector3 Vector3::GetOrthogonal() const
{
	if (z != 0)
	{
		return Vector3(1, 1, (x + y) / -z).Normalised();
	}
	else if (y != 0)
	{
		return Vector3(1, (x + z) / -y, 1).Normalised();
	}
	else
	{
		return Vector3((y + z) / -x, 1, 1).Normalised();
	}
}

Vector3 BarycentricToCartesian(const Vector3& p, const Vector3& a, const Vector3& b, const Vector3& c)
{
	return
	{
		a.x * p.x + b.x * p.y + c.x * p.z,
		a.y * p.x + b.y * p.y + c.y * p.z,
		0
	};
}

Vector3 Max(const Vector3& vecA, const Vector3& vecB)
{
	return Vector3(
		vecA.x > vecB.x ? vecA.x : vecB.x,
		vecA.y > vecB.y ? vecA.y : vecB.y,
		vecA.z > vecB.z ? vecA.z : vecB.z);
}

Vector3 Min(const Vector3& vecA, const Vector3& vecB)
{
	return Vector3(
		vecA.x < vecB.x ? vecA.x : vecB.x,
		vecA.y < vecB.y ? vecA.y : vecB.y,
		vecA.z < vecB.z ? vecA.z : vecB.z);
}

Vector3 Vector3::CalcNormal(const Vector3& vecA, const Vector3& vecB, const Vector3& vecC)
{
	return Cross(vecB - vecA, vecC - vecA).Normalised();
}

float Vector3::operator[](const Axis& axis) const
{
	switch (axis) {
	default:
	case Axis::X:
		return x;
	case Axis::Y:
		return y;
	case Axis::Z:
		return z;
	}
}

Vector4 operator+(const Vector4& vecA, const Vector4& vecB)
{
	return{ vecA.x + vecB.x, vecA.y + vecB.y, vecA.z + vecB.z, vecA.w + vecB.w };
}

void operator+=(Vector4& vecA, const Vector4& vecB)
{
	vecA.x += vecB.x;
	vecA.y += vecB.y;
	vecA.z += vecB.z;
	vecA.w += vecB.w;
}

Vector4 operator-(const Vector4& vecA, const Vector4& vecB)
{
	return{ vecA.x - vecB.x, vecA.y - vecB.y, vecA.z - vecB.z, vecA.w - vecB.w };
}

void operator-=(Vector4& vecA, const Vector4& vecB)
{
	vecA.x -= vecB.x;
	vecA.y -= vecB.y;
	vecA.z -= vecB.z;
	vecA.w -= vecB.w;
}

Vector4 operator-(const Vector4& vec)
{
	return{ -vec.x, -vec.y, -vec.z, -vec.w };
}

Vector4 operator*(const Vector4& vec, const float multiplier)
{
	return{ vec.x * multiplier, vec.y * multiplier, vec.z * multiplier, vec.w * multiplier };
}

Vector4 operator*(const float multiplier, const Vector4& vec)
{
	return{ vec.x * multiplier, vec.y * multiplier, vec.z * multiplier, vec.w * multiplier };
}

void operator*=(Vector4& vec, const float multiplier)
{
	vec.x *= multiplier;
	vec.y *= multiplier;
	vec.z *= multiplier;
	vec.w *= multiplier;
}

Vector4 operator/(const Vector4& vec, const float divisor)
{
	return{ vec.x / divisor, vec.y / divisor, vec.z / divisor, vec.w / divisor };
}

void operator/=(Vector4& vec, const float divisor)
{
	vec.x /= divisor;
	vec.y /= divisor;
	vec.z /= divisor;
	vec.w /= divisor;
}

bool operator==(const Vector4& vecA, const Vector4& vecB)
{
	return vecA.x == vecB.x && vecA.y == vecB.y && vecA.z == vecB.z && vecA.w == vecB.w;
}

bool operator!=(const Vector4& vecA, const Vector4& vecB)
{
	return vecA.x != vecB.x || vecA.y != vecB.y || vecA.z != vecB.z || vecA.w != vecB.w;
}

float Dot(const Vector4& vecA, const Vector4& vecB)
{
	return vecA.x * vecB.x + vecA.y * vecB.y + vecA.z * vecB.z + vecA.w * vecB.w;
}

Vector4 Max(const Vector4& vecA, const Vector4& vecB)
{
	return Vector4(
		vecA.x > vecB.x ? vecA.x : vecB.x,
		vecA.y > vecB.y ? vecA.y : vecB.y,
		vecA.z > vecB.z ? vecA.z : vecB.z,
		vecA.w > vecB.w ? vecA.w : vecB.w);
}

Vector4 Min(const Vector4& vecA, const Vector4& vecB)
{
	return Vector4(
		vecA.x < vecB.x ? vecA.x : vecB.x,
		vecA.y < vecB.y ? vecA.y : vecB.y,
		vecA.z < vecB.z ? vecA.z : vecB.z,
		vecA.w < vecB.w ? vecA.w : vecB.w);
}

float Vector4::Magnitude() const
{
	return sqrt(x * x + y * y + z * z + w * w);
}

Vector4 Vector4::Normalised() const
{
	return *this / Magnitude();
}

bool operator==(const Vector2i& vecA, const Vector2i& vecB)
{
	return vecA.x == vecB.x && vecA.y == vecB.y;
}

bool operator!=(const Vector2i& vecA, const Vector2i& vecB)
{
	return vecA.x != vecB.x || vecA.y != vecB.y;
}

Vector2i operator+(const Vector2i& vecA, const Vector2i& vecB)
{
	return { vecA.x + vecB.x, vecA.y + vecB.y };
}

void operator+=(Vector2i& vecA, const Vector2i& vecB)
{
	vecA.x += vecB.x;
	vecA.y += vecB.y;
}

Vector2i operator-(const Vector2i& vecA, const Vector2i& vecB)
{
	return { vecA.x - vecB.x, vecA.y - vecB.y };
}

void operator-=(Vector2i& vecA, const Vector2i& vecB)
{
	vecA.x -= vecB.x;
	vecA.y -= vecB.y;
}

Vector2i operator-(const Vector2i& vec)
{
	return { -vec.x, -vec.y };
}

Vector2i operator*(const Vector2i& vec, const float multiplier)
{
	return{ static_cast<int>(vec.x * multiplier), static_cast<int>(vec.y * multiplier) };
}

Vector2i operator*(const float multiplier, const Vector2i& vec)
{
	return{ static_cast<int>(vec.x * multiplier), static_cast<int>(vec.y * multiplier) };
}

void operator*=(Vector2i& vec, const float multiplier)
{
	vec.x = static_cast<int>(vec.x * multiplier);
	vec.y = static_cast<int>(vec.y * multiplier);
}

Vector2i operator/(const Vector2i& vec, const float divisor)
{
	return { static_cast<int>(vec.x / divisor), static_cast<int>(vec.y / divisor) };
}

void operator/=(Vector2i& vec, const float divisor)
{
	vec.x = static_cast<int>(vec.x / divisor);
	vec.y = static_cast<int>(vec.y / divisor);
}

Colour::Colour(const Vector4& vec)
{
	r = vec.x;
	g = vec.y;
	b = vec.z;
	a = vec.w;
}