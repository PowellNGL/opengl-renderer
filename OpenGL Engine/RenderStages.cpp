#include <cassert>
#include <queue>

#include "RenderStages.h"
#include "Material.h"
#include "Object.h"
#include "Uniforms.h"
#include "Bounds.h"
#include "Terrain.h"
#include "MeshBufferManager.h"
#include "Texture.h"
#include "Scene.h"
#include "Renderer.h"

RenderStages::MakeGBuffersStage::MakeGBuffersStage(Renderer& renderer, Uniforms& uniformBlocks) : RenderStage(renderer, uniformBlocks) {
	Material* material = renderer.render_data.Materials.New();
	material->Metallic = 1;
	material->Albedo = Colour(0, 0, 0, 1);
	material->Roughness = 1;
	material->Emissivity = Colour(0, 1, 0, 1);
	wireframe_material = material;
}

void RenderStages::MakeGBuffersStage::Run(
	const Framebuffer& framebuffer,
	const Scene& scene,
	const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures,
	MeshBufferManager& meshBufferManager,
	const Camera& camera,
	const Vector2i& resolution, 
	const Colour& ambientLight,
	const int wireframeMode)
{
	last_drawn_mesh = nullptr;

	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	glViewport(0, 0, resolution.x, resolution.y);
	uniformBlocks.SetRenderResolution(resolution);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);
		
	Matrix4 viewMatrix = camera.MakeViewMatrix();
	Matrix4 projMatrix = camera.MakeProjectionMatrix();
	Frustum cameraFrustum = camera.MakeFrustum();

	auto objectsInFrustum = scene.GetAllIntersectingObjects(cameraFrustum);

	if (wireframeMode == 0 || wireframeMode == 1)
	{
		glUseProgram(programNoAlpha.GLIndex());
		glUniform3f(2, ambientLight.r, ambientLight.g, ambientLight.b);

		struct TransparentDrawInfo {
			const MeshInstance* mesh_instance;
			Matrix4 MV;
			Matrix4 MVP;
		};
		std::vector<TransparentDrawInfo> transparent_draws;

		const Material* lastMaterial = nullptr;
		for (const Object* object : objectsInFrustum)
		{
			ObjectID object_id = scene.Objects.ToIndex(object);
			Matrix4 MV = viewMatrix * renderer.ObjectTransforms.at(object_id);
			Matrix4 MVP = projMatrix * MV;
			glUniformMatrix4fv(0, 1, GL_TRUE, (GLfloat*)&MV);
			glUniformMatrix4fv(1, 1, GL_TRUE, (GLfloat*)&MVP);

			for (const auto& meshInstance : object->model->Meshes)
			{
				const Material* material = meshInstance.material;
				Uniforms::MaterialUniforms materialUniforms(*material);
				if (meshInstance.isVisible && !material->IsSkybox)
				{
					if (material->HasAlbedoMap() && renderer.render_data.Textures[material->AlbedoMapID]._channels == 4)
					{
						transparent_draws.push_back({ &meshInstance, MV, MVP });
						continue;
					}
					if (lastMaterial != meshInstance.material) {
						uniformBlocks.MaterialUniformBuffer.UploadAll(&materialUniforms);
						BindTexturesByID(bufferedTextures, 0, {
							material->AlbedoMapID,
							material->RoughnessMapID,
							material->MetallicMapID,
							material->NormalMapID,
							material->EmissivityMapID
						});
					}

					DrawMesh(*meshInstance.mesh, meshBufferManager);
				}
			}
		}

		glUseProgram(programAlpha.GLIndex());
		glUniform3f(2, ambientLight.r, ambientLight.g, ambientLight.b);

		for (const auto& [mesh_instance, MV, MVP] : transparent_draws)
		{
			glUniformMatrix4fv(0, 1, GL_TRUE, (GLfloat*)&MV);
			glUniformMatrix4fv(1, 1, GL_TRUE, (GLfloat*)&MVP);

			const Material* material = mesh_instance->material;
			Uniforms::MaterialUniforms materialUniforms(*material);
			uniformBlocks.MaterialUniformBuffer.UploadAll(&materialUniforms);
			BindTexturesByID(bufferedTextures, 0, {
				material->AlbedoMapID,
				material->RoughnessMapID,
				material->MetallicMapID,
				material->NormalMapID,
				material->EmissivityMapID
			});
			DrawMesh(*mesh_instance->mesh, meshBufferManager);
		}
	}

	if (wireframeMode == 1 || wireframeMode == 2)
	{
		glUseProgram(programNoAlpha.GLIndex());
		glUniform3f(2, ambientLight.r, ambientLight.g, ambientLight.b);

		glDepthFunc(GL_LEQUAL);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		Uniforms::MaterialUniforms materialUniforms(*wireframe_material);
		uniformBlocks.MaterialUniformBuffer.UploadAll(&materialUniforms);
		BindTexturesByID(bufferedTextures, 0, {
			wireframe_material->AlbedoMapID,
			wireframe_material->RoughnessMapID,
			wireframe_material->MetallicMapID,
			wireframe_material->NormalMapID,
			wireframe_material->EmissivityMapID
		});

		for (const Object* object : objectsInFrustum)
		{
			ObjectID object_id = scene.Objects.ToIndex(object);
			Matrix4 MV = viewMatrix * renderer.ObjectTransforms.at(object_id);
			Matrix4 MVP = projMatrix * MV;
			glUniformMatrix4fv(0, 1, GL_TRUE, (GLfloat*)&MV);
			glUniformMatrix4fv(1, 1, GL_TRUE, (GLfloat*)&MVP);

			for (const auto& meshInstance : object->model->Meshes) {
				DrawMesh(*meshInstance.mesh, meshBufferManager);
			}
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDepthFunc(GL_LESS);
	}
}

void RenderStages::CopyTextureStage::Run(const TextureBuffer & source, const Framebuffer* destination, const Vector2i& destinationResolution)
{
	glUseProgram(program.GLIndex());

	if (destination == nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, destination->GLIndex());
	}
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glViewport(0, 0, destinationResolution.x, destinationResolution.y);
	uniformBlocks.SetRenderResolution(destinationResolution);
	BindTextures(0, { &source });
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
}

void RenderStages::LightingStage::Run(
	const Vector2i& resolution, 
	const Framebuffer& framebuffer,
	const TextureBuffer& albedoBuffer,
	const TextureBuffer& smoothAndMetalBuffer, 
	const TextureBuffer& normalBuffer, 
	const TextureBuffer& depthBuffer, 
	const TextureBuffer& pointLightShadowBuffer, 
	const TextureBuffer& directionalLightShadowBuffer,
	const bool isPointLight)
{
	glUseProgram(isPointLight ? pointLightProgram.GLIndex() : directionalLightProgram.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDisable(GL_DEPTH_TEST);

	BindTextures(0, { &albedoBuffer,
		&smoothAndMetalBuffer,
		&normalBuffer,
		&depthBuffer,
		&pointLightShadowBuffer,
		&directionalLightShadowBuffer });

	glViewport(0, 0, resolution.x, resolution.y);
	uniformBlocks.SetRenderResolution(resolution);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
}

void RenderStages::PointLightShadowMapStage::Run(
	const PointLight& light,
	const Vector2i& shadowResolution,
	const Framebuffer& framebuffer,
	const TextureBuffer& shadowMap,
	const Scene& scene,
	const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures,
	MeshBufferManager& meshBufferManager,
	const Camera& camera)
{
	last_drawn_mesh = nullptr;

	glUseProgram(programNoAlpha.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());

	const Uniforms::PointLightUniforms pointLightUniforms(light);
	uniformBlocks.PointLightUniformBuffer.UploadAll(&pointLightUniforms);

	Matrix4 projection = Matrix4::MakePerspectiveProjection(pointLightUniforms.clip.near, pointLightUniforms.clip.far, pi / 2.0f, pi / 2.0f);
	Matrix4 lightTransform = Matrix4::MakeTranslation(-light.object->Position);
	std::array<Matrix4, 6> faceViewMatrices {
		Matrix4::FromQuaternion(Quaternion(Vector3::Up(), pi * 0.5f)) * lightTransform,		// -X
		Matrix4::FromQuaternion(Quaternion(Vector3::Up(), pi * 1.5f)) * lightTransform,		// +X
		Matrix4::FromQuaternion(Quaternion(Vector3::Left(), pi * 0.5f)) * lightTransform,	// +Y
		Matrix4::FromQuaternion(Quaternion(Vector3::Left(), pi * 1.5f)) * lightTransform,	// -Y
		Matrix4::FromQuaternion(Quaternion(Vector3::Up(), pi * 0.0f)) * lightTransform,		// -Z
		Matrix4::FromQuaternion(Quaternion(Vector3::Up(), pi * 1.0f)) * lightTransform		// +Z
	};

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glViewport(0, 0, shadowResolution.x, shadowResolution.y);
	uniformBlocks.SetRenderResolution(shadowResolution);

	struct TransparentDrawInfo {
		const Mesh* mesh;
		TextureID albedoMapID;
		Matrix4 MV;
		Matrix4 MVP;
	};
	std::vector<TransparentDrawInfo> transparent_draws;

	Frustum cameraFrustum = camera.MakeFrustum();

	for (int i = 0; i < 6; i++)
	{
		Frustum faceFrustum(projection * faceViewMatrices[i]);
		if (!cameraFrustum.Intersects(faceFrustum)) {
			continue;
		}

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, shadowMap.GLIndex(), 0);
		glClear(GL_DEPTH_BUFFER_BIT);

		transparent_draws.clear();

		auto objectsInRange = scene.GetAllIntersectingObjects(faceFrustum);

		Matrix4 MV;
		Matrix4 MVP;
		for (const Object* object : objectsInRange)
		{
			const Matrix4& object_transform = renderer.ObjectTransforms.at(scene.Objects.ToIndex(object));
			MV = faceViewMatrices[i] * object_transform;
			MVP = projection * MV;
			glUniformMatrix4fv(0, 1, GL_TRUE, reinterpret_cast<const GLfloat*>(&MV));
			glUniformMatrix4fv(1, 1, GL_TRUE, reinterpret_cast<const GLfloat*>(&MVP));

			for (auto& meshInstance : object->model->Meshes)
			{
				const Material* material = meshInstance.material;
				if (meshInstance.isShadowCaster && !material->IsSkybox)
				{
					if (material->HasAlbedoMap() && renderer.render_data.Textures[material->AlbedoMapID]._channels == 4)
					{
						transparent_draws.push_back({ meshInstance.mesh, material->AlbedoMapID, MV, MVP });
						continue;
					}
					DrawMesh(*meshInstance.mesh, meshBufferManager);
				}
			}
		}

		for (const auto& [mesh, albedoMapID, MV, MVP] : transparent_draws) {
			glUseProgram(programAlpha.GLIndex());
			glUniformMatrix4fv(0, 1, GL_TRUE, reinterpret_cast<const GLfloat*>(&MV));
			glUniformMatrix4fv(1, 1, GL_TRUE, reinterpret_cast<const GLfloat*>(&MVP));
			BindTexturesByID(bufferedTextures, 0, { albedoMapID });
			DrawMesh(*mesh, meshBufferManager);
		}
	}
}

void RenderStages::DirectionalLightShadowMapStage::Run(
	const DirectionalLight& light,
	const Vector2i& shadowResolution,
	const std::array<float, NUM_CASCADES + 1>& cascadeBoundaries,
	const Framebuffer& framebuffer,
	const Scene& scene,
	const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures,
	MeshBufferManager& meshBufferManager,
	const Camera& camera,
	const TextureBuffer& shadowMap)
{
	last_drawn_mesh = nullptr;

	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	const Uniforms::DirectionalLightUniforms uniforms(light, shadowResolution, cascadeBoundaries, camera);
	uniformBlocks.DirectionalLightUniformBuffer.UploadAll(&uniforms);
	glViewport(0, 0, shadowResolution.x, shadowResolution.y);
	uniformBlocks.SetRenderResolution(shadowResolution);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	
	for (int cascade = 0; cascade < NUM_CASCADES; cascade++)
	{
		const auto& cascade_info = uniforms.cascadeInfo[cascade];

		glUseProgram(programNoAlpha.GLIndex());
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadowMap.GLIndex(), 0, cascade);
		glClear(GL_DEPTH_BUFFER_BIT);
		glUniform1i(2, cascade);

		Frustum cascadeFrustum(cascade_info.Projection * cascade_info.Transform);
		const auto objectsInCascade = scene.GetAllIntersectingObjects(cascadeFrustum);


		struct TransparentDrawInfo {
			const Mesh* mesh;
			TextureID albedoMapID;
			Matrix4 MV;
			Matrix4 MVP;
		};

		std::vector<TransparentDrawInfo> transparent_draws;

		for (const Object* object : objectsInCascade)
		{
			const Matrix4& object_transform = renderer.ObjectTransforms.at(scene.Objects.ToIndex(object));
			Matrix4 MV = cascade_info.Transform * object_transform;
			Matrix4 MVP = cascade_info.Projection * MV;

			glUniformMatrix4fv(0, 1, GL_TRUE, reinterpret_cast<GLfloat*>(&MV));
			glUniformMatrix4fv(1, 1, GL_TRUE, reinterpret_cast<GLfloat*>(&MVP));

			for (auto& meshInstance : object->model->Meshes)
			{
				const Material* material = meshInstance.material;
				if (meshInstance.isShadowCaster && !material->IsSkybox)
				{
					if (material->HasAlbedoMap() && renderer.render_data.Textures[material->AlbedoMapID]._channels == 4)
					{
						transparent_draws.push_back({ meshInstance.mesh, material->AlbedoMapID, MV, MVP });
						continue;
					}
					DrawMesh(*meshInstance.mesh, meshBufferManager);
				}
			}
		}

		glUseProgram(programAlpha.GLIndex());
		glUniform1i(2, cascade);
		for (const auto& [mesh, albedoMapID, MV, MVP] : transparent_draws)
		{
			glUniformMatrix4fv(0, 1, GL_TRUE, reinterpret_cast<const GLfloat*>(&MV));
			glUniformMatrix4fv(1, 1, GL_TRUE, reinterpret_cast<const GLfloat*>(&MVP));
			BindTexturesByID(bufferedTextures, 0, { albedoMapID });
			DrawMesh(*mesh, meshBufferManager);
		}
	}
}

void RenderStages::SkyboxStage::Run(const Framebuffer& GBufferFBO, const TextureBuffer& SkyboxCubemap, const Vector2i& resolution)
{
	glUseProgram(program.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, GBufferFBO.GLIndex());
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, resolution.x, resolution.y);
	uniformBlocks.SetRenderResolution(resolution);
	BindTextures(5, { &SkyboxCubemap });
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
}

void RenderStages::BloomConvolutionStage::Run(const TextureBuffer& sourceTextureBuffer, const TextureBuffer& convolutionsTextureBuffer, const Vector2i& sourceResolution, const Framebuffer& framebuffer)
{
	glUseProgram(program.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	
	//Convolve down to use filtering to spread the colour around
	Vector2i targetResolution;
	int sourceLevel, targetLevel;
	for (targetLevel = 0; targetLevel < BLOOM_OCTAVES; targetLevel++)
	{
		if (targetLevel == 0)
		{
			BindTextures(0, { &sourceTextureBuffer });
			glUniform1i(1, GL_TRUE);
		}
		else if (targetLevel == 1)
		{
			BindTextures(0, { &convolutionsTextureBuffer });
			glUniform1i(1, GL_FALSE);
		}
		sourceLevel = std::max(targetLevel - 1, 0);

		glNamedFramebufferTexture(framebuffer.GLIndex(), GL_COLOR_ATTACHMENT0, convolutionsTextureBuffer.GLIndex(), targetLevel);
		glUniform1i(0, sourceLevel);
		targetResolution = sourceResolution / powf(2.0f, targetLevel + 1);
		glViewport(0, 0, targetResolution.x, targetResolution.y);
		uniformBlocks.SetRenderResolution(targetResolution);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
	}
}

void RenderStages::GaussianBlurStage::Run(const TextureBuffer& sourceTextureBuffer, const int sourceTextureLevel, const Vector2i& sourceResolution, const TextureBuffer& targetTextureBufferX, const TextureBuffer& targetTextureBufferY, const Framebuffer& framebuffer)
{
	glUseProgram(program.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	glUniform2f(0, 1.0f / sourceResolution.x, 1.0f / sourceResolution.y);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	glUniform1i(1, GL_TRUE);
	BindTextures(0, { &sourceTextureBuffer });
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, targetTextureBufferX.GLIndex(), 0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

	glUniform1i(1, GL_FALSE);
	BindTextures(0, { &targetTextureBufferX });
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, targetTextureBufferY.GLIndex(), 0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
}

void RenderStages::PostprocessingStage::Run(const Vector2i& outputResolution, const TextureBuffer& colourBuffer, const TextureBuffer& depthBuffer, const TextureBuffer& bloomBuffer)
{
	glUseProgram(program.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, outputResolution.x, outputResolution.y);
	uniformBlocks.SetRenderResolution(outputResolution);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	BindTextures(0, {
		&colourBuffer,
		&depthBuffer,
		&bloomBuffer
	});
	glUniform3f(0, 0.8f, 0.9f, 1);	//Fog colour
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
}

void RenderStages::DrawLineStage::Run(const Line& line, const Matrix4& transform, 
	const Camera& camera, const Framebuffer& framebuffer, const Colour& colour)
{
	glUseProgram(program.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	Matrix4 mvp = camera.MakeProjectionMatrix() * (camera.MakeViewMatrix() * transform);

	glUniform3fv(0, 1, &line.start.x);
	glUniform3fv(1, 1, &line.end.x);
	glUniformMatrix4fv(2, 1, GL_TRUE, (GLfloat*)&mvp);
	glUniform4fv(3, 1, (GLfloat*)&colour);

	glDrawArrays(GL_LINES, 0, 2);
}

void RenderStages::DrawBoundsStage::Run(const Bounds& bounds, const Matrix4& transform, 
	const Camera& camera, const Framebuffer& framebuffer, const Colour& colour)
{
	glUseProgram(program.GLIndex());
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.GLIndex());
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	Matrix4 mvp = camera.MakeProjectionMatrix() * (camera.MakeViewMatrix() * transform);

	glUniform3fv(0, 1, &bounds.v1.x);
	glUniform3fv(1, 1, &bounds.v2.x);
	glUniformMatrix4fv(2, 1, GL_TRUE, (GLfloat*)&mvp);
	glUniform4fv(3, 1, (GLfloat*)&colour);

	glDrawArrays(GL_POINTS, 0, 1);
}

//Binds the relevant vertex array object, vertex buffer object and element buffer object, then does the draw call for the elements of the mesh
void RenderStages::RenderStage::DrawMesh(const Mesh& mesh, const MeshBufferManager& manager)
{
	if (manager.BufferedMeshes.find(mesh.ID()) == manager.BufferedMeshes.end())
		return;

	const MeshBufferInfo& mbi = manager.BufferedMeshes.at(mesh.ID());
	if (&mesh != last_drawn_mesh)
	{
		glBindVertexArray(manager.VAO.GLIndex());

		auto bindBuffer = [&](VertexBuffers::Type type, size_t elementSizeBytes) {
			glBindVertexBuffer((GLuint)type, mbi.buffers.BufferIndices[(size_t)type], 0, elementSizeBytes);
		};
		bindBuffer(VertexBuffers::Type::Position, sizeof(Vector3));
		bindBuffer(VertexBuffers::Type::Normal, sizeof(Vector3));
		bindBuffer(VertexBuffers::Type::UV, sizeof(Vector2));
		bindBuffer(VertexBuffers::Type::Binormal, sizeof(Vector3));
		bindBuffer(VertexBuffers::Type::Tangent, sizeof(Vector3));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mbi.buffers.BufferIndices[(size_t)VertexBuffers::Type::Index]);

		last_drawn_mesh = &mesh;
	}
	
	glDrawElementsBaseVertex(GL_TRIANGLES, mbi.IndicesLength, GL_UNSIGNED_INT, (void*)(mbi.IndicesOffset * sizeof(GLuint)), mbi.VertsOffset);
}