#pragma once

#include "Vector.h"
#include "Quaternion.h"
#include <math.h>
#include <immintrin.h>

struct Matrix4
{
	using IndexT = size_t;

private:
	std::array<float, 16> elements;

public:
	static const Matrix4 Identity;

	constexpr Matrix4() = default;
	constexpr Matrix4(const std::array<float, 16>& inputElements) : elements(inputElements) {}
	constexpr Matrix4(
		float a, float b, float c, float d,
		float e, float f, float g, float h,
		float i, float j, float k, float l,
		float m, float n, float o, float p)
		: elements{ a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p } {}

	constexpr const float& Element(IndexT x, IndexT y) const
	{
		return elements[y * 4 + x];
	}

	float& Element(IndexT x, IndexT y)
	{
		return elements[y * 4 + x];
	}

	float& operator[](size_t i) {
		return elements[i];
	}
	const float& operator[](size_t i) const {
		return elements[i];
	}

	bool operator== (const Matrix4& other) const
	{
		return elements == other.elements;
	}
	Matrix4 operator* (float scalar) const;
	Matrix4 operator+ (const Matrix4& matrixB) const;
	Matrix4 operator- (const Matrix4& matrixB) const;
	Matrix4 Inverse() const;
	static Matrix4 FromEulerAngles(const Vector3& angles);
	static Matrix4 FromQuaternion(const Quaternion& quaternion);
	static Matrix4 MakeTranslation(const Vector3& translation);
	static Matrix4 MakeScale(const Vector3& scale);
	static Matrix4 MakeOrthographicProjection(float nearClip, float farClip, const Vector2& viewportScale);
	static Matrix4 MakePerspectiveProjection(float nearClip, float farClip, float fovX, float fovY);
};

#if __AVX__
inline Matrix4 operator* (const Matrix4& matA, const Matrix4& matB)
{
	Matrix4 out;

	__m256 rowB01 = _mm256_loadu_ps(&matB[0]);	// 0, 1, 2, 3, 4, 5, 6, 7
	__m256 rowB23 = _mm256_loadu_ps(&matB[8]);	// 8, 9, 10, 11, 12, 13, 14, 15

	auto doRow = [&](size_t index)
	{
		__m128 val0 = _mm_broadcast_ss(&matA[index + 0]);
		__m128 val1 = _mm_broadcast_ss(&matA[index + 1]);
		__m256 tmp0 = _mm256_set_m128(val1, val0);	// 0, 0, 0, 0, 1, 1, 1, 1
		tmp0 = _mm256_mul_ps(tmp0, rowB01);

		__m128 val2 = _mm_broadcast_ss(&matA[index + 2]);
		__m128 val3 = _mm_broadcast_ss(&matA[index + 3]);
		__m256 tmp1 = _mm256_set_m128(val3, val2);	// 2, 2, 2, 2, 3, 3, 3, 3
		tmp1 = _mm256_mul_ps(tmp1, rowB23);

		// Sum all four 128-bit lanes
		tmp0 = _mm256_add_ps(tmp0, tmp1);
		__m128 rowOut = _mm256_extractf128_ps(tmp0, 1);
		rowOut = _mm_add_ps(rowOut, _mm256_castps256_ps128(tmp0));

		_mm_storeu_ps(&out[index], rowOut);
	};

	doRow(0);
	doRow(4);
	doRow(8);
	doRow(12);

	return out;
}

inline Vector4 operator* (const Matrix4& matrix, const Vector4& vector)
{
	Vector4 out;

	__m128 vec = _mm_loadu_ps(&vector.x);
	__m128 row0 = _mm_loadu_ps(&matrix[0]);
	__m128 row1 = _mm_loadu_ps(&matrix[4]);
	__m128 row2 = _mm_loadu_ps(&matrix[8]);
	__m128 row3 = _mm_loadu_ps(&matrix[12]);

	row0 = _mm_mul_ps(vec, row0);
	row1 = _mm_mul_ps(vec, row1);
	row2 = _mm_mul_ps(vec, row2);
	row3 = _mm_mul_ps(vec, row3);

	_MM_TRANSPOSE4_PS(row0, row1, row2, row3);

	__m128 tmp0 = _mm_add_ps(row0, row1);
	__m128 tmp1 = _mm_add_ps(row2, row3);
	tmp0 = _mm_add_ps(tmp0, tmp1);
	_mm_storeu_ps(&out.x, tmp0);

	return out;
}
#else
inline Matrix4 operator* (const Matrix4& matA, const Matrix4& matB)
{
	Matrix4 out;
	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			out.Element(x, y) =
				matA.Element(0, y) * matB.Element(x, 0) +
				matA.Element(1, y) * matB.Element(x, 1) +
				matA.Element(2, y) * matB.Element(x, 2) +
				matA.Element(3, y) * matB.Element(x, 3);
		}
	}
	return out;
}

inline Vector4 operator*(const Matrix4& matrix, const Vector4& vector)
{
	Vector4 out;

	out.x = matrix.Element(0, 0) * vector.x + matrix.Element(1, 0) * vector.y + matrix.Element(2, 0) * vector.z + matrix.Element(3, 0) * vector.w;
	out.y = matrix.Element(0, 1) * vector.x + matrix.Element(1, 1) * vector.y + matrix.Element(2, 1) * vector.z + matrix.Element(3, 1) * vector.w;
	out.z = matrix.Element(0, 2) * vector.x + matrix.Element(1, 2) * vector.y + matrix.Element(2, 2) * vector.z + matrix.Element(3, 2) * vector.w;
	out.w = matrix.Element(0, 3) * vector.x + matrix.Element(1, 3) * vector.y + matrix.Element(2, 3) * vector.z + matrix.Element(3, 3) * vector.w;

	return out;
}
#endif