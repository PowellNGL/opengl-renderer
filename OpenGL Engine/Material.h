#pragma once

#include <string>

#include "Colour.h"
#include "Shared.h"

struct Material
{
	std::string Name = "";

	Colour Albedo { Colour::Default() };
	float Roughness { 0.5f };
	float Metallic { 0.0f };
	Colour Emissivity { Colour(0, 0, 0, 1) };
	bool IsSkybox { false };

	//Non-GL IDs
	MaterialID AlbedoMapID { InvalidMaterialID };
	MaterialID RoughnessMapID { InvalidMaterialID };
	MaterialID MetallicMapID { InvalidMaterialID };
	MaterialID EmissivityMapID { InvalidMaterialID };
	MaterialID NormalMapID { InvalidMaterialID };

	inline bool HasAlbedoMap() const { return AlbedoMapID != InvalidMaterialID; }
	inline bool HasRoughnessMap() const { return RoughnessMapID != InvalidMaterialID; }
	inline bool HasMetallicMap() const { return MetallicMapID != InvalidMaterialID; }
	inline bool HasEmissivityMap() const { return EmissivityMapID != InvalidMaterialID; }
	inline bool HasNormalMap() const { return NormalMapID != InvalidMaterialID; }

	Material() = default;
	Material(const std::string& name) : Name(name) {};
	static Material DefaultBlankMaterial(const std::string& name, const Colour& colour)
	{
		Material material;
		material.Name = name;
		material.Albedo = colour;
		return material;
	}
};
