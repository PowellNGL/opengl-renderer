#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <regex>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <cmath>
#include <SDL.h>

#include "DoomReader.h"
#include "Triangulator.h"
#include "Object.h"
#include "Bounds.h"
#include "Scene.h"
#include "Texture.h"
#include "Mesh.h"
#include "RenderData.h"

namespace DoomReader
{
	constexpr float mapScale = 50;

	//These functions all assume the processor is little-endian
	//TODO: Something with constexpr to convert them if necessary (Although is this ever likely to run on a non-x86 machine?)

	inline int32_t ReadInt32(const std::vector<char>& buffer, const int offset)
	{
		return *reinterpret_cast<int32_t*>(const_cast<char*>(&buffer[offset]));
	}

	inline int16_t ReadInt16(const std::vector<char>& buffer, const int offset)
	{
		return *reinterpret_cast<int16_t*>(const_cast<char*>(&buffer[offset]));
	}

	inline uint8_t ReadUInt8(const std::vector<char>& buffer, const int offset)
	{
		return *reinterpret_cast<uint8_t*>(const_cast<char*>(&buffer[offset]));
	}

	inline Rect ReadRect(const std::vector<char>& buffer, const int offset)
	{
		Rect rect;
		rect.x1 = static_cast<float>(ReadInt16(buffer, offset + 0));
		rect.y1 = static_cast<float>(ReadInt16(buffer, offset + 2));
		rect.x2 = static_cast<float>(ReadInt16(buffer, offset + 4));
		rect.y2 = static_cast<float>(ReadInt16(buffer, offset + 6));
		return rect;
	}

	inline DirectoryEntry ReadDirectoryEntry(const std::vector<char>& buffer, const int offset)
	{
		DirectoryEntry entry;

		entry.filepos = ReadInt32(buffer, offset);
		entry.size = ReadInt32(buffer, offset + 0x04);
		entry.name = ReadString(buffer, offset + 0x08, 8);

		return entry;
	}

	inline std::string ReadString(const std::vector<char>& buffer, const int offset, const int length)
	{
		std::string output(&buffer[offset], length);
		output = output.substr(0, output.find('\0'));
		for (auto& c : output)
		{
			c = toupper(c);
		}
		return output;
	}

	inline bool IsMapLump(const DirectoryEntry& entry)
	{
		return std::regex_match(entry.name, std::regex("E\\dM\\d|MAP\\d\\d"));
	}

	inline Map ReadMap(const std::vector<DirectoryEntry>& directory, const int mapEntryIndex, const std::vector<char>& buffer)
	{
		Map map;

		for (int index = mapEntryIndex + 1; index < directory.size(); index++)
		{
			DirectoryEntry currentEntry = directory[index];
			if (currentEntry.name == "VERTEXES")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 4)
				{
					map.Vertexes.push_back(Vector2{ static_cast<float>(ReadInt16(buffer, offset)), -static_cast<float>(ReadInt16(buffer, offset + 2)) });
				}
			}
			else if (currentEntry.name == "LINEDEFS")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 14)
				{
					Linedef line;
					line.startVertex = ReadInt16(buffer, offset + 0);
					line.endVertex = ReadInt16(buffer, offset + 2);
					line.flags = ReadInt16(buffer, offset + 4);
					line.specialType = ReadInt16(buffer, offset + 6);
					line.sectorTag = ReadInt16(buffer, offset + 8);
					line.rightSidedef = ReadInt16(buffer, offset + 10);
					line.leftSidedef = ReadInt16(buffer, offset + 12);
					map.Linedefs.push_back(line);
				}
			}
			else if (currentEntry.name == "SIDEDEFS")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 30)
				{
					Sidedef side;
					side.offset = { (float)ReadInt16(buffer, offset + 0), (float)ReadInt16(buffer, offset + 2) };
					side.upperTexture = ReadString(buffer, offset + 4, 8);
					side.lowerTexture = ReadString(buffer, offset + 12, 8);
					side.middleTexture = ReadString(buffer, offset + 20, 8);
					side.sectorNumber = ReadInt16(buffer, offset + 28);
					map.Sidedefs.push_back(side);
				}
			}
			else if (currentEntry.name == "SEGS")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 12)
				{
					Seg seg;
					seg.startVertex = ReadInt16(buffer, offset + 0);
					seg.endVertex = ReadInt16(buffer, offset + 2);
					seg.angle = ReadInt16(buffer, offset + 4);
					seg.linedef = ReadInt16(buffer, offset + 6);
					seg.direction = ReadInt16(buffer, offset + 8);
					seg.offset = ReadInt16(buffer, offset + 10);
					map.Segs.push_back(seg);
				}
			}
			else if (currentEntry.name == "SECTORS")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 26)
				{
					Sector sector;
					sector.floorHeight = ReadInt16(buffer, offset + 0);
					sector.ceilingHeight = ReadInt16(buffer, offset + 2);
					sector.floorTexture = ReadString(buffer, offset + 4, 8);
					sector.ceilingTexture = ReadString(buffer, offset + 12, 8);
					sector.lightLevel = ReadInt16(buffer, offset + 20);
					sector.type = ReadInt16(buffer, offset + 22);
					sector.tagNumber = ReadInt16(buffer, offset + 24);
					map.Sectors.push_back(sector);
				}
			}
			else if (currentEntry.name == "NODES")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 28)
				{
					Node node;
					node.x =  static_cast<float>(ReadInt16(buffer, offset + 0));
					node.y =  static_cast<float>(ReadInt16(buffer, offset + 2));
					node.dX = static_cast<float>(ReadInt16(buffer, offset + 4));
					node.dY = static_cast<float>(ReadInt16(buffer, offset + 6));
					node.rightBoundingBox = ReadRect(buffer, offset + 8);
					node.leftBoundingBox = ReadRect(buffer, offset + 16);
					node.rightChild = ReadInt16(buffer, offset + 24);
					node.leftChild = ReadInt16(buffer, offset + 26);
					map.Nodes.push_back(node);
				}
			}
			else if (currentEntry.name == "SSECTORS")
			{
				for (int offset = currentEntry.filepos; offset < currentEntry.filepos + currentEntry.size; offset += 4)
				{
					Subsector subsector;
					subsector.segCount = ReadInt16(buffer, offset + 0);
					subsector.segCount = ReadInt16(buffer, offset + 2);
					map.Subsectors.push_back(subsector);
				}
			}
			else if (currentEntry.name == "THINGS"
				|| currentEntry.name == "REJECT"
				|| currentEntry.name == "BLOCKMAP")
			{
				//Unused lumps
			}
			else
			{
				break;
			}
		}
		return map;
	}

	std::vector<MapTexture> ReadTextureLump(const DirectoryEntry& entry, const std::vector<char>& buffer)
	{
		std::vector<MapTexture> output;

		int numTextures = ReadInt32(buffer, entry.filepos + 0);
		std::vector<int> textureOffsets(numTextures);

		int offset = entry.filepos + 4;
		for (int i = 0; i < numTextures; i++)
		{
			textureOffsets[i] = ReadInt32(buffer, offset);
			offset += 4;
		}

		for (int i = 0; i < textureOffsets.size(); i++)
		{
			offset = entry.filepos + textureOffsets[i];

			output.push_back(MapTexture());
			MapTexture& mapTexture = output.back();
			mapTexture.Name = ReadString(buffer, offset + 0, 8);
			mapTexture.Width = ReadInt16(buffer, offset + 12);
			mapTexture.Height = ReadInt16(buffer, offset + 14);

			int patchCount = ReadInt16(buffer, offset + 20);
			for (int j = 0; j < patchCount; j++)
			{
				int patchOffset = offset + 22 + (10 * j);
				MapPatch mapPatch;
				mapPatch.OriginX = ReadInt16(buffer, patchOffset + 0);
				mapPatch.OriginY = ReadInt16(buffer, patchOffset + 2);
				mapPatch.PictureID = ReadInt16(buffer, patchOffset + 4);
				mapTexture.Patches.push_back(mapPatch);
			}
		}

		return output;
	}

	Texture* ReadFlat(const DirectoryEntry& entry, const std::vector<char>& buffer,
		const std::array<Colour, 256>& palette, RenderData& render_data)
	{
		Texture* texture = render_data.Textures.New(64, 64, 4);

		int i = 0;
		for (auto it = texture->_pixels.begin(); it != texture->_pixels.end(); it += 4)
		{
			const Colour& colour = palette[static_cast<size_t>(buffer[entry.filepos + i++])];
			*(it++) = colour.r;
			*(it++) = colour.g;
			*(it++) = colour.b;
			*(it++) = colour.a;
		}

		return texture;
	}

	std::vector<std::string> ReadPNames(const DirectoryEntry& entry, const std::vector<char>& buffer,
		RenderData& render_data)
	{
		std::vector<std::string> names;

		int numPatches = ReadInt32(buffer, entry.filepos);

		for (int i = 0; i < numPatches; i++)
		{
			std::string name = ReadString(buffer, entry.filepos + 4 + i * 8, 8);
			for (auto& c : name)
			{
				c = toupper(c);
			}
			names.push_back(name);
		}

		return names;
	}

	Picture ReadPicture(const DirectoryEntry& entry, const std::vector<char>& buffer, 
						const std::array<Colour, 256>& palette, RenderData& render_data)
	{
		int width = ReadInt16(buffer, entry.filepos + 0);
		int height = ReadInt16(buffer, entry.filepos + 2);

		Picture picture {
			ReadInt16(buffer, entry.filepos + 4),
			ReadInt16(buffer, entry.filepos + 6),
			render_data.Textures.New(Texture(width, height, 4))
		};
				
		std::vector<int> pointers(width);

		//Get the offsets of each column of pixels
		for (int x = 0; x < width; x++)
		{
			pointers[x] = ReadInt32(buffer, entry.filepos + 8 + x * 4);
		}

		int pointer = 0;
		for (int x = 0; x < width; x++)
		{
			int columnDataOffset = entry.filepos + pointers[pointer++];
			//Read a column
			while (true)
			{
				int yStartOffset = (int)(unsigned char)buffer[columnDataOffset++];
				int numPixels = (int)(unsigned char)buffer[columnDataOffset++];
				if (yStartOffset == 255)
				{
					break;
				}
				columnDataOffset++;	//Apparently the third byte in a column doesn't do anything so skip it

				for (int i = 0; i < numPixels; i++)
				{
					int y = yStartOffset + i;

					int colourID = (unsigned char)buffer[columnDataOffset + i];
					float* pixel = picture.texture->Pixel(x, y);
					Colour colour = palette[colourID];
					pixel[0] = colour.r;
					pixel[1] = colour.g;
					pixel[2] = colour.b;
					pixel[3] = colour.a;
				}
				columnDataOffset += numPixels + 1;	//Also the last byte does nothing too so skip that
			}
		}

		return picture;
	}

	void MakeWallMesh(
		Model& destModel,
		Scene& scene,
		const Vector2& startVertex,
		const Vector2& endVertex,
		int ceilingHeight,
		int floorHeight,
		Material* material,
		bool right,
		bool topPegged,
		Vector2 textureOffset,
		bool makeRearShadowCaster,
		const RenderData& render_data)
	{
		int top = std::max(ceilingHeight, floorHeight);
		int bottom = std::min(ceilingHeight, floorHeight);

		scene.CommonMeshes.push_back(Mesh());
		Mesh& mesh = scene.CommonMeshes.back();
		mesh.Verts.push_back(Vector3(startVertex.x, bottom, startVertex.y) / mapScale);
		mesh.Verts.push_back(Vector3(endVertex.x, bottom, endVertex.y) / mapScale);
		mesh.Verts.push_back(Vector3(endVertex.x, top, endVertex.y) / mapScale);
		mesh.Verts.push_back(Vector3(startVertex.x, top, startVertex.y) / mapScale);

		const Texture& albedo_map = render_data.Textures[material->AlbedoMapID];
		Vector2 textureSize = { static_cast<float>(albedo_map._width), static_cast<float>(albedo_map._height) };
		Vector2 wallSize = { (endVertex - startVertex).Magnitude(), static_cast<float>(top - bottom) };
		Vector2 sizeRatio = { wallSize.x / textureSize.x, wallSize.y / textureSize.y };

		float leftUV = 0 + textureOffset.x / textureSize.x;
		float rightUV = sizeRatio.x + textureOffset.x / textureSize.x;
		float topUV, bottomUV;
		if (topPegged)
		{
			topUV = 0;
			bottomUV = sizeRatio.y;
		}
		else 
		{
			topUV = -sizeRatio.y;
			bottomUV = 0;
		}
		topUV += textureOffset.y / textureSize.y;
		bottomUV += textureOffset.y / textureSize.y;

 		if (right)
		{
			mesh.UVs.push_back({ leftUV, bottomUV });
			mesh.UVs.push_back({ rightUV, bottomUV });
			mesh.UVs.push_back({ rightUV, topUV });
			mesh.UVs.push_back({ leftUV, topUV });

			mesh.AppendTriangle(0, 1, 2);
			mesh.AppendTriangle(0, 2, 3);
		}
		else
		{
			mesh.UVs.push_back({ rightUV, bottomUV });
			mesh.UVs.push_back({ leftUV, bottomUV });
			mesh.UVs.push_back({ leftUV, topUV });
			mesh.UVs.push_back({ rightUV, topUV });

			mesh.AppendTriangle(0, 2, 1);
			mesh.AppendTriangle(0, 3, 2);
		}
		mesh.CalcNormals();

		destModel.Meshes.push_back(MeshInstance(&destModel, &mesh, material));

		if (makeRearShadowCaster)
		{
			scene.CommonMeshes.push_back(Mesh(mesh));
			Mesh& rearMesh = scene.CommonMeshes.back();
			rearMesh.Reverse();
			destModel.Meshes.push_back(MeshInstance(&destModel, &rearMesh));
			destModel.Meshes.back().isShadowCaster = true;
			destModel.Meshes.back().isVisible = false;
		}
	}

	void GenerateMaterial(const std::string& name, TextureID texture_id, std::unordered_map<std::string, Material*>& materials)
	{
		if (auto it = materials.find(name); it != materials.end())
		{
			Material* material = it->second;
			material->Name = name;
			material->AlbedoMapID = texture_id;
			material->Roughness = 1;
			material->Metallic = 0;
			material->Emissivity = Colour(0, 0, 0, 1);
		}
	}

	void GenerateObjectsFromMap(const Map& map, Scene& scene, const std::unordered_map<std::string, Material*>& materials, RenderData& render_data)
	{
		std::vector<Object*> outputObjects;

		//This is a vector of unordered maps each representing a Sector
		//Each unordered map associates a vertex within the sector with a PolyVert structure that will make it easier to triangulate the polygon later
		std::vector<std::unordered_map<int, Triangulator::PolyVert>> sectorVerts(map.Sectors.size());
		
		int skyHeight = map.Sectors.front().ceilingHeight;
		for (auto& sector : map.Sectors)
		{
			skyHeight = std::max(skyHeight, sector.ceilingHeight);
		}

		Material flatMaterial = Material::DefaultBlankMaterial("Wall", Colour(1, 1, 1, 1));

		for (const Linedef& linedef : map.Linedefs)
		{
			bool upperUnpegged = (linedef.flags & 1 << 3) != 0;
			bool lowerUnpegged = (linedef.flags & 1 << 4) != 0;

			if (linedef.leftSidedef == -1 || linedef.rightSidedef == -1)
			{
				//One sided

				int sidedefID;
				bool right;
				if (linedef.leftSidedef == -1)
				{
					sidedefID = linedef.rightSidedef;
					right = true;
				}
				else 
				{
					sidedefID = linedef.leftSidedef;
					right = false;
				}
				const Sidedef& sidedef = map.Sidedefs[sidedefID];
				const Sector& sector = map.Sectors[sidedef.sectorNumber];
				Object* newWall = scene.CreateObject("One-sided wall");
				outputObjects.push_back(newWall);
				scene.CommonModels.push_back(Model("Doom wall"));
				newWall->model = &scene.CommonModels.back();
				Model& sectorModel = *newWall->model;
				
				Vector2 startVert = map.Vertexes[linedef.startVertex];
				Vector2 endVert = map.Vertexes[linedef.endVertex];
								
				MakeWallMesh(sectorModel, scene, startVert, endVert, sector.ceilingHeight, sector.floorHeight, 
								materials.at(sidedef.middleTexture), right, lowerUnpegged ? false : true, sidedef.offset, true, render_data);

				if (sector.ceilingTexture == "F_SKY1") {
					MakeWallMesh(sectorModel, scene, startVert, endVert, sector.ceilingHeight, skyHeight, 
									materials.at("F_SKY1"), right, false, Vector2::Zero(), false, render_data);
					sectorModel.Meshes.back().material->IsSkybox = true;
				}

				Triangulator::ConnectVerts(sectorVerts[sidedef.sectorNumber][linedef.startVertex], sectorVerts[sidedef.sectorNumber][linedef.endVertex]);
			}
			else
			{
				//Two sided

				const Sidedef& rightSidedef = map.Sidedefs[linedef.rightSidedef];
				const Sector& rightSector = map.Sectors[rightSidedef.sectorNumber];
				const Sidedef& leftSidedef = map.Sidedefs[linedef.leftSidedef];
				const Sector& leftSector = map.Sectors[leftSidedef.sectorNumber];
				
				scene.CommonModels.push_back(Model("Doom wall"));
				Object* newWall = scene.CreateObject("Two-sided wall", &scene.CommonModels.back());
				outputObjects.push_back(newWall);
				Model& wallModel = *newWall->model;
				
				Vector2 startVert = map.Vertexes[linedef.startVertex];
				Vector2 endVert = map.Vertexes[linedef.endVertex];

				Material* rightUpperMaterial = materials.at(rightSidedef.upperTexture);
				Material* rightMiddleMaterial = materials.at(rightSidedef.middleTexture);
				Material* rightLowerMaterial = materials.at(rightSidedef.lowerTexture);
				Material* leftUpperMaterial = materials.at(leftSidedef.upperTexture);
				Material* leftMiddleMaterial = materials.at(leftSidedef.middleTexture);
				Material* leftLowerMaterial = materials.at(leftSidedef.lowerTexture);

				bool skyLeft = leftSector.ceilingTexture == "F_SKY1";
				bool skyRight = rightSector.ceilingTexture == "F_SKY1";

				//Lower unpegged textures are weird and for some reason align with the ceiling of the sector, not the floor
				float rightExtraOffset = 0;
				float leftExtraOffset = 0;
				if (lowerUnpegged)
				{
					if (rightLowerMaterial->HasAlbedoMap())
					{
						rightExtraOffset = 0;
					} else {
						rightExtraOffset = (rightSector.ceilingHeight - rightSector.floorHeight) % render_data.Textures[rightLowerMaterial->AlbedoMapID]._height;
					}
					if (leftLowerMaterial->HasAlbedoMap())
					{
						leftExtraOffset = 0;
					} else {
						leftExtraOffset = (leftSector.ceilingHeight - leftSector.floorHeight) % render_data.Textures[leftLowerMaterial->AlbedoMapID]._height;
					}
				}

				if (leftUpperMaterial->Name != "-" && !(skyLeft && skyRight))
				{
					MakeWallMesh(wallModel, scene, startVert, endVert, leftSector.ceilingHeight, rightSector.ceilingHeight, leftUpperMaterial, false, upperUnpegged, leftSidedef.offset, true, render_data);
					
					if (skyLeft)
					{
						MakeWallMesh(wallModel, scene, startVert, endVert, skyHeight, leftSector.ceilingHeight, leftUpperMaterial, false, false, Vector2::Zero(), false, render_data);
						wallModel.Meshes.back().material->IsSkybox = true;
					}
				}
				if (leftLowerMaterial->Name != "-")
				{
					MakeWallMesh(wallModel, scene, startVert, endVert, leftSector.floorHeight, rightSector.floorHeight, leftLowerMaterial, false, !lowerUnpegged, leftSidedef.offset + Vector2(0, leftExtraOffset), true, render_data);
				}
				if (leftMiddleMaterial->Name != "-")
				{
					MakeWallMesh(wallModel, scene, startVert, endVert, std::min(leftSector.ceilingHeight, rightSector.ceilingHeight), std::max(leftSector.floorHeight, rightSector.floorHeight), 
						leftMiddleMaterial, false, !lowerUnpegged, Vector2::Zero(), false, render_data);
				}
			
				if (rightUpperMaterial->Name != "-" && !(skyLeft && skyRight))
				{
					MakeWallMesh(wallModel, scene, startVert, endVert, rightSector.ceilingHeight, leftSector.ceilingHeight, rightUpperMaterial, true, upperUnpegged, rightSidedef.offset, true, render_data);

					if (skyRight)
					{
						MakeWallMesh(wallModel, scene, startVert, endVert, skyHeight, rightSector.ceilingHeight, rightUpperMaterial, true, false, Vector2::Zero(), false, render_data);
						wallModel.Meshes.back().material->IsSkybox = true;
					}
				}
				if (rightLowerMaterial->Name != "-")
				{
					MakeWallMesh(wallModel, scene, startVert, endVert, rightSector.floorHeight, leftSector.floorHeight, rightLowerMaterial, true, !lowerUnpegged, rightSidedef.offset + Vector2(0, rightExtraOffset), true, render_data);
				}
				if (rightMiddleMaterial->Name != "-")
				{
					MakeWallMesh(wallModel, scene, startVert, endVert, std::min(leftSector.ceilingHeight, rightSector.ceilingHeight), std::max(leftSector.floorHeight, rightSector.floorHeight),
						rightMiddleMaterial, true, !lowerUnpegged, Vector2::Zero(), false, render_data);
				}
				
				ConnectVerts(sectorVerts[rightSidedef.sectorNumber][linedef.startVertex], sectorVerts[rightSidedef.sectorNumber][linedef.endVertex]);
				ConnectVerts(sectorVerts[leftSidedef.sectorNumber][linedef.endVertex], sectorVerts[leftSidedef.sectorNumber][linedef.startVertex]);
			}
		}
		
		for (auto& sectorMap : sectorVerts)
		{
			for (auto & pair : sectorMap)
			{
				pair.second.index = pair.first;
				pair.second.position = map.Vertexes[pair.first];
			}
		}

		for (int sectorNumber = 0; sectorNumber < map.Sectors.size(); sectorNumber++)
		{
			const Sector& sector = map.Sectors[sectorNumber];
			std::vector<Triangulator::PolyVert*> verts;
			for (auto& pair : sectorVerts[sectorNumber])
			{
				verts.push_back(&pair.second);
			}

			scene.CommonModels.push_back(Model("Doom floor"));
			Object* newFloor = scene.CreateObject("Floor", &scene.CommonModels.back());
			outputObjects.push_back(newFloor);
			Model& floorModel = scene.CommonModels.back();
			scene.CommonModels.push_back(Model("Doom ceiling"));
			Object* newCeiling = scene.CreateObject("Ceiling", &scene.CommonModels.back());
			outputObjects.push_back(newCeiling);
			Model& ceilingModel = scene.CommonModels.back();
			std::vector<std::vector<int>> tris = Triangulator::Triangulate(verts);
			
			//Floor
			{
				bool sky = sector.floorTexture == "F_SKY1";
				scene.CommonMeshes.push_back(Mesh());
				Mesh& floorMesh = scene.CommonMeshes.back();
				floorModel.Meshes.push_back(MeshInstance(&floorModel, &floorMesh, materials.at(sector.floorTexture)));
				
				for (std::vector<int> triangle : tris)
				{
					const Vector2& vert1 = map.Vertexes[triangle[0]];
					const Vector2& vert2 = map.Vertexes[triangle[2]];
					const Vector2& vert3 = map.Vertexes[triangle[1]];

					floorMesh.Verts.push_back(Vector3(vert1.x, sector.floorHeight, vert1.y) / mapScale);
					floorMesh.Verts.push_back(Vector3(vert2.x, sector.floorHeight, vert2.y) / mapScale);
					floorMesh.Verts.push_back(Vector3(vert3.x, sector.floorHeight, vert3.y) / mapScale);

					floorMesh.UVs.push_back(vert1 / 64.0f);
					floorMesh.UVs.push_back(vert2 / 64.0f);
					floorMesh.UVs.push_back(vert3 / 64.0f);

					floorMesh.AppendTriangle(floorMesh.Verts.size() - 3, floorMesh.Verts.size() - 2, floorMesh.Verts.size() - 1);
				}
				floorModel.Meshes.back().material->IsSkybox = sky;
				floorMesh.CalcNormals();

				if (!sky)
				{
					scene.CommonMeshes.push_back(Mesh(floorMesh));
					floorModel.Meshes.push_back(MeshInstance(&floorModel, &scene.CommonMeshes.back()));
					floorModel.Meshes.back().isVisible = false;
					floorModel.Meshes.back().isShadowCaster = true;
					floorModel.Meshes.back().mesh->Reverse();
				}
			}
			//Ceiling
			{
				bool sky = sector.ceilingTexture == "F_SKY1";
				
				scene.CommonMeshes.push_back(Mesh());
				Mesh& ceilingMesh = scene.CommonMeshes.back();

				ceilingModel.Meshes.push_back(MeshInstance(&ceilingModel, &ceilingMesh, materials.at(sector.ceilingTexture)));

				for (std::vector<int> triangle : tris)
				{
					const Vector2& vert1 = map.Vertexes[triangle[0]];
					const Vector2& vert2 = map.Vertexes[triangle[1]];
					const Vector2& vert3 = map.Vertexes[triangle[2]];

					int ceilingHeight = sector.ceilingTexture == "F_SKY1" ? skyHeight : sector.ceilingHeight;

					ceilingMesh.Verts.push_back(Vector3(vert1.x, ceilingHeight, vert1.y) / mapScale);
					ceilingMesh.Verts.push_back(Vector3(vert2.x, ceilingHeight, vert2.y) / mapScale);
					ceilingMesh.Verts.push_back(Vector3(vert3.x, ceilingHeight, vert3.y) / mapScale);

					ceilingMesh.UVs.push_back(vert1 / 64.0f);
					ceilingMesh.UVs.push_back(vert2 / 64.0f);
					ceilingMesh.UVs.push_back(vert3 / 64.0f);

					ceilingMesh.AppendTriangle(ceilingMesh.Verts.size() - 3, ceilingMesh.Verts.size() - 2, ceilingMesh.Verts.size() - 1);
				}
				ceilingModel.Meshes.back().material->IsSkybox = sky;
				ceilingMesh.CalcNormals();
				
				if (!sky)
				{
					scene.CommonMeshes.push_back(ceilingMesh);
					ceilingModel.Meshes.push_back(MeshInstance(&ceilingModel, &scene.CommonMeshes.back()));
					ceilingModel.Meshes.back().isVisible = false;
					ceilingModel.Meshes.back().isShadowCaster = true;
					ceilingModel.Meshes.back().mesh->Reverse();
				}
			}
		}
		
		for (Object* object : outputObjects)
		{
			object->model->RecalcAABB();
			object->RecalcAABB();
		}

		//Centre the level on 0,0,0
		Bounds bounds = Object::GetBounds(outputObjects);
		Vector3 centre = bounds.Centre();
		for (auto* object : outputObjects)
		{
			object->RecentreOnModel();
			object->Position -= centre;
			object->Position += Vector3(0, 300, 0);
			object->RecalcAABB();
		}
	}

	std::array<Colour, 256> ReadPalette(const DirectoryEntry& entry, const std::vector<char>& buffer)
	{
		std::array<Colour, 256> palette;
		
		for (int i = 0; i < 256; i++)
		{
			palette[i].r = (float)(unsigned char)buffer[entry.filepos + (i * 3) + 0] / 255.0f;
			palette[i].g = (float)(unsigned char)buffer[entry.filepos + (i * 3) + 1] / 255.0f;
			palette[i].b = (float)(unsigned char)buffer[entry.filepos + (i * 3) + 2] / 255.0f;
			palette[i].a = 1;
		}

		return palette;
	}

	void ReadWAD(const std::string& filename, const std::string& mapToLoad, Scene& scene, RenderData& render_data)
	{
		std::ifstream file(filename, std::ios::binary | std::ios::ate);
		if (!file.is_open())
		{
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", (std::string("Could not open file: ") + filename).c_str(), NULL);
			return;
		}

		std::streamsize length = file.tellg();
		file.seekg(0, std::ios::beg);

		std::vector<char> buffer(length);
		if (!file.read(buffer.data(), length))
		{
			return;
		}

		std::string headerID = ReadString(buffer, 0, 4);
		if (headerID != "IWAD" && headerID != "PWAD")
		{
			return;
		}
		int numLumps = ReadInt32(buffer, 0x04);
		int infoTableOfs = ReadInt32(buffer, 0x08);

		std::vector<DirectoryEntry> directory(numLumps);
		std::unordered_map<std::string, DirectoryEntry*> directoryNameIndex;

		for (int i = 0; i < numLumps; i++)
		{
			directory[i] = ReadDirectoryEntry(buffer, infoTableOfs + 0x10 * i);
			directoryNameIndex[directory[i].name] = &directory[i];
		}

		std::unordered_map<std::string, Texture*> textureMap;
		std::vector<MapTexture> mapTextures;
		std::vector<std::string> pNames;
		std::array<Colour, 256> palette;
		bool mapFound = false;
		Map map;
		for (int i = 0; i < directory.size(); i++)
		{
			if (IsMapLump(directory[i]))
			{
				if (!mapFound && directory[i].name == mapToLoad)
				{
					map = ReadMap(directory, i, buffer);
					mapFound = true;
				}
			}
			else if (directory[i].name == "TEXTURE1" || directory[i].name == "TEXTURE2")
			{
				std::vector<MapTexture> newMapTextures = ReadTextureLump(directory[i], buffer);
				mapTextures.insert(mapTextures.end(), newMapTextures.begin(), newMapTextures.end());
			}
			else if (directory[i].name == "PNAMES")
			{
				pNames = ReadPNames(directory[i], buffer, render_data);
			}
			else if (directory[i].name == "PLAYPAL")
			{
				palette = ReadPalette(directory[i], buffer);
			}
		}

		for (MapTexture& mapTexture : mapTextures)
		{
			Texture* texture = render_data.Textures.New(mapTexture.Width, mapTexture.Height, 4);
			textureMap[mapTexture.Name] = texture;

			for (MapPatch& patchInfo : mapTexture.Patches)
			{
				DirectoryEntry& patchEntry = *directoryNameIndex[pNames[patchInfo.PictureID]];
				Picture patchPicture = ReadPicture(patchEntry, buffer, palette, render_data);
				texture->Blit(*patchPicture.texture, patchInfo.OriginX, patchInfo.OriginY);
			}
		}

		std::unordered_map<std::string, Material*> materials;
		materials.emplace("-", render_data.Materials.New());
		for (const auto& [name, texture] : textureMap) {
			GenerateMaterial(name, render_data.Textures.ToIndex(texture), materials);
		}
		for (Sector& sector : map.Sectors)
		{
			Texture* floor_texture = ReadFlat(*directoryNameIndex[sector.floorTexture], buffer, palette, render_data);
			textureMap[sector.floorTexture] = floor_texture;
			GenerateMaterial(sector.floorTexture, render_data.Textures.ToIndex(floor_texture), materials);

			Texture* ceiling_texture = ReadFlat(*directoryNameIndex[sector.ceilingTexture], buffer, palette, render_data);
			textureMap[sector.ceilingTexture] = ceiling_texture;
			GenerateMaterial(sector.ceilingTexture, render_data.Textures.ToIndex(ceiling_texture), materials);
		}

		if (mapFound) {
			GenerateObjectsFromMap(map, scene, materials, render_data);
		}
	}
}