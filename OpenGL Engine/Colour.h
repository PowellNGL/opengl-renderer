#pragma once

#include "Shared.h"

struct Vector4;

struct Colour
{
	float r, g, b, a;

	constexpr Colour() : r(0), g(0), b(0), a(1) {}
	constexpr Colour(const float r, const float g, const float b, const float a) : r(r), g(g), b(b), a(a) {}
	Colour(const Vector4& vec);

	static inline constexpr Colour Default() {
		return DebugValue(Colour{ 1, 0, 1, 1 }, Colour{ 1, 1, 1, 1 });
	}
};