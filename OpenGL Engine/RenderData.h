#pragma once

#include "TypedArena.h"
#include "Material.h"
#include "Texture.h"

using TextureStorage = TypedArena<Texture>;
using MaterialStorage = TypedArena<Material>;

struct RenderData
{
	MaterialStorage Materials;
	TextureStorage Textures;
};