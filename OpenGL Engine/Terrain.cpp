#include <cmath>
#include <unordered_set>

#include "Terrain.h"
#include "Mesh.h"
#include "Scene.h"
#include "RenderData.h"

constexpr float fastPow(const float input, const unsigned int power)
{
	return power == 0 ? 1.0f : input * fastPow(input, power - 1);
}

float Terrain::GetSimplexHeight(const Vector2& position)
{
	float output;
	float mountainScale = (1 - std::fabs(simplex.Noise(position / 10000))) * 0.5f + 0.5f;

	output = (simplex.FractalNoise(position * simplexScale) * 0.5f + 0.5f) * mountainScale;

	return terrainHeight * fastPow(output, 2);
}

//The integer part of chunkSpacePoint is the XY location of the chunk, the fractional part is the UV coordinate within that chunk to sample from
float Terrain::SampleHeightmapsAtPosition(const Vector2& chunkSpacePoint)
{
	Vector2 chunkPos;
	Vector2 chunkUV;
	
	chunkPos.x = floorf(chunkSpacePoint.x);
	chunkPos.y = floorf(chunkSpacePoint.y);

	chunkUV = chunkSpacePoint - chunkPos;

	size_t chunkID = IDOfChunkAtPosition(static_cast<int>(chunkPos.x), static_cast<int>(chunkPos.y));
	if (chunkID == -1)
	{
		return 0.0f;
	} 
	else 
	{ 
		return chunks[chunkID].SampleHeightmapFiltered(chunkUV);
	}
}

void Terrain::MakeListsOfChunksToCheck()
{
	unfilledChunkPositions.clear();
	uncheckedChunks.clear();
	
	const Vector2i directions[4] = { {1, 0}, {0, 1}, {-1, 0}, {0, -1} };
	Vector2i currentPoint = Vector2i(0, 0);

	//Check the centre chunk first as it won't be looked at by the loop
	size_t centreID = IDOfChunkAtPosition(centreChunk.x, centreChunk.y);
	if (centreID == -1)
		unfilledChunkPositions.push_back(Vector2i(centreChunk.x, centreChunk.y));
	else
		uncheckedChunks.push_back(&chunks[centreID]);

	//Loop in a spiral out from the centre chunk
	for (int radius = 0; radius <= maxChunkDistance; radius++)
	{
		currentPoint = Vector2i(-1, -1) * radius;
		for (int direction = 0; direction < 4; direction++)
		{
			for (int i = 0; i < radius * 2; i++)
			{
				int x = currentPoint.x;
				int y = currentPoint.y;

				size_t chunkID = IDOfChunkAtPosition(centreChunk.x + x, centreChunk.y + y);
				if (chunkID == -1)
					unfilledChunkPositions.push_back(Vector2i(centreChunk.x + x, centreChunk.y + y));
				else
					uncheckedChunks.push_back(&chunks[chunkID]);

				currentPoint += directions[direction];
			}
		}
	}

	//Check all the chunks outside maxChunkDistance that weren't covered by the spiral
	for (auto& chunk : chunks)
	{
		if ((!chunk.inPosition) || std::abs(chunk.chunkPos.x - centreChunk.x) > maxChunkDistance || std::abs(chunk.chunkPos.y - centreChunk.y) > maxChunkDistance)
		{
			uncheckedChunks.push_back(&chunk);
		}
	}
}

Terrain::Terrain(Scene* scene, const unsigned int seed, MaterialStorage& materials) : simplex(seed), scene(scene), material(materials.New())
{
	material->Albedo = { 0.4f, 0.6f, 0.3f, 1 };
	material->Roughness = 0.8f;
	material->Metallic = 0.0f;
	material->Emissivity = { 0, 0, 0, 1 };

	chunks.reserve(numChunks);
	for (int x = -maxChunkDistance; x <= maxChunkDistance; x++)
	{
		for (int y = -maxChunkDistance; y <= maxChunkDistance; y++)
		{
			int resolution = targetChunkResolution(Vector2i(x, y));
			chunks.emplace_back(*scene, chunks.size(), *this, resolution);
		}
	}
}

std::vector<Object*> Terrain::UpdateChunks(const Vector2& worldCentre, const unsigned int maxQuadsUpdated, const bool debugVisualiseChanges)
{
	std::vector<Object*> changedChunkObjects;

	Vector2i newCentreChunk = Vector2i(std::roundf(worldCentre.x / chunkSize), std::roundf(worldCentre.y / chunkSize));
	if (centreChunk != newCentreChunk)
	{
		centreChunk = newCentreChunk;
		MakeListsOfChunksToCheck();
	}
	
	//Do initial debug colours
	for (auto& chunk : chunks)
	{
		chunk.model.Meshes.front().material = material;

		if (debugVisualiseChanges && chunk.chunkPos == centreChunk) {
			//chunk.model.Meshes.front().material.Albedo = Colour(1, 0.5f, 0.5f, 1);
		}
	}

	if (uncheckedChunks.empty() && unfilledChunkPositions.empty())
		return changedChunkObjects;

	std::vector<ChunkNeighbourInformation> previousNeighbourInformation(chunks.size());
	for (auto& chunk : chunks)
	{
		//Get a list of each chunk's current neighbours and their resolutions
		previousNeighbourInformation[chunk.chunkID] = chunk.neighbours;
	}

	//Reposition every chunk that needs to be moved into one of the unfilled spaces found above
	//Also change the resolution of any chunk that needs it (because it's a different distance from the centre now)
	unsigned int totalQuadsUpdated = 0;
	std::unordered_set<size_t> changedChunkIDs;
	while (!uncheckedChunks.empty())
	{
		TerrainChunk& chunk = *uncheckedChunks.front();
		
		int targetResolution = targetChunkResolution(chunk.chunkPos);
		
		//Add chunks that need to be moved
		if ((!chunk.inPosition) || std::abs(chunk.chunkPos.x - centreChunk.x) > maxChunkDistance || std::abs(chunk.chunkPos.y - centreChunk.y) > maxChunkDistance)
		{
			Vector2i newChunkPosition = unfilledChunkPositions.front();
			chunk.SetChunkPosition(newChunkPosition);
			unfilledChunkPositions.erase(unfilledChunkPositions.begin());
			changedChunkIDs.insert(chunk.chunkID);

			totalQuadsUpdated += chunk.resolution * chunk.resolution;
		}
		//Add chunks that only need their resolutions changed
		else if (targetResolution != chunk.resolution)
		{
			chunk.resolution = targetResolution;
			changedChunkIDs.insert(chunk.chunkID);

			totalQuadsUpdated += chunk.resolution * chunk.resolution;
		}

		uncheckedChunks.erase(uncheckedChunks.begin());

		if (totalQuadsUpdated >= maxQuadsUpdated)
			break;
	}
	
	//Remake the heightmaps for the all the chunks identified in the previous step
	for (const size_t changedChunkID : changedChunkIDs)
	{
		chunks[changedChunkID].RemakeHeightmap();
	}

	for (auto& chunk : chunks)
	{
		if (!chunk.inPosition)
		{
			continue;
		}

		//Update the neighbour information for each chunk now that some of them have been moved
		chunk.neighbours = ChunkNeighbourInformation(chunk);

		bool alreadyChanged = changedChunkIDs.find(chunk.chunkID) != changedChunkIDs.end();

		//Tell each chunk to update its edges based on its new neighbours
		bool edgesChanged = chunk.MatchHeightmapEdges(previousNeighbourInformation[chunk.chunkID], alreadyChanged);

		//If its edges were changed, or it was updated in an earlier step, return it from the function
		if (edgesChanged || alreadyChanged)
		{
			chunk.UpdateMesh();
			changedChunkObjects.push_back(chunk.object);

			if (debugVisualiseChanges)
			{
				//chunk.model.Meshes.front().material.Albedo = Colour(0.5f, 0.5f, 1, 1);
			}
		}
	}
	
	return changedChunkObjects;
}

TerrainChunk::TerrainChunk(Scene& scene, const size_t chunkID, Terrain& parent, const int resolution) 
	: chunkID(chunkID), parent(&parent), resolution(resolution), model("Terrain Chunk Model")
{
	model.Meshes.push_back(MeshInstance(&model, &mesh, parent.material));
	object = scene.CreateObject("Terrain chunk " + std::to_string(chunkID), &model);
}

//Set the correct number of verts, create the triangle list and position the vertices to match the heightmap
void TerrainChunk::UpdateMesh()
{
	Mesh& mesh = *model.Meshes.front().mesh;
	mesh.Verts.resize((resolution + 1) * (resolution + 1));
	mesh.Normals.resize((resolution + 1) * (resolution + 1));
	mesh.UVs.resize((resolution + 1) * (resolution + 1));
	mesh.Tangents.resize((resolution + 1) * (resolution + 1));
	mesh.Binormals.resize((resolution + 1) * (resolution + 1));

	mesh.Triangles.clear();

	//Fill in the triangle indices now, as they shouldn't need to change until it is next resized
	for (uint32_t x = 0; x < resolution; x++)
	{
		for (uint32_t z = 0; z < resolution; z++)
		{
			uint32_t x0 = x;
			uint32_t x1 = x + 1;
			uint32_t z0 = z * (resolution + 1);
			uint32_t z1 = (z + 1) * (resolution + 1);

			if ((x + z) % 2 == 0)
			{
				mesh.AppendTriangle(x0 + z0, x1 + z0, x0 + z1);
				mesh.AppendTriangle(x1 + z0, x1 + z1, x0 + z1);
			} else {
				mesh.AppendTriangle(x0 + z0, x1 + z0, x1 + z1);
				mesh.AppendTriangle(x0 + z0, x1 + z1, x0 + z1);
			}
		}
	}
	
	//Set the heights from the heightmap
	float startPos = -parent->chunkSize / 2;
	for (uint32_t x = 0; x < resolution + 1; x++)
	{
		for (uint32_t z = 0; z < resolution + 1; z++)
		{
			Vector3& vert = mesh.Verts[x * (resolution + 1) + z];
			vert.x = startPos + (parent->chunkSize / resolution) * x;
			vert.z = startPos + (parent->chunkSize / resolution) * z;
			vert.y = heightmap[x][z];
		}
	}

	CalcNormalsFromHeightmap();
	model.RecalcAABB();
	object->RecalcAABB();
}

//Just creates the 2D array full of heights, doesn't affect the mesh
void TerrainChunk::RemakeHeightmap()
{
	heightmap.resize(resolution + 1);
	for (auto& heightmapRow : heightmap)
	{
		heightmapRow.resize(resolution + 1);
	}

	Vector2 firstVertWorldPos = Vector2(object->Position.x, object->Position.z) - Vector2(parent->chunkSize / 2, parent->chunkSize / 2);
	float pixelScale = parent->chunkSize / resolution;

	for (int x = 0; x < resolution + 1; x++)
	{
		for (int y = 0; y < resolution + 1; y++)
		{
			Vector2 pixelWorldPos = firstVertWorldPos + Vector2(x, y) * pixelScale;
			heightmap[x][y] = parent->GetSimplexHeight(pixelWorldPos);
		}
	}
}

void TerrainChunk::SetChunkPosition(const Vector2i& chunkPosition)
{
	if (inPosition)
	{
		parent->chunkPositions[chunkPos.x].erase(chunkPos.y);
	}
	inPosition = true;
	chunkPos = chunkPosition;
	object->Position = Vector3((float)chunkPos.x, 0, (float)chunkPos.y) * parent->chunkSize;
	resolution = parent->targetChunkResolution(chunkPosition);
	parent->chunkPositions[chunkPos.x][chunkPos.y] = chunkID;
}

//Change the heightmap along the edges of the chunk so it matches the edge of each adjacent chunk regardless of its resolution
//Returns true if any edges needed to be updated
bool TerrainChunk::MatchHeightmapEdges(const ChunkNeighbourInformation& previousNeighbours, const bool thisChunkHasChanged)
{
	int32_t resolution_i = static_cast<int32_t>(resolution);
	Vector2i edgeStart[4] = { {0, 0}, {resolution_i, 0}, {resolution_i, resolution_i}, {0, resolution_i} };	//Point to start walking along each edge from
	Vector2i edgeStep[4] = { {1, 0}, {0, 1}, {-1, 0}, {0, -1} };											//Direction to travel to go along the edge
	Vector2i adjacentChunkOffset[4] = { {0, -1}, {1, 0}, {0, 1}, {-1, 0} };									//Position in chunk coords of adjacent chunk relative to this one
	bool changeMade = false;

	for (int edge = 0; edge < 4; edge++)
	{
		if (previousNeighbours.adjacentChunks[edge] == neighbours.adjacentChunks[edge] && !thisChunkHasChanged)
			continue;	//This edge hasn't changed so don't update anything
		
		size_t adjacentChunkID = parent->IDOfChunkAtPosition(chunkPos + adjacentChunkOffset[edge]);
		if (adjacentChunkID == -1)
			continue;	//This means we're at the edge of the loaded chunk range


		TerrainChunk& adjChunk = parent->chunks[adjacentChunkID];
		Vector2 firstVertWorldPos = Vector2(object->Position.x, object->Position.z) - Vector2(parent->chunkSize / 2, parent->chunkSize / 2);
		float pixelScale = parent->chunkSize / resolution;
		float resolutionScale = 1.0f / resolution;
		for (size_t i = 0; i <= resolution; i++)
		{
			Vector2i currentHeightmapPoint = edgeStart[edge] + edgeStep[edge] * i;		//Current point on the heightmap in pixel coordinates
			Vector2 currentUV = Vector2(currentHeightmapPoint) * resolutionScale;				//Current point on the chunk in UV coordinates
			Vector2 pixelWorldPos = firstVertWorldPos + Vector2(currentHeightmapPoint) * pixelScale;

			//If the adjacent chunk is larger than this one, we don't want to match its vertices, we want to just revert
			//this edge to the unmodified simplex noise height
			if (adjChunk.resolution >= resolution)
			{
				//Recalculate the points on the edge with the simplex generator
				heightmap[currentHeightmapPoint.x][currentHeightmapPoint.y] = parent->GetSimplexHeight(pixelWorldPos);
			} else {
				//Sample the equivalent point on the adjacent chunk by subtracting adjacentChunkOffset from the current UV
				//If the adjacent chunk has a different resolution this will automatically interpolate values within the lower of the two resolutions
				heightmap[currentHeightmapPoint.x][currentHeightmapPoint.y] = adjChunk.SampleHeightmapFiltered(currentUV - Vector2(adjacentChunkOffset[edge]));
			}
		}

		changeMade = true;
	}

	return changeMade;
}

void TerrainChunk::CalcNormalsFromHeightmap()
{
	if (model.Meshes.empty())
	{
		return;
	}
	Mesh& mesh = *model.Meshes.front().mesh;

	float UVPixelSize = 1.0f / static_cast<float>(resolution);	//Size of a heightmap pixel in UV coordinates
	float UVSampleMultiplier = static_cast<float>(parent->nearestChunkResolution) / static_cast<float>(resolution);	//Multiplier for the height of any pixel sampled with scaled UV offsets
	float worldPixelSize = parent->chunkSize / static_cast<float>(resolution);	//Size of a pixel on this chunk in world coordinates

	const Vector2i heightmapSampleOffsets[4] = { Vector2i(0, -1), Vector2i(1, 0), Vector2i(0, 1), Vector2i(-1, 0) };
	Vector2 scaledUVSampleOffsets[4];

	//These offsets are scaled to match the size of a pixel on the highest resolution heightmap on the terrain, in UV coordinates
	for (int i = 0; i < 4; i++)
	{
		scaledUVSampleOffsets[i] = Vector2(heightmapSampleOffsets[i]) / parent->nearestChunkResolution;
	}

	for (int x = 0; x <= resolution; x++)
	{
		for (int y = 0; y <= resolution; y++)
		{
			Vector2 centreUV = Vector2(x, y) * UVPixelSize;
			float centreHeight = heightmap[x][y];
			float sampleHeights[4];

			//Sample from the 4 adjacent pixels
			for (int i = 0; i < 4; i++)
			{
				Vector2i heightmapSamplePoint = Vector2i(x + heightmapSampleOffsets[i].x, y + heightmapSampleOffsets[i].y);

				if (heightmapSamplePoint.x < 0 || heightmapSamplePoint.x > resolution || heightmapSamplePoint.y < 0 || heightmapSamplePoint.y > resolution)
				{
					//When the sample point is outside the heightmap, this section instead looks up the pixel 1 in from the edge on the neighbouring chunk.
					//This is done by generating UV coordinates and using bilinear filtering to sample the point from the chunk's heightmap using those coordinates
					//The sample point isn't offset by a whole heightmap pixel, however, as the adjacent chunk may have a higher heightmap resolution and 
					//we want to sample from the first pixel in from the edge. Instead, the UV is offset by the size of a pixel on the highest-resolution chunk's heightmap 
					//(as this will never overshoot the pixel we're trying to look up) then scaled as if it was sampled from a point 1 pixel away
					Vector2 UVSamplePoint = Vector2(chunkPos) + centreUV + scaledUVSampleOffsets[i];
					sampleHeights[i] = centreHeight + (parent->SampleHeightmapsAtPosition(UVSamplePoint) - centreHeight) * UVSampleMultiplier;
				} else {
					//This branch is simpler. It just looks up the adjacent pixel on this chunk's heightmap
					sampleHeights[i] = heightmap[x + heightmapSampleOffsets[i].x][y + heightmapSampleOffsets[i].y];
				}
			}
			
			Vector3 northSouthDiff = Vector3(2 * worldPixelSize, sampleHeights[1] - sampleHeights[3], 0);
			Vector3 westEastDiff = Vector3(0, sampleHeights[2] - sampleHeights[0], 2 * worldPixelSize);
			Vector3 normal = -Cross(northSouthDiff, westEastDiff);
			mesh.Normals[x * (resolution + 1) + y] = normal.Normalised();
		}
	}
}

//Take UV coords, sample the 4 nearest points on the heightmap and do some bilinear filtering
float TerrainChunk::SampleHeightmapFiltered(const Vector2& coords) const
{
	//Convert UV to pixel coordinates
	Vector2 scaledCoords = coords * resolution;

	float x0, x1, y0, y1;
	float xFrac, yFrac;
	xFrac = std::modf(scaledCoords.x, &x0);
	x1 = x0 + 1;
	yFrac = std::modf(scaledCoords.y, &y0);
	y1 = y0 + 1;

	x0 = std::fminf(resolution, std::fmaxf(0.0f, x0));
	x1 = std::fminf(resolution, std::fmaxf(0.0f, x1));
	y0 = std::fminf(resolution, std::fmaxf(0.0f, y0));
	y1 = std::fminf(resolution, std::fmaxf(0.0f, y1));

	float xMid0 = heightmap[(int)x0][(int)y0] * (1 - xFrac) + heightmap[(int)x1][(int)y0] * xFrac;
	float xMid1 = heightmap[(int)x0][(int)y1] * (1 - xFrac) + heightmap[(int)x1][(int)y1] * xFrac;
	float output = xMid0 * (1 - yFrac) + xMid1 * yFrac;

	return output;
}

bool TerrainChunk::CompareDistance(const TerrainChunk* chunkA, const TerrainChunk* chunkB)
{
	Vector2i v1 = chunkA->chunkPos - chunkA->parent->centreChunk;
	Vector2i v2 = chunkB->chunkPos - chunkA->parent->centreChunk;
	return (v1.x * v1.x + v1.y * v1.y > v2.x * v2.x + v2.y * v2.y);
}

ChunkNeighbourInformation::ChunkNeighbourInformation()
{
	adjacentChunks[0] = std::pair<int, int>(-1, 0);
	adjacentChunks[1] = std::pair<int, int>(-1, 0);
	adjacentChunks[2] = std::pair<int, int>(-1, 0);
	adjacentChunks[3] = std::pair<int, int>(-1, 0);
}

ChunkNeighbourInformation::ChunkNeighbourInformation(const TerrainChunk& chunk)
{
	const Vector2i offsets[4] = { {0, -1}, {1, 0}, {0, 1}, {-1, 0} };

	for (int i = 0; i < 4; i++)
	{
		size_t adjacentChunkID = chunk.parent->IDOfChunkAtPosition(chunk.chunkPos + offsets[i]);
		adjacentChunks[i].first = adjacentChunkID;
		if (adjacentChunkID == -1)
		{
			adjacentChunks[i].second = 0;
		} else {
			adjacentChunks[i].second = chunk.parent->chunks[adjacentChunkID].resolution;
		}
	}
}

bool ChunkNeighbourInformation::operator==(const ChunkNeighbourInformation & other)
{
	return (
		adjacentChunks[0] == other.adjacentChunks[0] &&
		adjacentChunks[1] == other.adjacentChunks[1] &&
		adjacentChunks[2] == other.adjacentChunks[2] &&
		adjacentChunks[3] == other.adjacentChunks[3]
		);
}
