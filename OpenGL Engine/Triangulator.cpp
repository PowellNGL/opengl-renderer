#include "Triangulator.h"
#include <string>

namespace Triangulator
{
	std::vector<std::vector<int>> Triangulate(std::vector<PolyVert*>& verts)
	{
		std::vector<std::vector<int>> triangles;
		std::unordered_set<int> failedStartPoints;

		int i = 0;
		while (!verts.empty() && i++ < 1000)
		{
			RemoveColinearVerts(verts);	//It may be overkill to do this every time but I'm feeling overcautious
			ConnectOrRemoveLeaves(verts);

			std::vector<PolyVert*> triangle;

			PolyVert* startPoint = nullptr;
			for (PolyVert* vert : verts)
			{
				if (failedStartPoints.find(vert->index) == failedStartPoints.end())
				{
					startPoint = vert;
					break;
				}
			}
			if (startPoint == nullptr)
			{
				break;
			}

			//StartB is the first vertex in the triangle, startC is the second. StartA is the vertex that connects to startB along the same edge
			PolyVert& startB = *startPoint;
			PolyVert& startC = **startB.next.begin();
			PolyVert& startA = LeastClockwiseIncomingEdge(startB, startC);

			triangle.push_back(&startB);
			triangle.push_back(&startC);
			
			for (PolyVert* vert : verts)
			{
				if (vert->index == startB.index || vert->index == startC.index)
				{
					continue;
				}
				else if ((VertsAreConnected(startC, *vert) || vert->index == startA.index) &&
					Cross(vert->position - startB.position, startC.position - startB.position) == 0)	//If the triangle has zero area
				{
					//Skip all the checks and add it - it'll be removed from the poly but not added to the final output
				}
				else if (!PointIsInsideCorner(startA.position, startB.position, startC.position, vert->position))
				{
					continue;
				}
				else if (!PointIsInsideCorner(startB.position, startC.position, MostClockwiseOutgoingEdge(startB, startC).position, vert->position))
				{
					continue;
				}
				else if (LineIntersectsPoly(startB, *vert, verts))
				{
					continue;
				}
				else if (LineIntersectsPoly(startC, *vert, verts))
				{
					continue;
				}
				else if (AnyPointInsideTriangle(startB, startC, *vert, verts))
				{
					continue;
				}
				else if (Cross(vert->position - startB.position, startC.position - startB.position) == 0)
				{
					continue;
				}

				triangle.push_back(vert);
				break;
			}

			if (triangle.size() < 3)
			{
				failedStartPoints.insert(startB.index);
				continue;
			}

			//Extract the triangle from the polygon while keeping its edges complete
			SwapEdgeStates(triangle, verts);

			triangles.push_back(std::vector<int>());
			for (auto vert : triangle)
			{
				triangles.back().push_back(vert->index);
			}
		}

		return triangles;
	}

	//If a vert ever has more incoming edges than outgoing edges (or vice versa), that means it's part of an unclosed polygon
	//This function detects verts with only one edge (leaves) and tries to find another vert with the same properties to connect it to.
	//If there isn't another one, it recursively trims leaves until there are none left
	void ConnectOrRemoveLeaves(std::vector<PolyVert*>& verts)
	{
		std::unordered_set<PolyVert*> incomingLeaves;
		std::unordered_set<PolyVert*> outgoingLeaves;

		//Find leaves with only one edge
		for (PolyVert* vert : verts)
		{
			if (vert->next.size() == 1 && vert->prev.size() == 0)
			{
				outgoingLeaves.insert(vert);
			}
			else if (vert->prev.size() == 1 && vert->next.size() == 0)
			{
				incomingLeaves.insert(vert);
			}
		}

		if (!outgoingLeaves.empty() && !incomingLeaves.empty())
		{
			bool changeMade = false;
			//Attempt to pair leaves together according to how far they are from each other
			//It's a bit of a naive approach but it should work as long as the maps aren't too badly malformed
			do {
				changeMade = false;
				for (PolyVert* vert : incomingLeaves)
				{
					bool candidateFound = false;
					PolyVert* closestCandidate = *outgoingLeaves.begin();
					float closestDistance = std::numeric_limits<float>::max();

					for (PolyVert* candidate : outgoingLeaves)
					{
						float distance = (candidate->position - vert->position).Magnitude();
						if (distance < closestDistance)
						{
							closestCandidate = candidate;
							closestDistance = distance;
							candidateFound = true;
						}
					}

					if (candidateFound)
					{
						changeMade = true;
						ConnectVerts(*vert, *closestCandidate);
						outgoingLeaves.erase(closestCandidate);
						incomingLeaves.erase(vert);
						break;
					}
				}
			} while (changeMade);
		}

		//Recursively trim leaves until there are none left
		bool changeMade = false;
		do
		{
			changeMade = false;
			for (PolyVert* vert : verts)
			{
				if (vert->next.size() == 1 && vert->prev.size() == 0)
				{
					DisconnectVerts(*vert, **vert->next.begin());
					changeMade = true;
					break;
				}
				else if (vert->prev.size() == 1 && vert->next.size() == 0)
				{
					DisconnectVerts(**vert->prev.begin(), *vert);
					changeMade = true;
					break;
				}
			}
		} while (changeMade);

		RemoveIsolatedVerts(verts);
	}

	bool VertsAreConnected(const PolyVert& startVert, const PolyVert& endVert)
	{
		if (std::find(startVert.next.begin(), startVert.next.end(), &endVert) == startVert.next.end())
		{
			return false;
		}

		return true;
	}

	void DisconnectVerts(PolyVert& startVert, PolyVert& endVert)
	{
		for (auto it = startVert.next.begin(); it != startVert.next.end(); it++)
		{
			if (*it == &endVert)
			{
				startVert.next.erase(&endVert);
				endVert.prev.erase(&startVert);

				return;
			}
		}
	}

	void ConnectVerts(PolyVert& startVert, PolyVert& endVert)
	{
		if (&startVert == &endVert)
		{
			return;
		}
		startVert.next.insert(&endVert);
		endVert.prev.insert(&startVert);
	}

	//Remove verts with no edges
	void RemoveIsolatedVerts(std::vector<PolyVert*>& verts)
	{
		auto it = verts.begin();
		while (it != verts.end())
		{
			if ((**it).next.empty() && (**it).prev.empty())
			{
				it = verts.erase(it);
			}
			else
			{
				++it;
			}
		}
	}

	bool LineIntersectsPoly(const PolyVert& start, const PolyVert& end, const std::vector<PolyVert*>& verts)
	{
		//If they're connected they don't intersect
		if (VertsAreConnected(start, end) || VertsAreConnected(end, start))
		{
			return false;
		}

		for (const PolyVert* start2 : verts)
		{
			for (const PolyVert* end2 : start2->next)
			{
				if (start2 == &start || end2 == &end || start2 == &end || end2 == &start)
				{
					continue;
				}

				if (CheckLineIntersection(start.position, end.position, start2->position, end2->position))
				{
					return true;
				}
			}
		}
		return false;
	}

	//They intersect if:
	//	ACD and BCD have opposite windings, and
	//	ABC and ABD have opposite windings
	bool CheckLineIntersection(const Vector2& A, const Vector2& B, const Vector2& C, const Vector2& D)
	{
		float ABC = Cross(B - A, C - A);
		float ABD = Cross(B - A, D - A);
		float ACD = Cross(C - A, D - A);
		float BCD = Cross(C - B, D - B);

		return (ABC < 0 != ABD < 0) && (ACD < 0 != BCD < 0);
	}

	//Gives a clockwise rotation around origin from pointA to pointB
	float RotationToPoint(const Vector2& origin, const Vector2& pointA, const Vector2& pointB)
	{
		Vector2 direction1 = pointA - origin;
		Vector2 direction2 = pointB - origin;
		float rotation1 = atan2f(direction1.y, direction1.x);
		float rotation2 = atan2f(direction2.y, direction2.x);
		float out = rotation2 - rotation1;
		if (out < 0)
		{
			out += ((int)(out / (pi * 2)) + 1) * pi * 2;
		}
		out -= (int)(out / (pi * 2));
		return out;
	}

	//Given an edge AB, find the vertex C which creates the most acute angle on the right hand side of B
	PolyVert& MostClockwiseOutgoingEdge(PolyVert& A, PolyVert& B)
	{
		float lowestInteriorAngle = std::numeric_limits<float>::max();
		PolyVert* currentBest = *B.next.begin();
		for (auto vert : B.next)
		{
			float interiorAngle = RotationToPoint(B.position, vert->position, A.position);
			if (interiorAngle < lowestInteriorAngle)
			{
				currentBest = vert;
				lowestInteriorAngle = interiorAngle;
			}
		}

		return *currentBest;
	}

	//Given an edge BC, find the vertex A which creates the most acute angle on the right hand side of B
	PolyVert& LeastClockwiseIncomingEdge(const PolyVert& B, const PolyVert& C)
	{
		float highestExteriorAngle = 0;
		PolyVert* currentBest = *B.prev.begin();

		for (auto vert : B.prev)
		{
			float exteriorAngle = RotationToPoint(B.position, vert->position, C.position);
			if (exteriorAngle > highestExteriorAngle)
			{
				currentBest = vert;
			}
		}
		return *currentBest;
	}

	bool PointIsInsideCorner(const Vector2& cornerA, const Vector2& cornerB, const Vector2& cornerC, const Vector2& point)
	{
		float AngleBA = RotationToPoint(cornerB, cornerC, cornerA);
		float AngleBP = RotationToPoint(cornerB, cornerC, point);

		return (AngleBP >= 0 && AngleBP <= AngleBA);
	}

	bool AnyPointInsideTriangle(const PolyVert& A, const PolyVert& B, const PolyVert& C, const std::vector < PolyVert*>& verts)
	{
		for (PolyVert* vert : verts)
		{
			if (vert->index == A.index || vert->index == B.index || vert->index == C.index)
			{
				continue;
			}

			float ABTest = Cross(B.position - A.position, vert->position - A.position);
			float BCTest = Cross(C.position - B.position, vert->position - B.position);
			float CATest = Cross(A.position - C.position, vert->position - C.position);

			if (ABTest > 0 && BCTest > 0 && CATest > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	//For each pair of verts AB in edgesToSwap, remove the edge if one exists between them, or add an edge from B to A if there isn't 
	void SwapEdgeStates(std::vector<PolyVert*>& edgesToSwap, std::vector<PolyVert*>& allVerts)
	{
		PolyVert* previousVert = edgesToSwap.back();
		for (PolyVert* vert : edgesToSwap)
		{
			if (VertsAreConnected(*previousVert, *vert))
			{
				DisconnectVerts(*previousVert, *vert);
			}
			else
			{
				if (!VertsAreConnected(*vert, *previousVert))
				{
					ConnectVerts(*vert, *previousVert);
				}
			}
			previousVert = vert;
		}
		RemoveIsolatedVerts(allVerts);
	}

	//If any three connected verts form a triangle with area 0, that triangle can be safely removed from the polygon in order to simplify it
	void RemoveColinearVerts(std::vector<PolyVert*>& verts)
	{
		RemoveIsolatedVerts(verts);

		bool changeMade = false;
		do {
			changeMade = false;
			for (PolyVert* vertB : verts)
			{
				for (PolyVert* vertA : vertB->prev)
				{
					if (vertB->next.empty())
					{
						continue;
					}
					PolyVert* vertC = &MostClockwiseOutgoingEdge(*vertA, *vertB);
					if (Cross(vertB->position - vertA->position, vertC->position - vertB->position) == 0)
					{
						std::vector<PolyVert*> edgesToSwap({ vertA, vertB, vertC });
						SwapEdgeStates(edgesToSwap, verts);
						changeMade = true;
						break;
					}
				}
				if (changeMade) break;
			}
		} while (changeMade);
	}
}