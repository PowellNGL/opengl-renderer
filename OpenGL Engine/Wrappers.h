#pragma once

#include <GL/glew.h>
#include <vector>
#include <memory>
#include <SDL.h>
#include <unordered_map>
#include <limits>
#include <cassert>
#include <filesystem>
#include <iostream>

#include "Vector.h"

struct PackedVertex;
struct Texture;
struct VertexArrayObject;
struct VertexBuffers;
struct ElementBuffer;

constexpr inline GLuint NULL_GLUINT = std::numeric_limits<GLuint>::max();

enum class TextureBufferType
{
	Flat,
	Cubemap,
	Array
};

enum class TextureEdgeMode
{
	ClampToEdge,
	ClampToBorder,
	Repeat
};

struct VertexBuffers
{
public:
	enum class Type
	{
		Position = 0,
		Normal = 1,
		UV = 2,
		Tangent = 3,
		Binormal = 4,
		Index = 5
	};

	std::array<GLuint, 6> BufferIndices;
	size_t NumVerts = 0;
	size_t NumIndices = 0;

	VertexArrayObject& VAO;

	VertexBuffers(VertexArrayObject& vao, size_t numVerts, size_t numIndices);
	~VertexBuffers();
	VertexBuffers(VertexBuffers&& other) = delete;
	VertexBuffers(VertexBuffers& other) = delete;
	VertexBuffers& operator= (VertexBuffers& other) = delete;
	VertexBuffers& operator= (VertexBuffers&& other) = delete;
};

struct VertexArrayObject
{
private:
	GLuint glIndex;

public:
	inline GLuint GLIndex() const { return glIndex; }

	VertexArrayObject();
	~VertexArrayObject()
	{
		if (glIndex != NULL_GLUINT)
		{
			glDeleteVertexArrays(1, &glIndex);
		}
	}

	VertexArrayObject(VertexArrayObject&& other) noexcept
	{
		glIndex = other.glIndex;
		other.glIndex = NULL_GLUINT;
	}

	VertexArrayObject& operator= (VertexArrayObject&& other) noexcept
	{
		glIndex = other.glIndex;
		other.glIndex = NULL_GLUINT;
		return *this;
	}

	VertexArrayObject(VertexArrayObject& other) = delete;
	VertexArrayObject& operator= (VertexArrayObject& other) = delete;
};

template<typename T>
struct UniformBufferObject
{
private:
	GLuint _glIndex{ NULL_GLUINT };
	size_t _size{};
	GLenum _usage;

	void validate() const {
		assert(_glIndex != NULL_GLUINT);
	}

public:
	GLuint GLIndex() const { return _glIndex; };
	UniformBufferObject(const GLuint binding, size_t initialSize, GLenum usage) : _usage(usage) {
		glGenBuffers(1, &_glIndex);
		glBindBufferBase(GL_UNIFORM_BUFFER, binding, _glIndex);
		if (initialSize != 0) {
			Resize(initialSize);
		}
	}
	~UniformBufferObject() {
		if (_glIndex != NULL_GLUINT) {
			glDeleteBuffers(1, &_glIndex);
		}
	}
	UniformBufferObject(UniformBufferObject&& other) noexcept {
		_glIndex = other._glIndex;
		_size = other._size;
		other._glIndex = NULL_GLUINT;
	}
	UniformBufferObject& operator= (UniformBufferObject&& other) noexcept {
		_glIndex = other._glIndex;
		_size = other._size;
		other._glIndex = NULL_GLUINT;
		return *this;
	}
	UniformBufferObject(const UniformBufferObject& other) = delete;
	UniformBufferObject& operator= (const UniformBufferObject& other) = delete;

	void Resize(size_t size)
	{
		validate();
		_size = size;
		glNamedBufferData(_glIndex, size * sizeof(T), NULL, _usage);
	}

	T* MapForWrite() const
	{
		validate();
		glInvalidateBufferData(_glIndex);
		glMapNamedBuffer(_glIndex, GL_WRITE_ONLY);
	}

	void Unmap() const
	{
		validate();
		glUnmapBuffer(_glIndex);
	}

	void UploadAll(const T* data, bool invalidate = false) const
	{
		validate();
		if (invalidate) {
			glInvalidateBufferData(_glIndex);
		}
		glNamedBufferSubData(_glIndex, 0, _size * sizeof(T), data);
	}
};

struct TextureBufferOptions
{
public:
	TextureBufferType Type = TextureBufferType::Flat;
	int NumLayers = 1;
	bool UseLinearFilter = false;
	bool UseShadowTest = false;
	bool UseMipMap = false;
	bool UseAnisotropicFiltering = false;
	TextureEdgeMode EdgeMode = TextureEdgeMode::ClampToBorder;
};

struct TextureBuffer
{
private:
	GLuint glIndex;
	GLuint glTarget;

public:
	GLuint GLIndex() const { return glIndex; }
	GLuint GLTarget() const { return glTarget; }

	TextureBuffer() : glIndex(NULL_GLUINT), glTarget(GL_TEXTURE_2D) {};

	~TextureBuffer()
	{
		if (glIndex != NULL_GLUINT)
		{
			glDeleteTextures(1, &glIndex);
		}
	}

	TextureBuffer(TextureBuffer&& other) noexcept : glIndex(other.glIndex), glTarget(other.glTarget)
	{
		other.glIndex = NULL_GLUINT;
	};

	TextureBuffer& operator= (TextureBuffer&& other) noexcept
	{
		glIndex = other.glIndex;
		glTarget = other.glTarget;

		other.glIndex = NULL_GLUINT;
		return *this;
	}

	TextureBuffer(const TextureBuffer &other) = delete;
	TextureBuffer& operator= (const TextureBuffer &other) = delete;
	TextureBuffer(const Vector2i& resolution, const GLint format, const std::vector<const Texture*>& startingTexture, const TextureBufferOptions& options = TextureBufferOptions());
	TextureBuffer(const Vector2i& resolution, const GLint format, const Texture* startingTexture, const TextureBufferOptions& options = TextureBufferOptions())
		: TextureBuffer(resolution, format, std::vector<const Texture*>({ startingTexture }), options) {}
};

struct Framebuffer
{
private:
	GLuint glIndex;
	
public:
	GLuint GLIndex() const { return glIndex; };

	Framebuffer()
	{
		glGenFramebuffers(1, &glIndex);
	}

	~Framebuffer()
	{
		if (glIndex != NULL_GLUINT)
		{
			glDeleteFramebuffers(1, &glIndex);
		}
	}

	Framebuffer(Framebuffer&& other) noexcept : glIndex(other.glIndex)
	{
		other.glIndex = NULL_GLUINT;
	}

	Framebuffer& operator= (Framebuffer&& other) noexcept
	{
		glIndex = other.glIndex;
		other.glIndex = NULL_GLUINT;
		return *this;
	}

	Framebuffer(const Framebuffer& other) = delete;
	Framebuffer& operator= (const Framebuffer& other) = delete;
};

struct ShaderPreprocessorConstant
{
public:
	std::string Name;
	std::string Value;

	ShaderPreprocessorConstant(const std::string& name, const std::string& value) : Name(name), Value(value) {}
};

struct Program
{
protected:
	GLuint glIndex = NULL_GLUINT;

	static std::unordered_map<std::string, std::string> shaderFileSources;

public:
	GLuint GLIndex() const { return glIndex; };

	static std::string& ReadShaderFile(const std::filesystem::path& filepath);
	static void ClearShaderFiles();

	Program() {};

	Program(
		const std::filesystem::path& vertShaderFile,
		const std::filesystem::path& geomShaderFile, 
		const std::filesystem::path& fragShaderFile, 
		const std::filesystem::path& compShaderFile,
		const std::initializer_list<ShaderPreprocessorConstant>& constants = {})
	{
		glIndex = glCreateProgram();
		GLuint vertShaderID = NULL_GLUINT;
		GLuint geomShaderID = NULL_GLUINT;
		GLuint fragShaderID = NULL_GLUINT;
		GLuint compShaderID = NULL_GLUINT;

		using namespace std::filesystem;

		path shaderDir = std::filesystem::current_path() / "Shaders";
		path headerFile = shaderDir / "ShaderHeader.glsl";

		std::string preprocessorBlock = "";
		for (const ShaderPreprocessorConstant& constant : constants)
		{
			preprocessorBlock += "\n#define " + constant.Name + " " + constant.Value;
		}

		std::string fullHeader = ReadShaderFile(headerFile) + preprocessorBlock + '\n';

		auto compile = [&](const path& filename, GLenum shaderType) -> GLuint
		{
			GLuint shaderObj = glCreateShader(shaderType);
			assert(shaderObj != 0);

			if (filename.empty()) return -1;

			const path filepath = canonical(shaderDir / filename);
			auto sourceIt = shaderFileSources.find(filepath.generic_string());
			if (sourceIt == shaderFileSources.end()) {
				ReadShaderFile(filepath);
				sourceIt = shaderFileSources.find(filepath.generic_string());
			}

			std::string fullSource = fullHeader + sourceIt->second;
			const char* fullSourcePtr = fullSource.c_str();

			glShaderSource(shaderObj, 1, &fullSourcePtr, NULL);
			glCompileShader(shaderObj);

			GLint result;
			glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &result);
			if (!result)
			{
				GLchar compileOutput[1024];
				glGetShaderInfoLog(shaderObj, sizeof(compileOutput), NULL, compileOutput);

				std::cout << "Error compiling " << filename.generic_string()
						  << ": " << compileOutput << "\n\n" << fullSource;
				assert(false);
				SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error compiling shader", compileOutput, NULL);
				throw std::runtime_error("Shader failed to compile");
			}

			glAttachShader(glIndex, shaderObj);
			return shaderObj;
		};

		vertShaderID = compile(vertShaderFile, GL_VERTEX_SHADER);
		geomShaderID = compile(geomShaderFile, GL_GEOMETRY_SHADER);
		fragShaderID = compile(fragShaderFile, GL_FRAGMENT_SHADER);
		compShaderID = compile(compShaderFile, GL_COMPUTE_SHADER);

		GLint result;
		glLinkProgram(glIndex);
		glGetProgramiv(glIndex, GL_LINK_STATUS, &result);
		if (!result)
		{
			GLsizei length;
			GLchar infoLog[1000];
			glGetProgramInfoLog(glIndex, 1000, &length, infoLog);
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Shader linking error", infoLog, NULL);
		}

		if (vertShaderID != NULL_GLUINT) glDeleteShader(vertShaderID);
		if (geomShaderID != NULL_GLUINT) glDeleteShader(geomShaderID);
		if (fragShaderID != NULL_GLUINT) glDeleteShader(fragShaderID);
		if (compShaderID != NULL_GLUINT) glDeleteShader(compShaderID);
	}

	~Program()
	{
		if (glIndex != NULL_GLUINT)
		{
			glDeleteProgram(glIndex);
		}
	}

	Program(Program&& other) noexcept
	{
		glIndex = other.glIndex;
		other.glIndex = NULL_GLUINT;
	}

	Program& operator= (Program&& other) noexcept
	{
		glIndex = other.glIndex;
		other.glIndex = NULL_GLUINT;
		return *this;
	}

	Program(const Program& other) = delete;
	Program& operator= (const Program& other) = delete;
};

void BindTextures(const int bindingOffset, const std::initializer_list<const TextureBuffer*>& textures);
void BindTexturesByID(const std::unordered_map<int, std::unique_ptr<TextureBuffer>>& bufferedTextures, const int bindingOffset, const std::initializer_list<TextureID>& textureIDs);