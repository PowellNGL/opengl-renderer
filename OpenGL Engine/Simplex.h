#pragma once

#include <math.h>
#include "Vector.h"
#include <algorithm>
#include <random>

class Simplex
{
private:

	//The simplex grid is an infinite grid of equilateral triangles with i,j coordinates
	//The grid of squares that makes up regular cartesian 2D space is mapped to the simplex grid with the skewing functions below
	//Each square on the cartesian grid contains two skewed simplex triangles, separated by the square's diagonal

	static constexpr float skewFactor = 0.5f * (1.73205080757f - 1);				//0.5 * (sqrt(3) - 1)
	static constexpr float unskewFactor = ((1.0f / 1.73205080757f) - 1.0f) / 2.0f;	//((1 / sqrt(3)) - 1) / 2
	static constexpr float outputScaling = 0.5f / 0.0101419259f;	//Value estimated by generating loads of values and finding the highest/lowest outputs

	static constexpr int octaves = 10;

	std::array<Vector2, 8> gradients;
	std::array<int, 256> perm;

	//Convert x,y coordinates to i,j coordinates
	static inline Vector2 Skew(const Vector2& v)
	{
		float factor = (v.x + v.y) * skewFactor;
		return Vector2(v.x + factor, v.y + factor);
	}

	//Convert i,j coordinates to x,y coordinates
	static inline Vector2 Unskew(const Vector2& v)
	{
		float factor = (v.x + v.y) * unskewFactor;
		return Vector2(v.x + factor, v.y + factor);
	}

	static constexpr float fastFloor(const float x) 
	{
		return x > 0 ? static_cast<float>(static_cast<int>(x)) : static_cast<float>(static_cast<int>(x) - 1); 
	}

	inline float cornerContribution(const Vector2& xyDisplacement, const Vector2& ijTriangle, const int octaveOffset) const
	{
		float contribution = 0.5f - (xyDisplacement.x * xyDisplacement.x + xyDisplacement.y * xyDisplacement.y);
		if (contribution > 0)
		{
			//This could really be any hash function as long as it came up with pseudorandom but consistent gradients for each IJ point
			Vector2 gradient = gradients[perm[(static_cast<int>(ijTriangle.x) + octaveOffset + perm[(static_cast<int>(ijTriangle.y) + octaveOffset) & 255]) & 255] % 8];
			contribution *= contribution;	//^2
			contribution *= contribution;	//^4
			contribution *= Dot(gradient, xyDisplacement);
			return contribution;
		} else {
			return 0;
		}
	}

public:
	inline float Noise(const Vector2& xyInput, const int octaveOffset = 0) const
	{
		Vector2 xyTriangle0, xyTriangle1, xyTriangle2;
		Vector2 ijTriangle0, ijTriangle1, ijTriangle2;
		Vector2 xyDisplacements0, xyDisplacements1, xyDisplacements2;
		Vector2 ijCoords = Skew(xyInput);

		//First calculate triangle corner coordinates in xy and ij space

		//First corner
		ijTriangle0 = Vector2(fastFloor(ijCoords.x), fastFloor(ijCoords.y));
		xyTriangle0 = Unskew(ijTriangle0);
		xyDisplacements0 = xyInput - xyTriangle0;

		//Second corner
		//Looking along the diagonal dividing the cell in XY space, is the input point on the left or the right?
		if (xyDisplacements0.x < xyDisplacements0.y)
		{
			//Left
			ijTriangle1 = ijTriangle0 + Vector2(0, 1);
		} else {
			//Right
			ijTriangle1 = ijTriangle0 + Vector2(1, 0);
		}
		xyTriangle1 = Unskew(ijTriangle1);
		xyDisplacements1 = xyInput - xyTriangle1;

		//Third corner
		ijTriangle2 = ijTriangle0 + Vector2(1, 1);
		xyTriangle2 = Unskew(ijTriangle2);
		xyDisplacements2 = xyInput - xyTriangle2;

		//Finally, calculate each corner's contribution to the final value according to https://en.wikipedia.org/wiki/Simplex_noise#Kernel_summation
		float output = cornerContribution(xyDisplacements0, ijTriangle0, octaveOffset);
		output += cornerContribution(xyDisplacements1, ijTriangle1, octaveOffset);
		output += cornerContribution(xyDisplacements2, ijTriangle2, octaveOffset);

		return output * outputScaling * 2;
	}

	inline float FractalNoise(const Vector2& xyInput) const
	{
		float total = 0;
		float freq = 1;
		float ampl = 1;
		for (int octave = 0; octave < octaves; octave++)
		{
			total += Noise(xyInput * freq, octave) * ampl;
			freq *= 2;
			ampl *= 0.5f;
		}

		return total;
	}

	Simplex(const unsigned int seed)
	{
		//perm is used as a list of pseudorandom offsets into the gradient table, so it needs to be shuffled with the specified seed
		for (int i = 0; i < 256; i++)
		{
			perm[i] = i;
		}
		std::mt19937 random(seed);
		std::shuffle(perm.begin(), perm.end(), random);

		gradients =
		{
			Vector2(0.0f, -1.0f),
			Vector2(1.0f, 1.0f).Normalised(),
			Vector2(1.0f, 0.0f),
			Vector2(-1.0f, 0.0f),
			Vector2(0.0f, 1.0f),
			Vector2(-1.0f, -1.0f).Normalised(),
			Vector2(-1.0f, 1.0f).Normalised(),
			Vector2(1.0f, -1.0f).Normalised()
		};
	}
};