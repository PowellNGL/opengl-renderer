#pragma once

#include <cstdint>
#include <limits>

constexpr inline uint32_t NUM_CASCADES = 6;
constexpr inline float SHADOW_BIAS_MULTIPLIER = 4;
constexpr inline uint32_t BLOOM_OCTAVES = 4;

enum class Axis { X = 0, Y = 1, Z = 2 };

using ObjectID = uint32_t;
constexpr inline ObjectID InvalidObjectID = std::numeric_limits<ObjectID>::max();

using TextureID = uint32_t;
constexpr inline TextureID InvalidTextureID = std::numeric_limits<TextureID>::max();

using MaterialID = uint32_t;
constexpr inline MaterialID InvalidMaterialID = std::numeric_limits<MaterialID>::max();