#include "Mesh.h"
#include "Texture.h"

unsigned int Mesh::nextID = 0;

void Mesh::AppendVertex(const Vector3& position, const Vector3& normal, const Vector2& UV, const Vector3& Tangent, const Vector3& Binormal)
{
	Verts.push_back(position);
	Normals.push_back(normal);
	UVs.push_back(UV);
	Tangents.push_back(Tangent);
	Binormals.push_back(Binormal);
}

void Mesh::AppendTriangle(const uint32_t v0, const uint32_t v1, const uint32_t v2)
{
	Triangles.push_back(v0);
	Triangles.push_back(v1);
	Triangles.push_back(v2);
}

bool Mesh::IsValid() const
{
	return (
		UVs.size() == Verts.size() &&
		Normals.size() == Verts.size() &&
		Tangents.size() == Verts.size() &&
		Binormals.size() == Verts.size() &&
		Triangles.size() % 3 == 0);
}

void Mesh::CombineWith(const Mesh & otherMesh)
{
	size_t numVerts = Verts.size();

	Verts.insert(Verts.end(), otherMesh.Verts.begin(), otherMesh.Verts.end());
	Normals.insert(Normals.end(), otherMesh.Normals.begin(), otherMesh.Normals.end());
	UVs.insert(UVs.end(), otherMesh.UVs.begin(), otherMesh.UVs.end());
	Tangents.insert(Tangents.end(), otherMesh.Tangents.begin(), otherMesh.Tangents.end());
	Binormals.insert(Binormals.end(), otherMesh.Binormals.begin(), otherMesh.Binormals.end());

	for (size_t vertIndex = 0; vertIndex < otherMesh.Triangles.size(); vertIndex++)
	{
		Triangles.push_back(otherMesh.Triangles[vertIndex + numVerts]);
	}
}

void Mesh::CalcNormals()
{
	if (Normals.size() != Verts.size())
	{
		Normals.resize(Verts.size());
	}
	std::fill(Normals.begin(), Normals.end(), Vector3::Zero());

	for (size_t triangle = 0; triangle < Triangles.size() / 3; triangle++)
	{
		const Vector3& v1 = Verts[Triangles[triangle * 3 + 0]];
		const Vector3& v2 = Verts[Triangles[triangle * 3 + 1]];
		const Vector3& v3 = Verts[Triangles[triangle * 3 + 2]];
		Vector3 normal = Vector3::CalcNormal(v1, v2, v3);
		Normals[Triangles[triangle * 3 + 0]] = normal;
		Normals[Triangles[triangle * 3 + 1]] = normal;
		Normals[Triangles[triangle * 3 + 2]] = normal;
	}

	for (Vector3& normal : Normals)
	{
		normal.Normalise();
	}

	CalcTangents();
}

void Mesh::CalcTangents()
{
	if (Tangents.size() != Verts.size())
	{
		Tangents.resize(Verts.size());
	}
	if (Binormals.size() != Verts.size())
	{
		Binormals.resize(Verts.size());
	}
	if (UVs.size() != Verts.size())
	{
		UVs.resize(Verts.size());
	}
	std::fill(Tangents.begin(), Tangents.end(), Vector3::Zero());
	std::fill(Binormals.begin(), Binormals.end(), Vector3::Zero());

	for (size_t triangle = 0; triangle < Triangles.size() / 3; triangle++)
	{
		std::pair<Vector3, Vector3> TB = CalcTangent(triangle);

		for (size_t v = 0; v < 3; v++)
		{
			Tangents[triangle / 3 + v] += TB.first;
			Binormals[triangle / 3 + v] += TB.second;
		}
	}

	for (int i = 0; i < Verts.size(); i++)
	{
		Tangents[i].Normalise();
		Binormals[i].Normalise();
	}
}

void Mesh::Reverse()
{
	std::reverse(Triangles.begin(), Triangles.end());

	for (Vector3& normal : Normals)
	{
		normal = -normal;
	}

	CalcTangents();
}

std::pair<Vector3, Vector3> Mesh::CalcTangent(const size_t triangleID) const
{
	std::pair<Vector3, Vector3> out;

	//Adapted from http://stackoverflow.com/questions/5255806/how-to-calculate-tangent-and-binormal/5257471#5257471

	const Vector3& v0 = Verts[Triangles[triangleID * 3 + 0]];
	const Vector3& v1 = Verts[Triangles[triangleID * 3 + 1]];
	const Vector3& v2 = Verts[Triangles[triangleID * 3 + 2]];

	const Vector2& uv0 = UVs[Triangles[triangleID * 3 + 0]];
	const Vector2& uv1 = UVs[Triangles[triangleID * 3 + 1]];
	const Vector2& uv2 = UVs[Triangles[triangleID * 3 + 2]];

	Vector3 D = v1 - v0;
	Vector3 E = v2 - v0;

	Vector2 F = uv1 - uv0;
	Vector2 G = uv2 - uv0;

	// | T.x T.y T.z |           1         |  G.y  -F.y | | D.x D.y D.z |
	// |             | = ----------------- |            | |             |
	// | B.x B.y B.z |   F.x G.y - F.y G.x | -G.x   F.x | | E.x E.y E.z |

	float inverseDeterminant = 1 / ((F.x * G.y) - (F.y * G.x));

	out.first = Vector3(
		(inverseDeterminant * G.y * D.x) + (inverseDeterminant * -F.y * E.x),
		(inverseDeterminant * G.y * D.y) + (inverseDeterminant * -F.y * E.y),
		(inverseDeterminant * G.y * D.z) + (inverseDeterminant * -F.y * E.z)
	).Normalised();

	out.second = Vector3(
		(inverseDeterminant * -G.x * D.x) + (inverseDeterminant * F.x * E.x),
		(inverseDeterminant * -G.x * D.y) + (inverseDeterminant * F.x * E.y),
		(inverseDeterminant * -G.x * D.z) + (inverseDeterminant * F.x * E.z)
	).Normalised();

	return out;
}