#include "Quaternion.h"
#define _USE_MATH_DEFINES
#include <math.h>

const Quaternion Quaternion::Identity = { 0.0f, 0.0f, 0.0f, 1.0f };

Quaternion::Quaternion(const Vector3& axis, const float rotation)
{
	x = axis.x * sinf(rotation / 2.0f);
	y = axis.y * sinf(rotation / 2.0f);
	z = axis.z * sinf(rotation / 2.0f);
	w = cosf(rotation / 2.0f);
	Normalise();
}

Quaternion::Quaternion(const Vector3& eulerAngles)
{
	*this = Quaternion(Vector3::Forward(), eulerAngles.z) * Quaternion(Vector3::Up(), eulerAngles.y) * Quaternion(Vector3::Left(), eulerAngles.x);
}

//Return a quaternion that rotates from origin to dest
Quaternion::Quaternion(const Vector3& origin, const Vector3& dest)
{
	Vector3 v1 = origin.Normalised();
	Vector3 v2 = dest.Normalised();
	Vector3 cross = Cross(v1, v2);
	if (cross.Magnitude() < 0.00001f)
	{
		cross = origin.GetOrthogonal();
	}
	x = cross.x;
	y = cross.y;
	z = cross.z;
	w = 1 + Dot(v1, v2);
	Normalise();
}

Quaternion Quaternion::Normalised() const
{
	float square = x * x + y * y + z * z + w * w;
	float coefficient = 1;
	if (square != 1)
	{
		coefficient /= sqrtf(square);
	}
	return Quaternion(
		x * coefficient,
		y * coefficient,
		z * coefficient,
		w * coefficient
	);
}

void Quaternion::Normalise()
{
	float square = x * x + y * y + z * z + w * w;
	float coefficient = 1;
	if (square != 1)
	{
		coefficient /= sqrtf(square);
	}
	x *= coefficient;
	y *= coefficient;
	z *= coefficient;
	w *= coefficient;
}

Quaternion Quaternion::Conjugation() const
{
	return Quaternion{ -x, -y, -z, w };
}

Vector3 Quaternion::Axis() const
{
	if (x + y + z == 0)
	{
		return Vector3::Zero();
	}
	else 
	{
		return Vector3(x, y, z).Normalised();
	}
}

float Quaternion::Angle() const
{
	return 2 * acos(w);
}

Vector3 Quaternion::ToEulerAngles() const
{
	return Vector3(
		atan2(2 * (w * x + y * z), 1 - 2 * (x * x + y * y)),
		asin(2 * (w * y - z * x)),
		atan2(2 * (w * z + x * y), 1 - 2 * (y * y + z * z))
	);
}

Quaternion Quaternion::Reversed() const
{
	return{ x, y, z, -w };
}

Quaternion Quaternion::operator+(const Quaternion & qB) const
{
	return Quaternion(
		x + qB.x,
		y + qB.y,
		z + qB.z,
		w + qB.w
	);
}

void Quaternion::operator+=(const Quaternion & qB)
{
	x += qB.x;
	y += qB.y;
	z += qB.z;
	w += qB.w;

}

Quaternion Quaternion::operator-(const Quaternion & qB) const
{
	return Quaternion(
		x - qB.x,
		y - qB.y,
		z - qB.z,
		w - qB.w
	);
}

void Quaternion::operator-=(const Quaternion & qB)
{
	x += qB.x;
	y += qB.y;
	z += qB.z;
	w += qB.w;

}

//https://en.wikipedia.org/wiki/Quaternion#Hamilton_product
Quaternion Quaternion::operator*(const Quaternion& qB) const
{
	return Quaternion(
		w*qB.x + x*qB.w + y*qB.z - z*qB.y,
		w*qB.y - x*qB.z + y*qB.w + z*qB.x,
		w*qB.z + x*qB.y - y*qB.x + z*qB.w,
		w*qB.w - x*qB.x - y*qB.y - z*qB.z
	);
}

void Quaternion::operator*= (const Quaternion & qB)
{
	*this = *this * qB;
}

Vector3 Quaternion::operator*(const Vector3 & v) const
{
	if (v.Magnitude() == 0)
	{
		return v;
	}
	Quaternion qv = Quaternion(v.x, v.y, v.z, 0);
	qv = *this * qv * Conjugation();
	return Vector3(qv.x, qv.y, qv.z);
}

Vector3 operator*(const Vector3 & v, const Quaternion & q)
{
	return q * v;
}