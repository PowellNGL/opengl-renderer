#pragma once

#include <algorithm>
#include <vector>
#include <unordered_map>

#include "Scene.h"
#include "Mesh.h"
#include "Simplex.h"
#include "Vector.h"
#include "RenderData.h"

class Terrain;
class TerrainChunk;
struct Mesh;

struct ChunkNeighbourInformation
{
	//<ChunkID, chunk resolution>
	//Order of elements is always North, East, South, West
	std::array<std::pair<int, int>, 4> adjacentChunks;

	ChunkNeighbourInformation();
	ChunkNeighbourInformation(const TerrainChunk& chunk);
	bool operator== (const ChunkNeighbourInformation& other);
};

class TerrainChunk
{
	friend class Terrain;
	friend struct ChunkNeighbourInformation;

private:
	size_t chunkID;
	Object* object;
	std::vector<std::vector<float>> heightmap;	//Height of each vertex
	Terrain* parent = nullptr;
	uint32_t resolution;	//Number of quads along each edge, not number of verts
	Vector2i chunkPos;
	bool inPosition = false;
	ChunkNeighbourInformation neighbours;
	Mesh mesh;
	Model model;
	
	void UpdateMesh();
	void RemakeHeightmap();
	void SetChunkPosition(const Vector2i& chunkPosition);
	bool MatchHeightmapEdges(const ChunkNeighbourInformation& previousNeighbours, const bool thisChunkHasChanged);
	void CalcNormalsFromHeightmap();
	float SampleHeightmapFiltered(const Vector2& coords) const;

	static bool CompareDistance(const TerrainChunk* chunkA, const TerrainChunk* chunkB);

public:
	TerrainChunk(Scene& scene, const size_t ID, Terrain& parent, const int resolution);

	TerrainChunk(const TerrainChunk& other) = delete;
	TerrainChunk& operator= (const TerrainChunk& other) = delete;
	TerrainChunk(TerrainChunk&& other) = default;
	TerrainChunk& operator= (TerrainChunk&& other) = default;
};

class Terrain
{
	friend class TerrainChunk;
	friend struct ChunkNeighbourInformation;

public:
#ifdef _DEBUG
	static inline constexpr int maxChunkDistance = 1;
#else
	static inline constexpr int maxChunkDistance = 15;
#endif
	static inline constexpr size_t numChunks = (maxChunkDistance * 2 + 1) * (maxChunkDistance * 2 + 1);
	static inline constexpr float chunkSize = 1000;
	static inline constexpr uint32_t nearestChunkResolution = 256;
	static inline constexpr float simplexScale = 1 / 15000.0f;
	static inline constexpr float terrainHeight = 1000;

private:
	Simplex simplex;
	Scene* const scene;

	Vector2i centreChunk = { std::numeric_limits<int>::max(), std::numeric_limits<int>::min() };	//Centre in chunk coordinates. Start at int limits so that it's always updated immediately
	std::vector<Vector2i> unfilledChunkPositions;	//Gaps in the grid that needs a chunk put into them
	std::vector<TerrainChunk*> uncheckedChunks;		//Chunks that haven't been re-checked since the terrain centre last moved

	//Chunks in no particular order. Each chunk's ID refers to the offset into this vector
	std::vector<TerrainChunk> chunks;

	// Don't set this directly, use SetChunkPosition.
	// Contents: map<chunkX, map<chunkY, ID>>
	std::unordered_map<int, std::unordered_map<int, size_t>> chunkPositions;
	
	//Index of the chunk in the chunks vector, or -1 if the coordinates are out of bounds
	inline size_t IDOfChunkAtPosition(const int x, const int y)
	{
		return (chunkPositions.find(x) != chunkPositions.end() && chunkPositions.at(x).find(y) != chunkPositions.at(x).end())
			? chunkPositions[x][y]
			: -1;
	}
	inline size_t IDOfChunkAtPosition(const Vector2i& pos) { return IDOfChunkAtPosition(pos.x, pos.y); }
	inline int targetChunkResolution(const Vector2i& position) const
	{
		int distanceFromCentre = std::max(std::abs(position.x - centreChunk.x), std::abs(position.y - centreChunk.y));
		int tier = std::max<int>(0, static_cast<int>(std::ceil(std::log2f(distanceFromCentre + 1))));
		return std::max<int>(0, static_cast<int>(std::ceil(nearestChunkResolution / powf(2, tier))));
	}

	float SampleHeightmapsAtPosition(const Vector2& chunkSpacePoint);
	void MakeListsOfChunksToCheck();
	
public:
	Material* material;

	Terrain(Scene* scene, const unsigned int seed, MaterialStorage& materials);

	//Move chunks, change their meshes etc. Return a list of changed chunks
	std::vector<Object*> UpdateChunks(const Vector2& centre, const unsigned int maxChunkUpdates, const bool debugVisualiseChanges = false);
	float GetSimplexHeight(const Vector2& position);

	std::vector<Object*> GetChunkObjects() const
	{
		std::vector<Object*> out;
		for (const auto& chunk : chunks) {
			out.push_back(chunk.object);
		}
		return out;
	}
};