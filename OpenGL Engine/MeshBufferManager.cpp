#include <cassert>
#include <unordered_set>

#include "MeshBufferManager.h"
#include "Mesh.h"

//Check if a mesh has changed since being buffered and now needs more space in either its vertex or element buffer
bool MeshBufferManager::MeshNeedsLargerBufferRegion(const Mesh& mesh) const
{
	auto it = BufferedMeshes.find(mesh.ID());

	if (it == BufferedMeshes.end())
		return true;	//Return true if the mesh isn't buffered, as it's technically true

	return (mesh.Verts.size() > it->second.VertsLength ||
			mesh.Triangles.size() > it->second.IndicesLength);
}

void MeshBufferManager::BufferMeshesIntoSingleBuffer(const std::vector<Mesh*>& Meshes)
{
	size_t totalVerts = 0;
	size_t totalIndices = 0;
	for (const Mesh* mesh : Meshes)
	{
		totalVerts += mesh->Verts.size();
		totalIndices += mesh->Triangles.size();
	}

	VertexBuffers& buffer = *bufferWrappers.emplace_back(new VertexBuffers(VAO, totalVerts, totalIndices));

	std::unordered_set<unsigned int> processedMeshes;
	size_t vertsOffset = 0;
	size_t indicesOffset = 0;
	for (const Mesh* mesh : Meshes)
	{
		if (mesh->Verts.size() == 0 || mesh->Triangles.size() == 0) {
			continue;
		}

		//Only add a mesh if it's not already been buffered in this call
		if (processedMeshes.find(mesh->ID()) != processedMeshes.end()) {
			continue;
		} else {
			processedMeshes.insert(mesh->ID());
		}

		//If it's already buffered from a previous call, though, assume the calling function wants to replace it so remove it before re-adding it
		if (BufferedMeshes.find(mesh->ID()) != BufferedMeshes.end())
		{
			UnbufferMesh(*mesh);
		}

		BufferedMeshes.insert(std::pair<unsigned int, MeshBufferInfo>(mesh->ID(), MeshBufferInfo(*mesh, buffer, vertsOffset, mesh->Verts.size(), indicesOffset, mesh->Triangles.size())));

		auto bufferData = [&]<typename T>(VertexBuffers::Type type, size_t offset, const std::vector<T>& source) {
			glNamedBufferSubData(buffer.BufferIndices[(size_t)type], offset * sizeof(T), source.size() * sizeof(T), (void*)source.data());
		};

		bufferData(VertexBuffers::Type::Position, vertsOffset, mesh->Verts);
		bufferData(VertexBuffers::Type::Normal, vertsOffset, mesh->Normals);
		bufferData(VertexBuffers::Type::UV, vertsOffset, mesh->UVs);
		bufferData(VertexBuffers::Type::Binormal, vertsOffset, mesh->Binormals);
		bufferData(VertexBuffers::Type::Tangent, vertsOffset, mesh->Tangents);
		bufferData(VertexBuffers::Type::Index, indicesOffset, mesh->Triangles);

		vertsOffset += mesh->Verts.size();
		indicesOffset += mesh->Triangles.size();
	}
}

void MeshBufferManager::BufferMeshes(const std::vector<Mesh*>& Meshes)
{
	std::unordered_set<unsigned int> processedMeshes;
	for (const Mesh* mesh : Meshes)
	{
		//Only add a mesh if it's not already been buffered in this call
		if (processedMeshes.find(mesh->ID()) != processedMeshes.end())
		{
			continue;
		}
		else {
			processedMeshes.insert(mesh->ID());
		}

		BufferMesh(*mesh);
	}
}

void MeshBufferManager::BufferMesh(const Mesh& mesh)
{
	if (BufferedMeshes.find(mesh.ID()) != BufferedMeshes.end())
	{
		UnbufferMesh(mesh);
	}

	VertexBuffers& buffer = *bufferWrappers.emplace_back(new VertexBuffers(VAO, mesh.Verts.size(), mesh.Triangles.size()));

	BufferedMeshes.emplace(mesh.ID(), MeshBufferInfo(mesh, buffer, 0, mesh.Verts.size(), 0, mesh.Triangles.size()));

	auto bufferData = [&]<typename T>(VertexBuffers::Type type, const std::vector<T>&source) {
		glNamedBufferSubData(buffer.BufferIndices[(size_t)type], 0, source.size() * sizeof(T), (void*)source.data());
	};

	bufferData(VertexBuffers::Type::Position, mesh.Verts);
	bufferData(VertexBuffers::Type::Normal, mesh.Normals);
	bufferData(VertexBuffers::Type::UV, mesh.UVs);
	bufferData(VertexBuffers::Type::Binormal, mesh.Binormals);
	bufferData(VertexBuffers::Type::Tangent, mesh.Tangents);
	bufferData(VertexBuffers::Type::Index, mesh.Triangles);
}

void MeshBufferManager::UnbufferMesh(const Mesh& mesh)
{
	auto it = BufferedMeshes.find(mesh.ID());
	if (it == BufferedMeshes.end())
	{
		return;
	}

	//Remove the record for this mesh from the list of buffered meshes.
	//This will, of course, leave a gap in the vertex buffer if it isn't then destroyed
	VertexBuffers* buffers = &it->second.buffers;
	BufferedMeshes.erase(mesh.ID());

	//Clear up the buffer if this mesh was the only one in it
	for (const auto& [meshID, mbi] : BufferedMeshes)
	{
		if (&mbi.buffers == buffers)
		{
			//The vertex buffer has a mesh in it, so leave it alone
			return;
		}
	}

	for (auto it = bufferWrappers.begin(); it != bufferWrappers.end(); it++)
	{
		if (it->get()->BufferIndices[0] == buffers->BufferIndices[0])
		{
			bufferWrappers.erase(it);
			return;
		}
	}
}

void MeshBufferManager::UpdateMesh(const Mesh& mesh)
{	
	//Move the mesh if it no longer fits in its allocated space (leaving a gap behind)
	if (MeshNeedsLargerBufferRegion(mesh))
	{
		UnbufferMesh(mesh);
		BufferMesh(mesh);
	} else {
		//Buffer the verts to the same place they were before. If the mesh is smaller than before, there will be a gap after it in the buffer
		MeshBufferInfo& mbi = BufferedMeshes.at(mesh.ID());
		VertexBuffers* buffers = nullptr;
		for (auto& buffersPtr : bufferWrappers) {
			if (buffersPtr.get() == &mbi.buffers) {
				buffers = &mbi.buffers;
			}
		}
		
		if (buffers == nullptr) {
			return;
		}

		auto bufferData = [&]<typename T>(VertexBuffers::Type type, size_t offset, const std::vector<T>&source) {
			glNamedBufferSubData(buffers->BufferIndices[(size_t)type], offset * sizeof(T), source.size() * sizeof(T), (void*)source.data());
		};

		bufferData(VertexBuffers::Type::Position, mbi.VertsOffset, mesh.Verts);
		bufferData(VertexBuffers::Type::Normal, mbi.VertsOffset, mesh.Normals);
		bufferData(VertexBuffers::Type::UV, mbi.VertsOffset, mesh.UVs);
		bufferData(VertexBuffers::Type::Binormal, mbi.VertsOffset, mesh.Binormals);
		bufferData(VertexBuffers::Type::Tangent, mbi.VertsOffset, mesh.Tangents);
		bufferData(VertexBuffers::Type::Index, mbi.IndicesOffset, mesh.Triangles);

		mbi.IndicesLength = mesh.Triangles.size();
		mbi.VertsLength = mesh.Verts.size();
	}
}
