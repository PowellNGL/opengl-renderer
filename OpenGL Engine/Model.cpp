#include "Model.h"
#include "Bounds.h"
#include "Mesh.h"

unsigned int Model::nextID = 0;

void Model::RecalcAABB()
{
	if (!HasVerts())
	{
		AABB = Bounds(Vector3::Zero(), Vector3::Zero());
		return;
	}

	AABB.v1 = Meshes.front().mesh->Verts.front();
	AABB.v2 = AABB.v1;

	for (const auto& meshInstance : Meshes)
	{
		for (const Vector3& vert : meshInstance.mesh->Verts)
		{
			AABB.v1 = Min(AABB.v1, vert);
			AABB.v2 = Max(AABB.v2, vert);
		}
	}
}

bool Model::HasMeshes() const
{
	return !Meshes.empty();
}

bool Model::HasVerts() const
{
	for (const MeshInstance& meshInstance : Meshes)
	{
		if (!meshInstance.mesh->Verts.empty())
		{
			return true;
		}
	}
	return false;
}
