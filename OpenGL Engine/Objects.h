#pragma once

#include "Scene.h"
#include "Frustum.h"
#include "RenderData.h"

class PointLight
{
public:
	static inline constexpr float LIGHT_CUTOFF_MULTIPLIER = 100.0f;

	Object* object;
	std::unique_ptr<Model> model;
	Colour colour = { 1, 1, 1, 1 };
	float intensity = 1;

	PointLight(Scene& scene, MaterialStorage& materials, const std::string& name, const Colour& colour, const float intensity, Mesh& mesh)
		: object(scene.CreateObject(name)), model(std::make_unique<Model>(name)), colour(colour), intensity(intensity)
	{
		Material* material = materials.New();
		material->Albedo = { 0, 0, 0, 1 };
		material->Roughness = 1.0f;
		material->Metallic = 1.0f;
		material->Emissivity = colour * intensity * 10;

		model->Meshes.push_back(MeshInstance(model.get(), &mesh, material));
		model->Meshes.back().isShadowCaster = false;
		model->RecalcAABB();
		object->model = model.get();
	}

	float LightRadius() const
	{
		return intensity * LIGHT_CUTOFF_MULTIPLIER;
	}

	Bounds GetLightAreaAABB() const
	{
		Bounds bounds;
		float radius = LightRadius();
		bounds.v1 = Vector3(-1, -1, -1) * radius + object->Position;
		bounds.v2 = Vector3(1, 1, 1) * radius + object->Position;
		return bounds;
	}
};

class DirectionalLight
{
public:
	Object* object;
	std::unique_ptr<Model> model;
	Colour colour = { 1, 1, 1, 1 };
	float intensity = 1;

	DirectionalLight(Scene& scene, MaterialStorage& materials, const std::string& name, const Colour& colour, const float intensity, const Vector3& direction, Mesh& mesh)
		: object(scene.CreateObject(name)), model(std::make_unique<Model>(name)), colour(colour), intensity(intensity)
	{
		object->Rotation = Quaternion(-Vector3::Forward(), direction);

		Material* material = materials.New();
		material->Albedo = { 0, 0, 0, 1 };
		material->Roughness = 1.0f;
		material->Metallic = 1.0f;
		material->Emissivity = colour * intensity;

		model->Meshes.push_back(MeshInstance(model.get(), &mesh, material));
		model->Meshes.back().isShadowCaster = false;
		object->model = model.get();
	}
};

class Camera : public Object
{
public:
	float FovY = pi / 2.0f;
	float FovX = pi / 2.0f;
	float NearClip = 0.1f;
	float FarClip = 50.0f;

	Camera(const std::string& name) : Object(name) {};

	Frustum MakeFrustum() const
	{
		return Frustum(MakeProjectionMatrix() * MakeViewMatrix());
	}

	Matrix4 MakeProjectionMatrix() const
	{
		return Matrix4::MakePerspectiveProjection(NearClip, FarClip, FovX, FovY);
	}

	Matrix4 MakeViewMatrix() const
	{
		return Matrix4::FromQuaternion(Rotation.Reversed()) * Matrix4::MakeTranslation(-Position);
	}
};