#pragma once

#include <vector>
#include <algorithm>
#include <numeric>
#include <memory>

#include "Bounds.h"
#include "TypedArena.h"

template<typename T>
struct BVH
{
private:
	struct Node;

public:
	using ArenaT = TypedArena<Node>;

	struct NodeSetupInfo
	{
		T value;
		Bounds bounds;

		NodeSetupInfo(const T& value, const Bounds& bounds) : value(value), bounds(bounds) {}
		NodeSetupInfo(const NodeSetupInfo&) = default;
		NodeSetupInfo(NodeSetupInfo&&) = default;
		NodeSetupInfo& operator=(const NodeSetupInfo&) = default;
		NodeSetupInfo& operator=(NodeSetupInfo&&) = default;
	};

private:
	struct Node
	{
		using ChildrenT = std::pair<Node*, Node*>;

		Bounds bounds{};
		union Contents {
			T value;
			ChildrenT children;
			Contents() : children() {}
			~Contents() {}
		} contents{};
		bool is_leaf{};

		const T& value() const { return contents.value; }
		T& value() { return contents.value; }
		const ChildrenT& children() const { return contents.children; }
		ChildrenT& children() { return contents.children; }

		Node() = default;
		Node(const Node&) = default;
		Node(Node&&) = default;
		Node& operator=(const Node&) = default;
		Node& operator=(Node&&) = default;

		// Interior node constructor
		Node(Node* node1, Node* node2)
			: is_leaf(false), bounds(node1->bounds.Union(node2->bounds))
		{
			contents.children = std::make_pair(node1, node2);
		}

		// Leaf node constructor
		Node(const Bounds& bounds, const T& value)
			: is_leaf(true), bounds(bounds)
		{
			contents.value = value;
		}

		size_t RecursiveCountDepth(ArenaT& arena) const
		{
			if (is_leaf) {
				return 1;
			} else {
				return std::max(
					children().first->RecursiveCountDepth(arena),
					children().second->RecursiveCountDepth(arena)
				) + 1;
			}
		}

		const Node* RecursiveFindLowestContainingNode(ArenaT& arena, const BoundsTestable& collider) const
		{
			if (!collider.ContainedBy(bounds)) return nullptr;

			if (is_leaf) return this;

			if (auto firstChildVal = children().first->RecursiveFindLowestContainingNode(arena, collider); firstChildVal) {
				return firstChildVal;
			}
			if (auto secondChildVal = children().second->RecursiveFindLowestContainingNode(arena, collider); secondChildVal) {
				return secondChildVal;
			}

			// If this node contains the collider but neither of the children do, this is the node we're looking for
			return this;
		}

		void RecursiveFindIntersectingNodes(ArenaT& arena, std::vector<Node*>& out, const BoundsTestable& collider)
		{
			if (collider.Contains(bounds))
			{
				//If this node is completely inside the bounds, so must all its children be
				out.push_back(this);
			}
			else if (collider.Intersects(bounds))
			{
				//If the node intersects the bounds but isn't contained by them, there may be some sub-nodes that are outside it, so keep checking deeper into the tree
				if (is_leaf) {
					out.push_back(this);
				} else {
					children().first->RecursiveFindIntersectingNodes(arena, out, collider);
					children().second->RecursiveFindIntersectingNodes(arena, out, collider);
				}
			}
			//else the node is outside the bounds and can be ignored from now on
		}

		void RecursiveGetValues(ArenaT& arena, std::vector<T>& out) const
		{
			if (is_leaf) {
				out.push_back(value());
			} else {
				children().first->RecursiveGetValues(arena, out);
				children().second->RecursiveGetValues(arena, out);
			}
		}

		void RecursiveGetAllIntersectingValues(ArenaT& arena, std::vector<T>& out, const BoundsTestable& collider) const
		{
			if (collider.Contains(bounds))
			{
				//If this node is completely inside the bounds, so must all its children be
				RecursiveGetValues(arena, out);
			}
			else if (collider.Intersects(bounds))
			{
				//If the node intersects the bounds but isn't contained by them, there may be some sub-nodes that are outside it, so keep checking deeper into the tree
				if (is_leaf) {
					out.push_back(value());
				} else {
					children().first->RecursiveGetAllIntersectingValues(arena, out, collider);
					children().second->RecursiveGetAllIntersectingValues(arena, out, collider);
				}
			}
		}

		template<typename F>
		bool RecursiveUpdateBounds(ArenaT& arena, F& boundsFunc)
		{
			if (is_leaf) {
				const Bounds& newBounds = boundsFunc(value());
				if (newBounds != bounds)
				{
					//This object's bounds have changed, so update this node and propagate the change up the tree by returning true
					bounds = newBounds;
					return true;
				}
			} else {
				Node* firstChild = children().first;
				Node* secondChild = children().second;
				bool changeMade = firstChild->RecursiveUpdateBounds(arena, boundsFunc);
				changeMade |= secondChild->RecursiveUpdateBounds(arena, boundsFunc);
				if (changeMade)
				{
					//Sub-nodes were updated, so update this bounds of the current node and return true
					bounds = firstChild->bounds.Union(secondChild->bounds);
					return true;
				}
			}

			//No change was made to any object in or below this node
			return false;
		}

		void recursiveGetNodeAtDepth(ArenaT& arena, size_t remainingDepth, std::vector<Node*>& out)
		{
			if (remainingDepth == 0) {
				out.push_back(this);
				return;
			}

			if (!is_leaf) {
				children().first->recursiveGetNodeAtDepth(arena, remainingDepth - 1, out);
				children().second->recursiveGetNodeAtDepth(arena, remainingDepth - 1, out);
			}
		}
	};

	using InputT = std::vector<NodeSetupInfo>;
	using InputItT = typename InputT::iterator;

	static InputItT splitInputList(const InputItT& start, const InputItT& end)
	{
		Bounds bounds = std::accumulate(start, end, Bounds(), [](const Bounds& bounds, const NodeSetupInfo& info) {
			return bounds.Union(info.bounds);
		});
		Axis splitAxis = bounds.LongestAxis();
		float midpoint = bounds.Midpoint(splitAxis);
		size_t count = std::distance(start, end);

		auto mid = start + count / 2;
		std::nth_element(start, mid, end, [splitAxis](const NodeSetupInfo& a, const NodeSetupInfo& b) {
			return a.bounds.MaxExtent(splitAxis) < b.bounds.MaxExtent(splitAxis);
		});

		//auto mid = std::partition(start, end, [midpoint, splitAxis](const NodeSetupInfo& info) {
		//	return info.bounds.Midpoint(splitAxis) < midpoint;
		//});

		return mid;
	}

	static Node* recursiveBuildNode(ArenaT& arena, const InputItT& start, const InputItT& end)
	{
		if (std::distance(start, end) == 1) {
			// If there's only one object left in this branch just make a leaf and return it
			return arena.New(start->bounds, start->value);
		} else {
			// Else split the objects into two along the longest axis of the current bounds and go deeper
			InputItT mid = splitInputList(start, end);
			Node* node1 = recursiveBuildNode(arena, start, mid);
			Node* node2 = recursiveBuildNode(arena, mid, end);
			return arena.New(node1, node2);
		}
	}

	static void recursiveDestroyNode(ArenaT& arena, Node* node)
	{
		if (!node->is_leaf) {
			recursiveDestroyNode(arena, node->children().first);
			recursiveDestroyNode(arena, node->children().second);
		}
		arena.Delete(node);
	}

	bool initialised() const { return static_cast<bool>(arena); }

	std::unique_ptr<ArenaT> arena{};
	Node* root{};

public:
	friend struct Node;

	BVH() {}
	BVH(InputT& values) {
		arena = std::make_unique<ArenaT>();
		root = recursiveBuildNode(*arena, values.begin(), values.end());
	}
	
	~BVH() {
		if (initialised()) {
			recursiveDestroyNode(*arena, root);
		}
	}
	BVH(const BVH& other) = delete;
	BVH(BVH&& other) noexcept
	{
		root = other.root;
		arena = std::move(other.arena);
	};
	BVH& operator= (const BVH& other) = delete;
	BVH& operator= (BVH&& other) noexcept
	{
		if (this == &other) return *this;

		root = other.root;
		arena = std::move(other.arena);
		return *this;
	};

	void Rebuild(InputT& values) 
	{
		if (initialised()) {
			recursiveDestroyNode(*arena, root);
		} else {
			arena = std::make_unique<ArenaT>();
		}
		root = recursiveBuildNode(*arena, values.begin(), values.end());
	}

	size_t MaxDepth() const {
		if (!initialised()) {
			return 0;
		} else {
			return root->RecursiveCountDepth(*arena);
		}
	}
	std::vector<T> GetAllIntersectingValues(const BoundsTestable& collider) const
	{
		std::vector<T> out;
		if (initialised()) {
			root->RecursiveGetAllIntersectingValues(*arena, out, collider);
		}
		return out;
	}

	template<typename F>
	void UpdateNodeBounds(const F& boundsFunc) {
		if (initialised()) {
			root->RecursiveUpdateBounds(*arena, boundsFunc);
		}
	}

	std::vector<std::pair<bool, const Bounds*>> GetAllBoundsAtDepth(size_t depth) const
	{
		std::vector<std::pair<bool, const Bounds*>> out;
		if (initialised()) {
			std::vector<Node*> nodesAtDepth;
			root->recursiveGetNodeAtDepth(*arena, depth, nodesAtDepth);

			for (const Node* node : nodesAtDepth) {
				out.emplace_back(node->is_leaf, &node->bounds);
			}
		}
		return out;
	}
};