#include <queue>
#include <unordered_set>
#include <stdio.h>
#include <cassert>
#include <filesystem>

#include "Renderer.h"
#include "Matrix.h"
#include "Bounds.h"
#include "MeshBufferManager.h"
#include "Texture.h"
#include "Uniforms.h"
#include "RenderStages.h"
#include "Scene.h"
#include "BVH.h"

void GLAPIENTRY DebugMessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	char outMessage[1000];
	std::sprintf(outMessage, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
		type,
		severity,
		message);

	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", outMessage, NULL);
}

void Renderer::InitOpenGL()
{
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error starting GLEW", (const char*)glewGetErrorString(glewError), NULL);
	}
	
#ifdef _DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, false);
	glDebugMessageCallback(DebugMessageCallback, 0);
#endif

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);

	meshBufferManager = std::make_unique<MeshBufferManager>();
	InitShaders();
	BuildBuffers();
}

void Renderer::InitShaders()
{
	uniformBlocks = std::make_unique<Uniforms>();
	renderStages = std::make_unique<RenderStages>(*this, *uniformBlocks);
}

void Renderer::BuildBuffers()
{
	TextureBufferOptions options;

	//First Pass FBO

	GBufferFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, GBufferFBO->GLIndex());
	
	GBuffers[0] = std::make_unique<TextureBuffer>(InternalResolution, GL_RGB8, nullptr);			//Albedo
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, GBuffers[0]->GLIndex(), 0);
	GBuffers[1] = std::make_unique<TextureBuffer>(InternalResolution, GL_RG8, nullptr);			//Roughness/Metallic
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, GBuffers[1]->GLIndex(), 0);
	GBuffers[2] = std::make_unique<TextureBuffer>(InternalResolution, GL_RGB16_SNORM, nullptr);	//Normals
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, GBuffers[2]->GLIndex(), 0);
	CameraDepthBuffer = std::make_unique<TextureBuffer>(InternalResolution, GL_DEPTH_COMPONENT24, nullptr);	//Depth
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, CameraDepthBuffer->GLIndex(), 0);
	
	options = TextureBufferOptions();
	options.Type = TextureBufferType::Flat;
	options.UseLinearFilter = true;
	InternalColourBuffer = std::make_unique<TextureBuffer>(InternalResolution, GL_RGB16F, nullptr, options);	//Internal HDR colour buffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, InternalColourBuffer->GLIndex(), 0);

	GLenum drawBuffers[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
	glDrawBuffers(4, drawBuffers);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	DebugFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, DebugFBO->GLIndex());
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, InternalColourBuffer->GLIndex(), 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, CameraDepthBuffer->GLIndex(), 0);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	//Lighting pass FBO

	LightingPassFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, LightingPassFBO->GLIndex());
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, InternalColourBuffer->GLIndex(), 0);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	//Point light depth FBO

	PointLightDepthFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, PointLightDepthFBO->GLIndex());
	options = TextureBufferOptions();
	options.Type = TextureBufferType::Cubemap;
	options.UseShadowTest = true;
	options.UseLinearFilter = true;
	PointLightDepthBuffer = std::make_unique<TextureBuffer>(PointLightShadowResolution, GL_DEPTH_COMPONENT16, nullptr, options);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, PointLightDepthBuffer->GLIndex(), 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
	
	//Directional light depth FBO

	DirectionalLightDepthFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, DirectionalLightDepthFBO->GLIndex());
	options.Type = TextureBufferType::Array;
	options.NumLayers = NUM_CASCADES;
	options.EdgeMode = TextureEdgeMode::ClampToBorder;
	DirectionalLightShadowCascades = std::make_unique<TextureBuffer>(DirectionalLightShadowResolution, GL_DEPTH_COMPONENT24, nullptr, options);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, DirectionalLightShadowCascades->GLIndex(), 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	//Bloom

	BloomConvolutionFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, BloomConvolutionFBO->GLIndex());
	options = TextureBufferOptions();
	options.UseLinearFilter = true;
	options.UseMipMap = true;
	BloomDownsizeTextureBuffer = std::make_unique<TextureBuffer>(InternalResolution / 2.0f, GL_RGB16F, nullptr, options);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, BloomDownsizeTextureBuffer->GLIndex(), 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	//Gaussian Blur

	GaussianBlurFBO = std::make_unique<Framebuffer>();
	glBindFramebuffer(GL_FRAMEBUFFER, GaussianBlurFBO->GLIndex());
	options = TextureBufferOptions();
	options.UseLinearFilter = true;
	BloomBlurTextureBufferX = std::make_unique<TextureBuffer>(InternalResolution / powf(2.0f, BLOOM_OCTAVES), GL_RGB16F, nullptr, options);
	BloomBlurTextureBufferY = std::make_unique<TextureBuffer>(InternalResolution / powf(2.0f, BLOOM_OCTAVES), GL_RGB16F, nullptr, options);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, BloomBlurTextureBufferX->GLIndex(), 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}

void Renderer::BufferTextures()
{
	const GLint formatLookup[4] = { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 };
	TextureBufferOptions options;
	options.EdgeMode = TextureEdgeMode::Repeat;
	options.UseAnisotropicFiltering = true;
	options.UseLinearFilter = true;
	options.UseMipMap = true;
	for (const Texture& texture : render_data.Textures)
	{
		if (texture._pixels.size() == 0) continue;

		BufferedTextures[render_data.Textures.ToIndex(&texture)] =
			std::make_unique<TextureBuffer>(
				Vector2i(texture._width, texture._height),
				formatLookup[texture._channels - 1],
				std::vector<const Texture*>({&texture}),
				options
			);
	}
}

void Renderer::BufferSkybox(const std::vector<const Texture*>& textures)
{
	const GLint formatLookup[4] = { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 };
	TextureBufferOptions options;
	options.Type = TextureBufferType::Cubemap;
	options.UseLinearFilter = true;
	SkyboxBuffer = std::make_unique<TextureBuffer>(
		Vector2i(textures[0]->_width, textures[0]->_height), 
		formatLookup[textures[0]->_channels - 1],
		textures,
		options
	);
}

//TODO: Group meshes by material so the material uniforms only need to be sent once
void Renderer::Render(const Scene& scene,
	const std::vector<PointLight>& pointLights,
	const std::vector<DirectionalLight>& directionalLights,
	const Camera& camera, 
	const int wireframeMode,
	const int bufferDrawMode, 
	const bool drawBoundingBoxes,
	const int boundingBoxDepth)
{
	for (const Object& object : scene.Objects)
	{
		ObjectID index = scene.Objects.ToIndex(&object);
		ObjectTransforms.at(index) = object.MakeModelTransformMatrix();
	}

	uniformBlocks->SetCurrentTime(SDL_GetTicks());
	Uniforms::CameraUniforms cameraUniforms(camera);
	uniformBlocks->CameraUniformBuffer.UploadAll(&cameraUniforms);

	glBindVertexArray(meshBufferManager->VAO.GLIndex());
	glClearColor(0, 0, 0, 1);
	glClearDepth(1.0f);

	// G BUFFERS
	
	glBindFramebuffer(GL_FRAMEBUFFER, GBufferFBO->GLIndex());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, LightingPassFBO->GLIndex());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderStages->MakeGBuffers.Run(*GBufferFBO, scene, BufferedTextures, *meshBufferManager, camera, InternalResolution, ambientLight, wireframeMode);

	// POINT LIGHTS

	for (const auto& light : pointLights)
	{
		if (!camera.MakeFrustum().Intersects(light.GetLightAreaAABB()))
		{
			continue;
		}

		renderStages->PointLightShadowMap.Run(
			light,
			PointLightShadowResolution,
			*PointLightDepthFBO,
			*PointLightDepthBuffer,
			scene,
			BufferedTextures,
			*meshBufferManager,
			camera);

		renderStages->Lighting.Run(
			Vector2i(InternalResolution),
			*LightingPassFBO,
			*GBuffers[0],
			*GBuffers[1],
			*GBuffers[2],
			*CameraDepthBuffer,
			*PointLightDepthBuffer,
			*DirectionalLightShadowCascades,
			true);
	}

	// DIRECTIONAL LIGHTS

	for (const auto& light : directionalLights)
	{
		renderStages->DirectionalLightShadowMap.Run(
			light,
			DirectionalLightShadowResolution,
			CascadeBoundaries,
			*DirectionalLightDepthFBO,
			scene,
			BufferedTextures,
			*meshBufferManager,
			camera,
			*DirectionalLightShadowCascades);
		
		renderStages->Lighting.Run(
			Vector2i(InternalResolution),
			*LightingPassFBO,
			*GBuffers[0],
			*GBuffers[1],
			*GBuffers[2],
			*CameraDepthBuffer,
			*PointLightDepthBuffer,
			*DirectionalLightShadowCascades,
			false);
	}

	// DEBUG PRIMITIVES

	for (const DebugDraw& draw : debugDraws)
	{
		auto& drawBoundsStage = renderStages->DrawBounds;
		auto& drawLineStage = renderStages->DrawLine;

		if (std::holds_alternative<Bounds>(draw.primitive))
		{
			const Bounds& bounds = std::get<Bounds>(draw.primitive);
			drawBoundsStage.Run(bounds, draw.transform, camera, *DebugFBO, draw.colour);
		}
		else if (std::holds_alternative<Frustum>(draw.primitive))
		{
			const Frustum& frustum = std::get<Frustum>(draw.primitive);
			Matrix4 transform = frustum.ViewProjMatrix.Inverse() * draw.transform;
			drawBoundsStage.Run(Frustum::ClipSpaceBounds, transform, camera, *DebugFBO, draw.colour);
		}
		else if (std::holds_alternative<Line>(draw.primitive))
		{
			const Line& line = std::get<Line>(draw.primitive);
			drawLineStage.Run(line, draw.transform, camera, *DebugFBO, draw.colour);
		}
	}
	debugDraws.clear();

	// BLOOM

	renderStages->BloomConvolution.Run(*InternalColourBuffer, *BloomDownsizeTextureBuffer, InternalResolution, *BloomConvolutionFBO);
	renderStages->GaussianBlur.Run(*BloomDownsizeTextureBuffer, BLOOM_OCTAVES, InternalResolution / powf(2, BLOOM_OCTAVES), *BloomBlurTextureBufferX, *BloomBlurTextureBufferY, *GaussianBlurFBO);

	// POSTPROCESSING / DRAW TO SCREEN / DEBUG VIEWS
	
	switch (bufferDrawMode)
	{
	case 0:
		//Standard draw
		renderStages->Postprocessing.Run(OutputResolution, *InternalColourBuffer, *CameraDepthBuffer, *BloomBlurTextureBufferY);
		break;
	case 1:
	case 2:
	case 3:
		//G buffers
		renderStages->CopyTexture.Run(*GBuffers[bufferDrawMode - 1], nullptr, OutputResolution);
		break;
	case 4:
		//G buffer depth
		renderStages->CopyTexture.Run(*CameraDepthBuffer, nullptr, OutputResolution);
		break;
	case 5:
		//Bloom buffer
		renderStages->CopyTexture.Run(*BloomBlurTextureBufferY, nullptr, OutputResolution);
		break;		
	}
}