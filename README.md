This is a basic renderer written in C++ with OpenGL and SDL. Here's a screenshot of it doing its thing:

![Screenshot](http://i.imgur.com/9lAFqPM.jpg)

Features:

* Deferred renderer
* PBR materials using Cook-Torrance specular BRDF and Albedo/Roughness/Metallic/Normal material parameters
* Point and directional lights
* Soft PCF shadows, cast by the aforementioned lights
* Cascaded shadow maps for the directional lights
* Bloom
* Distance/height-based fog
* An infinite simplex noise-based terrain generator

There's a (Possibly slightly outdated) built version with some assets included available on the downloads page

Controls:

* WASD to move the camera
* Space to raise the camera, CTRL to lower it
* Shift to move faster
* Hold left mouse button to rotate the camera
* Hold right mouse button to move one of the point lights around
* F to make the loaded model rotate in place
* R to reload the scene and shaders
* E to cycle through wireframe display modes
* Q to cycle through displaying of various internal buffers (Albedo, Smoothness/Metalness, Normals, Depth and Bloom)
* B to toggle the display of object bounding boxes
* ESC to exit